import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Icofont from 'react-icofont';
import ReactTooltip from 'react-tooltip';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { GoogleMap, LoadScript, Marker, InfoWindow, MarkerClusterer, Polyline } from '@react-google-maps/api';
import { Editor } from '@tinymce/tinymce-react';
import { SketchPicker } from 'react-color';
const scriptFn = () => { }
const { SearchBar } = Search;

class SearchComponent extends React.Component {
    componentDidMount() {
        scriptFn()
    }
    render() {
        return (
            <div>
                <form onSubmit={e => { e.preventDefault(); }}>
                    <div className='form-group form-icon'>
                        <input type='text' onKeyUp={(e) => { this.props.onKeyUp(e) }} className='eldSearch' placeholder='Search ' />
                        <Icofont icon='search' style={{ marginLeft: '-2rem' }} /> <Icofont icon='icofont-info-circle' style={{ marginLeft: '2rem', color: 'var(--main-color)', fontSize: 'medium' }} /><i className='eldText'>Select your ELD providers from the list provided.</i>
                    </div>
                </form>
            </div>
        )
    }
}
class CarrierInfo extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        scriptFn()
    }
    carrierCodes() {
        let carrierCode = [];
        carrierCode.push(
            this.props.passingData.invitationData.carrierInfo.carrierCodes.map((val, index) => {
                return (
                    <div className='col' key={index}>
                        <h5># {val.codeType}-{val.codeValue}</h5>
                    </div>
                )
            })
        )
        return carrierCode
    }
    render() {
        return (
            <div className='row'>
                <div className='col'>
                    <h5><Icofont icon='icofont-user-alt-3' id='iconCOB' style={{ fontSize: '17px', fontWeight: '400' }} /> &nbsp;
                        {this.props.passingData.invitationData?.contactInfo?.firstName}&nbsp;{this.props.passingData.invitationData?.contactInfo?.lastName}
                    </h5>
                </div>
                <div className='col'>
                    <h5> <Icofont icon='icofont-truck-alt' id='iconCOB' style={{ fontSize: '17px', fontWeight: '400' }} /> &nbsp;
                        {this.props.passingData.invitationData?.carrierInfo?.carrierName}
                    </h5>
                </div>
                <div className='col'>
                    <h5> <Icofont icon='icofont-envelope' id='iconCOB' style={{ fontSize: '17px', fontWeight: '400' }} /> &nbsp;
                        {this.props.passingData.invitationData?.contactInfo?.email}
                    </h5>
                </div>
                {this.carrierCodes()}
            </div>
        );
    }
}

class EldSelection extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        scriptFn()
    }
    showEld() {
        let Eld = [];
        const compLength = this.props.eldData.invitationData.eldInfo.length;
        let noOfRows = Math.ceil(compLength / 4);
        let arr = Array(4).fill(0);
        for (let j = 0; j < noOfRows; j++) {
            Eld.push(
                <div className='row' style={{ width: '100%' }} key={j + 1}>
                    {
                        arr.map((val, index) => {
                            return (
                                <div className='col-md-3' key={index + 2}>
                                    {this.getData(j, index)}
                                </div>
                            )
                        })
                    }
                </div>
            )
        }
        return Eld
    }
    getData(j, i) {
        let index = (j * 4) + (i);
        let arr = [];
        let path = this.props.envData + 'elds/logo/'
        if (this.props.eldData.invitationData.eldInfo[index] !== undefined || null) {
            arr.push(
                <div className='row' style={{ opacity: this.props.eldData.invitationData.eldInfo[index]?.overallStatus === 'VERIFIED' || (!this.diableCheck(this.props.eldData.invitationData.eldInfo[index]?.eldProviderId) && this.props.eldData.checkedData.length > 4) ? '.5' : '1' }} key={this.props.eldData.invitationData.eldInfo[index]?.eldProviderId} key={this.props.eldData.invitationData.eldInfo[index]?.eldProviderId}>
                    <div className='col-md-1'>
                        <input type='checkbox' name={this.props.eldData.invitationData.eldInfo[index]?.eldProviderId} defaultChecked={this.props.eldData.invitationData.eldInfo[index].overallStatus === 'VERIFIED'} readOnly={this.props.eldData.invitationData.eldInfo[index]?.overallStatus === 'VERIFIED' || (!this.diableCheck(this.props.eldData.invitationData.eldInfo[index]?.eldProviderId) && this.props.eldData.checkedData.length > 4)}
                            className='checkBox' onChange={(e) => this.handleChange(e, this.props.eldData.invitationData.eldInfo[index], index)} />
                    </div>
                    <div className='col-md-9' style={{ bottom: '28px' }}>
                        <div style={{ width: '60%', marginBottom: '5px' }} data-tip data-for={'image' + this.props.eldData.invitationData.eldInfo[index]?.eldVendor}>
                            <img src={path + this.props.eldData.invitationData.eldInfo[index]?.eldVendor} className='image' />
                            <ReactTooltip id={'image' + this.props.eldData.invitationData.eldInfo[index]?.eldVendor}>
                                {this.props.eldData.invitationData.eldInfo[index]?.eldVendor}
                            </ReactTooltip>
                        </div>
                    </div>
                </div>
            )
            return arr
        }
    }
    handleChange = (e, data, index) => {
        this.props.onChange(e, data, index)
    }
    diableCheck(data) {
        for (let i = 0; i < this.props.eldData.checkedData.length; i++) {
            if (this.props.eldData.checkedData[i].eldProviderId === data) {
                return true
            }
        }
    }
    render() {
        return (
            <div>
                {this.showEld()}
            </div>
        )
    }
}
class AuthorizeEldRequired extends React.Component {
    constructor(props) {
        super(props);
    }
    preventCheck(e, item) {
        if (e.target.checked === true) {
            e.target.checked = false
            e.preventDefault();
            e.stopPropagation();
            e.nativeEvent.stopImmediatePropagation();
            return false;
        }
        e.target.checked = true
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        return false;
    }
    handleChecked = (e, item, i) => {
        this.props.onChange(e, item, i)
    }
    render() {
        return (
            <Fragment>
                <div className='col'>
                    <h5 className='subTitleCss'>Required Data Permissions &nbsp;
                        <Icofont icon='icofont-info-circle' style={{ color: 'var(--main-color)', fontSize: 'medium' }} />
                    </h5>
                    <div>
                        <div className='form-check'>
                            <input className='form-check-input' type='checkbox' name={this.props.item.eldDataSharing.key} id='authenticationData' readOnly={!this.props.item.eldDataSharing.authenticationDataEnabled || this.props.stateData.fieldDisable[this.props.item]?.diableField} defaultChecked={this.props.item.eldDataSharing.authenticationData} onChange={(e) => { !this.props.item.eldDataSharing.authenticationDataEnabled || this.props.stateData.fieldDisable[this.props.item]?.diableField ? this.preventCheck(e, this.props.i, this.props.item) : this.handleChecked(e, this.props.item, this.props.i) }} />
                            <label className='form-check-label' >
                                <Icofont icon='icofont-lock iconName' id='iconCOB' /> &nbsp;Authentication
                            </label>
                        </div>
                        <div className='form-check' >
                            <input className='form-check-input' type='checkbox' id='locationData' readOnly={!this.props.item.eldDataSharing.locationDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField} defaultChecked={this.props.item.eldDataSharing.locationData} onChange={(e) => { !this.props.item.eldDataSharing.locationDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField ? this.preventCheck(e, this.props.i, this.props.item) : this.handleChecked(e, this.props.item, this.props.i) }} />
                            <label className='form-check-label ' >
                                <Icofont icon='icofont-location-pin iconName ' id='iconCOB' /> &nbsp;Location Data
                            </label>
                        </div>
                        <div className='form-check'>
                            <input className='form-check-input' type='checkbox' id='vehicleData' readOnly={!this.props.item.eldDataSharing.vehicleDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField} defaultChecked={this.props.item.eldDataSharing.vehicleData} onChange={(e) => { !this.props.item.eldDataSharing.vehicleDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField ? this.preventCheck(e, this.props.i, this.props.item) : this.handleChecked(e, this.props.item, this.props.i) }} />
                            <label className='form-check-label ' >
                                <Icofont icon='icofont-truck-alt iconName ' id='iconCOB' /> &nbsp;Vehicle Data
                            </label>
                        </div>
                        <div className='form-check'>
                            <label className='form-check-label' >
                            </label>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
class AuthorizeEldAdditional extends React.Component {
    constructor(props) {
        super(props);
    }
    preventCheck(e, item) {
        if (e.target.checked === true) {
            e.target.checked = false
            e.preventDefault();
            e.stopPropagation();
            e.nativeEvent.stopImmediatePropagation();
            return false;
        }
        e.target.checked = true
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        return false;
    }
    handleChecked = (e, item, i) => {
        this.props.onChange(e, item, i)
    }
    render() {
        return (
            <Fragment>
                <div className='col'>
                    <h5 className='subTitleCss'>Additional Data Permissions</h5>
                    <div>
                        <div className='form-check'>
                            <input className='form-check-input' type='checkbox' id='driverData' readOnly={!this.props.item.eldDataSharing.driverDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField} defaultChecked={this.props.item.eldDataSharing.driverData} onChange={(e) => { !this.props.item.eldDataSharing.driverDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField || this.props.stateData.isSkipData ? this.preventCheck(e, this.props.i, this.props.item) : this.handleChecked(e, this.props.item, this.props.i) }} />
                            <label className='form-check-label' >
                                <i className='icofont-user-alt-7 iconName ' id='iconCOB' /> &nbsp;Driver Data
                            </label>
                        </div>
                        <div className='form-check'>
                            <input className='form-check-input' type='checkbox' id='hoursOfServiceData' readOnly={!this.props.item.eldDataSharing.hoursOfServiceDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField} defaultChecked={this.props.item.eldDataSharing.hoursOfServiceData} onChange={(e) => { !this.props.item.eldDataSharing.hoursOfServiceDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField || this.props.stateData.isSkipData ? this.preventCheck(e, this.props.i, this.props.item) : this.handleChecked(e, this.props.item, this.props.i) }} />
                            <label className='form-check-label' >
                                <i className='icofont-clock-time iconName' id='iconCOB' /> &nbsp;Hours of Service
                            </label>
                        </div>
                        <div className='form-check'>
                            <input className='form-check-input' type='checkbox' id='engineData' readOnly={!this.props.item.eldDataSharing.engineDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField} onChange={(e) => { !this.props.item.eldDataSharing.engineDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField || this.props.stateData.isSkipData ? this.preventCheck(e, this.props.i, this.props.item) : this.handleChecked(e, this.props.item, this.props.i) }} defaultChecked={this.props.item.eldDataSharing.engineData} />
                            <label className='form-check-label' >
                                <i className='icofont-spanner iconName' id='iconCOB' /> &nbsp;Engine Data
                            </label>
                        </div>
                        <div className='form-check'>
                            <input className='form-check-input' type='checkbox' id='dvirData' readOnly={!this.props.item.eldDataSharing.dvirDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField} onChange={(e) => { !this.props.item.eldDataSharing.dvirDataEnabled || this.props.stateData.fieldDisable[this.props.i]?.diableField || this.props.stateData.isSkipData ? this.preventCheck(e, this.props.i, this.props.item) : this.handleChecked(e, this.props.item, this.props.i) }} defaultChecked={this.props.item.eldDataSharing.dvirData} />
                            <label className='form-check-label' >
                                <i className='icofont-ebook iconName' id='iconCOB' /> &nbsp;DVIR
                            </label>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}
class UseExistingCredentials extends React.Component {
    constructor(props) {
        super(props);
    }
    preventCheck(e, item) {
        if (e.target.checked === true) {
            e.target.checked = false
            e.preventDefault();
            e.stopPropagation();
            e.nativeEvent.stopImmediatePropagation();
            return false;
        }
        e.target.checked = true
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        return false;
    }
    handleExitingEld = (item, i) => {
        this.props.onChange(item, i)
    }
    render() {
        return (
            <Fragment>
                <div className='form-check'>
                    <input className='form-check-input' type='checkbox' id='exiteldChcekbox' readOnly={this.props.stateData.fieldDisable[this.props.i]?.diableField || this.props.stateData.isSkipData} defaultChecked={this.props.stateData.fieldDisable[this.props.i]?.diableField} onChange={(e) => { this.props.stateData.fieldDisable[this.props.i]?.diableField || this.props.stateData.isSkipData ? this.preventCheck(e, this.props.i, this.props.item) : this.handleExitingEld(this.props.item, this.props.i) }} />
                    <label className='div-content' >&nbsp;
                        Utilize my existing ELD provider credentials
                    </label>
                </div>
            </Fragment>
        );
    }
}

class NewCredentials extends React.Component {
    constructor(props) {
        super(props);
    }
    handleInput = (e, item, i) => {
        this.props.onChange(e, item, i)
    }
    render() {
        return (
            <Fragment>
                {this.props.item.eldVendor === 'Samsara' ?
                    <>  <span className='samsaraPrefix'>samsara_api_</span>
                        <input type='text' className='form-control samasarlabelCssForVerifying  requiredfield json-col' 
                        placeholder={this.props.innsideItem.paramIsRequired ?
                        this.props.innsideItem.paramName + ' *' : this.props.innsideItem.paramName} 
                        id={this.props.innsideItem.paramName} 
                        disabled={!this.props.innsideItem.paramIsRequired} 
                        readOnly={this.props.stateData.fieldDisable[this.props.i]?.diableField} 
                        onChange={(e) => { this.handleInput(e, this.props.item, this.props.j) }}
                        value={this.props.innsideItem?.paramDefaultValue}
                         /></>
                    : <input type={this.props.innsideItem?.paramName.includes('password')?'password':'text'} className='form-control labelCssForVerifying  requiredfield json-col' 
                             placeholder={this.props.innsideItem.paramIsRequired ?
                             this.props.innsideItem.paramName + ' *' :this.props.innsideItem.paramName}
                             id={this.props.innsideItem.paramName} 
                             disabled={!this.props.innsideItem.paramIsRequired} 
                             readOnly={this.props.stateData.fieldDisable[this.props.i]?.diableField} 
                             onChange={(e) => { this.handleInput(e, this.props.item, this.props.j) }}
                             value={this.props.innsideItem?.paramDefaultValue}
                              />}
                {(  this.props.innsideItem.paramIsRequired && (this.props.innsideItem.paramValue === undefined || this.props.innsideItem.paramValue === null || this.props.innsideItem.paramValue === 'undefined' || this.props.innsideItem.paramValue === '')) && this.props.stateData.enableError &&  <span className='shadow-input'>Please Fill Required data</span>}

            </Fragment>
        );
    }
}
//Created for Carriers Tab
class TabsBar extends React.Component {
    async setIndex(methodName, selectedId) {
        await this.props.getIndex(methodName, selectedId)
        methodName()
    }
    render() {
        return (
            <>
                <ul className='nav nav-tabs' id='myTabs' role='tablist'>
                    {this.props.tabsData.map((tab, index) => (
                        <li className='nav-item'>
                            <a className={tab.refData == this.props.activeTab ? 'nav-link active' : 'nav-link'} id={tab.tabID} data-toggle='tab' href={tab.refData} role='tab' aria-controls={tab.refData} aria-selected={index == 0 ? 'true' : 'false'} onClick={() => { this.setIndex(tab.methodName, tab.refData) }}>{tab.component}</a>
                        </li>
                    ))}
                </ul>
            </>
        )
    }
}
class TableDataComponent extends React.Component {
    render() {
        const customTotal = (from, to, size) => (
            <span className='react-bootstrap-table-pagination-total'>
                Showing {from} to {to} of {size} Results
            </span>
        );
        const options = {
            paginationSize: 4,
            pageStartIndex: 1,
            firstPageText: 'First',
            prePageText: 'Back',
            nextPageText: 'Next',
            lastPageText: 'Last',
            nextPageTitle: 'First page',
            prePageTitle: 'Pre page',
            firstPageTitle: 'Next page',
            lastPageTitle: 'Last page',
            showTotal: true,
            paginationTotalRenderer: customTotal,
        };
        return (
            <ToolkitProvider
                keyField='id'
                data={this.props.tableData}
                columns={this.props.columnData}
                search
            >
                {
                    props => (
                        <div>
                            <SearchBar {...props.searchProps} />
                            <BootstrapTable bootstrap4
                                {...props.baseProps} pagination={paginationFactory(options)} noDataIndication={this.props.noDataIndication} hover bordered={false} expandRow={this.props.expandRow ? this.props.expandRow : ''} />
                        </div>
                    )
                }
            </ToolkitProvider>
        )
    }
}

class SearchCarrierComponent extends React.Component {
    render() {
        return (
            <>
                <span className='icofont-info-circle' ></span>
                &ensp;MC , DOT and Name searches both Trucker Cloud and FMCSA for information. SCAC searches only Trucker Cloud.
                <article className='table-data truck-data shadow bg-white rounded' style={{ display: 'block' }}>
                    <form >
                        <div onChange={(e) => { this.props.onChange(e) }} className='row'>
                            <div className='col-lg-3'>
                                <input type='radio' value={this.props.passedData.carriername || ''} id='carriernameid' checked={this.props.passedData.radio1} name='searchGroup' />
                                <input placeholder='Carrier Name ' id='carriernameid' autocomplete='off' type='text' class='labelCssForSearch' disabled={!this.props.passedData.radio1} value={this.props.passedData.carriername || ''} />
                                <span className='shadow-input cname-input'>{this.props.passedData.carrierNameError}</span>
                            </div>
                            <div className='col-lg-3'>
                                <input type='radio' id='mcnumberid' value={this.props.passedData.mcNumber || ''} name='searchGroup' checked={this.props.passedData.radio2} />
                                <input id='mcnumberid' class='labelCssForSearch' autocomplete='off' type='text' placeholder='MC # ' disabled={!this.props.passedData.radio2} value={this.props.passedData.mcNumber || ''} />
                                <span className='shadow-input'>{this.props.passedData.mcNumberError}</span>
                            </div>
                            <div className='col-lg-3'>
                                <input type='radio' value={this.props.passedData.dotNumber || ''} id='dotnumberid' name='searchGroup' checked={this.props.passedData.radio3} />
                                <input id='dotnumberid' type='text' placeholder='DOT #' autocomplete='off' class='labelCssForSearch' disabled={!this.props.passedData.radio3} value={this.props.passedData.dotNumber || ''} />
                                <span className='shadow-input'>{this.props.passedData.dotNumberError}</span>
                            </div>
                            <div className='col-lg-2'>
                                <input type='radio' id='scacnumberid' name='searchGroup' disabled />
                                <input id='scacnumberid' type='text' placeholder='SCAC #' class='labelCssForSearch' disabled />
                            </div>
                            <div className='col-lg-1'>
                                <button type='button' disabled={this.props.passedData.searchValue} value='searchbtn' className='btn btn-primary' onClick={(e) => { this.props.searchClick(e); }} >Search</button>
                            </div>
                        </div>
                    </form>
                </article>
            </>
        )
    }
}
class HeaderBar extends React.Component {
    render() {
        return (
            <div>
                <a className='navbar-brand' href='#' onClick={(e) => { this.props.onclick(e) }}>
                    <img id='trade-logo' src={this.props.passingData} />
                </a>
                <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
                    <span className='navbar-toggler-icon'></span>
                </button>
            </div>
        );
    }
}
class DashboardTrucksComponent extends React.Component {
    constructor(props) {
        super(props);
        this.timerSrc = null;
        this.srcData = '';
        let val = this.props.spinner ? true : false;
    }
    formatLocation = (location) => {
        try {
            let locationArray = location.split('|');
            locationArray = locationArray.reverse();
            return locationArray.filter(x => x.trim().length > 0).join(', ');
        } catch (err) {
            return location;
        }
    }

    handleClick = (arg1, arg2) => {
        var checked = document.getElementById('cb' + arg1.id).checked;
        var arr = [];
        if (arg2.target.className == 'icofont-circled-right') {
            arg2.target.className = 'icofont-circled-down';
            arr.push(arg1);
            if (checked) this.props.truckClicked(arr);
        } else {
            arg2.target.className = 'icofont-circled-right';
            this.props.truckClicked(arr);
        }
    }
    handleChecked = (arg) => {
        var remainingEle = this.props.selectedData.filter(function (truck) {
            var checked = document.getElementById('cb' + truck.id).checked;
            return checked !== false;
        });
        this.props.checkList(remainingEle);
    }
    truckClick = (e, truck) => {
        let arg2 = e.target.closest('label').firstElementChild.firstElementChild;
        if (arg2.className == 'icofont-circled-right') {
            arg2.className = 'icofont-circled-down';
        } else {
            arg2.className = 'icofont-circled-right';
        }
        this.props._truckClick(truck);
    }
    searchTrucks = (e) => {
        this.srcData = $(e.target).val().trim();
        if (this.timerSrc) {
            clearTimeout(this.timerSrc);
        }
        this.timerSrc = setTimeout(
            function () {
                clearTimeout(this.timerSrc);
                this.timerSrc = null;
                this.setState({});
            }
                .bind(this),
            500
        );
    }

    filterData = (data) => {
        if (!(data.length)) return [];
        if (this.srcData.length == 0) return data;
        let userType = localStorage.getItem('userType').toLowerCase();
        let expr = RegExp(this.srcData, 'i');
        let row = data.filter((item, index) => {
            return expr.test((userType == 'shipper' ? item.shipperRefNumber : item.truckNo));
        });
        return row;
    }
    render() {
        let userType = localStorage.getItem('userType').toLowerCase();
        let label = userType == 'shipper' ? 'Load' : 'Truck';
        var stopped = this.filterData(this.props.stopped);
        var idle = this.filterData(this.props.idle);
        var active = this.filterData(this.props.active);
        var disabled = this.props.disabled;
        const stoppedData = !this.props.loading && stopped.length ? (
            stopped.map((stoppedList, index) => {
                var titleval = '';
                let location = this.formatLocation(stoppedList.location);
                if (userType == 'broker') {
                    if (stoppedList.shipperRefNumber !== null) //&&userType!=='carrier'
                    {
                        titleval = 'Customer Load # : ' + stoppedList.shipperRefNumber;
                    }
                }
                var truckOrPoNumber = '';
                if (userType !== 'shipper') {
                    truckOrPoNumber = 'Truck/Asset No : ' + stoppedList.truckNo;
                } else {
                    truckOrPoNumber = 'Load No : ' + stoppedList.shipperRefNumber;
                }
                return (
                    <article key={index}>
                        <div className='card' >
                            <div className='card-header' id={'stoppedHedaing' + stoppedList.id}>
                                <div className='form-check'>
                                    <input className='form-check-input' type='checkbox' onChange={this.handleChecked.bind(stoppedList)} value={stoppedList.id} defaultChecked='true' id={'cb' + stoppedList.id} style={{ display: 'none' }} />
                                    <label data-placement='top' onClick={(e) => { this.truckClick(e, stoppedList) }} className='form-check-label' htmlFor={'cb1' + stoppedList.id} data-toggle='collapse' data-target={'#stoppedCollapse' + stoppedList.id} aria-expanded='true' aria-controls={'stoppedCollapse' + stoppedList.id}>
                                        <span>
                                            <Icofont onClick={this.handleClick.bind(this, stoppedList)} icon='circled-right' data-toggle='collapse' data-target={'#stoppedCollapse' + stoppedList.id} aria-expanded='true' aria-controls={'stoppedCollapse' + stoppedList.id} />
                                        </span> <span data-tip={titleval} title={titleval}>
                                            {truckOrPoNumber} {(stoppedList.loadVehicle == true) ? (<span><img alt='' src={this.props.loadVehicleicon} /></span>) : (<span></span>)}
                                        </span>
                                        <ReactTooltip place='top' /> </label>
                                    <span className='float-right' style={{ color: 'red' }} > Stopped <Icofont icon='ui-press' /></span>
                                </div>
                            </div>
                            <div id={'stoppedCollapse' + stoppedList.id} className='collapse' aria-labelledby={'stoppedHedaing' + stoppedList.id} data-parent='#stopped_stats'>
                                <div className='card-body'>
                                    <ul>
                                        <li>
                                            <span>Date:</span><span>{stoppedList.convertedDate}</span>
                                        </li>
                                        <li>
                                            <span>Location:</span><span>{location}</span>
                                        </li>
                                        <li>
                                            <span>Reason</span><span>-</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </article>
                )
            })
        ) : (
            <div className='stay-vertical'>
                {!this.props.loading ? <div>
                    <p>No Results Found</p>
                </div> :
                    <div className='text-center'>
                        <div className='spinner-border' role='status'>
                            <span className='sr-only'>Loading...</span>
                        </div>
                    </div>}
            </div>
        );
        const idleData = !this.props.loading && idle.length ? (
            idle.map((idleList, index) => {
                var titlevalidle = '';
                let location = this.formatLocation(idleList.location);
                if (userType == 'broker') {
                    if (idleList.shipperRefNumber !== null) {
                        titlevalidle = 'Customer Load # : ' + idleList.shipperRefNumber;
                    }
                }
                var truckOrPoNumber = '';
                if (userType !== 'shipper') {
                    truckOrPoNumber = 'Truck/Asset No : ' + idleList.truckNo;
                } else {
                    truckOrPoNumber = 'Load No : ' + idleList.shipperRefNumber;
                }
                return (
                    <div className='card' key={index}>
                        <div className='card-header' id={'idleHeading' + idleList.id}>
                            <div className='form-check'>
                                <input className='form-check-input' type='checkbox' onChange={this.handleChecked.bind(idleList)} value={idleList.id} defaultChecked='true' id={'cb' + idleList.id} style={{ display: 'none' }} />
                                <label data-placement='top' onClick={(e) => { this.truckClick(e, idleList) }} className='form-check-label' htmlFor={'cb1' + idleList.id} data-toggle='collapse' data-target={'#idleCollapse' + idleList.id} aria-expanded='true' aria-controls={'idleCollapse' + idleList.id}>
                                    <span>
                                        <Icofont onClick={this.handleClick.bind(this, idleList)} icon='circled-right' data-toggle='collapse' data-target={'#idleCollapse' + idleList.id} aria-expanded='true' aria-controls={'idleCollapse' + idleList.id} />
                                    </span> <span data-tip={titlevalidle} title={titlevalidle}>
                                        {truckOrPoNumber} {(idleList.loadVehicle == true) ? (<span><img alt='' src={this.props.loadVehicleicon} /></span>) : (<span></span>)}
                                    </span>
                                    <ReactTooltip place='top' /> </label>

                                <span className='float-right' style={{ color: '#fdc800' }}> <Icofont icon='ui-press' /></span>
                            </div>
                        </div>
                        <div id={'idleCollapse' + idleList.id} className='collapse' aria-labelledby={'idleHeading' + idleList.id} data-parent='#idle_stats'>
                            <div className='card-body'>
                                <ul>
                                    <li>
                                        <span>Date:</span><span>{idleList.convertedDate}</span>
                                    </li>
                                    <li>
                                        <span>Location:</span><span>{location}</span>
                                    </li>
                                    <li>
                                        <span>Reason</span><span>-</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                )
            })
        ) : (
            <div className='stay-vertical'>
                {!this.props.loading ? <div>
                    <p>No Results Found</p>
                </div> :
                    <div className='text-center'>
                        <div className='spinner-border' role='status'>
                            <span className='sr-only'>Loading...</span>
                        </div>
                    </div>}
            </div>
        );
        const activeData = !this.props.loading && active.length ? (
            active.map((activeList, index) => {
                var titlevalactive = '';
                let location = this.formatLocation(activeList.location);
                if (userType == 'broker') {
                    if (activeList.shipperRefNumber !== null) {
                        titlevalactive = 'Customer Load # : ' + activeList.shipperRefNumber;
                    }
                }
                var truckOrPoNumber = '';
                if (userType !== 'shipper') {
                    truckOrPoNumber = 'Truck/Asset No : ' + activeList.truckNo;
                } else {
                    truckOrPoNumber = 'Load No : ' + activeList.shipperRefNumber;
                }
                return (
                    <div className='card' key={index} >
                        <div className='card-header' id={'activeHedaing' + activeList.id}>
                            <div className='form-check'>
                                <input className='form-check-input' type='checkbox' onChange={this.handleChecked.bind(activeList)} value={activeList.id} defaultChecked='true' id={'cb' + activeList.id} style={{ display: 'none' }} />
                                <label data-placement='top' onClick={(e) => { this.truckClick(e, activeList) }} className='form-check-label' htmlFor={'cb1' + activeList.id} data-toggle='collapse' data-target={'#activeCollapse' + activeList.id} aria-expanded='true' aria-controls={'activeCollapse' + activeList.id}>
                                    <span>
                                        <Icofont onClick={this.handleClick.bind(this, activeList)} icon='circled-right' data-toggle='collapse' data-target={'#activeCollapse' + activeList.id} aria-expanded='true' aria-controls={'activeCollapse' + activeList.id} />
                                    </span> <span data-tip={titlevalactive} title={titlevalactive}>
                                        {truckOrPoNumber} {(activeList.loadVehicle == true) ? (<span><img alt='' src={this.props.loadVehicleicon} /></span>) : (<span></span>)}
                                    </span>
                                    <ReactTooltip place='top' /> </label>
                                <span className='float-right' style={{ color: 'green' }}> <Icofont icon='ui-press' /></span>
                            </div>
                        </div>
                        <div id={'activeCollapse' + activeList.id} className='collapse' aria-labelledby={'activeHedaing' + activeList.id} data-parent='#active_stats'>
                            <div className='card-body'>
                                <ul>
                                    <li>
                                        <span>Date:</span><span>{activeList.convertedDate}</span>
                                    </li>
                                    <li>
                                        <span>Location:</span><span>{location}</span>
                                    </li>
                                    <li>
                                        <span>Reason</span><span>-</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                )
            })
        ) : (
            <div className='stay-vertical'>
                {!this.props.loading ? <div>
                    <p>No Results Found</p>
                </div> :
                    <div className='text-center'>
                        <div className='spinner-border' role='status'>
                            <span className='sr-only'>Loading...</span>
                        </div>
                    </div>}
            </div>
        );
        return (
            <div className='truck-status'>
                <h4 className='page-title'>{label} Status</h4>
                <ul className='nav nav-tabs flex-column flex-sm-row' id='myTab' role='tablist'>
                    <li className='nav-item flex-sm-fill text-sm-center'>
                        <a className='nav-link active' style={disabled ? { pointerEvents: 'none', opacity: '0.4' } : {}} onClick={() => { this.props.tabClicked(1); }} id='home-tab' data-toggle='tab' href='#home' role='tab' aria-controls='home' aria-selected='true'>Active [{this.props.active.length}]</a>
                    </li>
                    <li className='nav-item flex-sm-fill text-sm-center'>
                        <a className='nav-link' style={disabled ? { pointerEvents: 'none', opacity: '0.4' } : {}} onClick={() => { this.props.tabClicked(2); }} id='profile-tab' data-toggle='tab' href='#profile' role='tab' aria-controls='profile' aria-selected='false'>Idle [{this.props.idle.length}] </a>
                    </li>
                    <li className='nav-item flex-sm-fill text-sm-center'>
                        <a className='nav-link' style={disabled ? { pointerEvents: 'none', opacity: '0.4' } : {}} onClick={() => { this.props.tabClicked(3); }} id='contact-tab' data-toggle='tab' href='#contact' role='tab' aria-controls='contact' aria-selected='false'>Stopped [{this.props.stopped.length}] </a>
                    </li>
                </ul>
                <div className='tab-search'>
                    <form onSubmit={e => { e.preventDefault(); }}>
                        <div className='form-group form-icon' id='searchicondashboard'>
                            <input type='text' onKeyUp={(e) => { this.searchTrucks(e) }} className='form-control control-custom' id='truckSearch' placeholder={'Search ' + label} />
                            <span><Icofont icon='search' /></span>
                        </div>
                    </form>
                </div>
                <div className='tab-content' id='myTabContent'>
                    <div className='tab-pane fade show active' id='home' role='tabpanel' aria-labelledby='home-tab'>
                        <div className='accordion' id='active_stats'>
                            {activeData}
                        </div>
                    </div>
                    <div className='tab-pane fade' id='profile' role='tabpanel' aria-labelledby='profile-tab'>
                        <div className='accordion' id='idle_stats'>
                            {idleData}
                        </div>
                    </div>
                    <div className='tab-pane fade' id='contact' role='tabpanel' aria-labelledby='contact-tab'>
                        <div className='accordion' id='stopped_stats'>
                            {stoppedData}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class GetCarrier extends React.Component {
    handleChange = (e) => {
        this.props.onChange(e)
    }
    render() {
        let userType = this.props.userType
        return (
            <div className='carrierDropdown'>
                <div className='col col-md-12'>
                    <label htmlFor='carrierdropdownLabel' className='carrierdropdownLabel' ><b>Select Carrier</b></label>
                    <select disabled={this.props.state.disabled} className='form-control' id='findCarrierId' onChange={(e) => { this.handleChange(e) }}>
                        <option value='0'>Select Carrier</option>
                        {
                            this.props.state.carrierNames?.length > 0 ? (this.props.state.carrierNames.map(function (company, index) {
                                return (
                                    <option key={index} value={userType === 'broker' ? company.Invited_Company_Id : company.Company_Id}>{company.Company_Name}</option>
                                )
                            })) : (<option>Loading.....</option>)
                        }
                    </select>
                </div>
            </div>
        );
    }
}

class GetEld extends React.Component {
    selectELD = (e) => {
        this.props.onChange(e)
    }
    render() {
        return (
            <div>
                {
                    this.props.dashboard ?
                        (this.props.eldList && this.props.eldList.length > 0) ?
                            <div className='carrierDropdown'>
                                <div className='col col-md-12'>
                                    <label htmlFor='carrierdropdownLabel' className='carrierdropdownLabel' ><b>Select ELD Providers</b></label>
                                    <select className='form-control' id='findCarrierId' onChange={(e) => { this.selectELD(e) }}>
                                        {
                                            (this.props.dashboard && this.props.eldList.length > 0) ? (this.props.eldList.map(function (row, index) {
                                                return (
                                                    <option key={index} value={row.ELD_Provider_Id}>{row.ELD_Provider_Disp_Name}</option>
                                                )
                                            })) : null
                                        }
                                    </select>
                                </div>
                            </div> : null
                        :  this.props.selectELD ? <div className='row'>
                                <div className='col-3'>
                                    <label ><b>Select ELD Provider</b></label>
                                    <select className='form-control' id='findELDId' onChange={(e) => { this.selectELD(e) }}>
                                        <option value='0'>Select ELD Provider</option>
                                        {
                                            this.props.selectELD?.eldProvidersList?
                                            this.props.selectELD.eldProvidersList.map(function (row) {
                                                return (
                                                    <option key={row.ELD_Provider_Id} value={row.ELD_Provider_Id}>{row.ELD_Provider_Disp_Name}</option>
                                                )
                                            })
                                            :this.props.selectELD?.eldList?.map(function (row) {
                                                return (
                                                    row.name !=='root Eld'?
                                                    <option key={row.id} value={row.id}>{row.eldProviderDispName}</option>:null
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                            </div>:null
                }
            </div>
        );
    }
}

class DashboardTrucksTrmComponent extends React.Component {
    constructor(props) {
        super(props);
        this.timerSrc = null;
        this.srcData = '';
    }
    formatLocation = (location) => {
        try {
            let locationArray = location.split('|');
            locationArray = locationArray.reverse();
            return locationArray.filter(x => x.trim().length > 0).join(', ');
        } catch (err) {
            return location;
        }
    }
    LoadingSpinner() {
        return (
            <div className='d-flex justify-content-center'>
                <div className='spinner-border' role='status'>
                    <span className='sr-only'>Loading...</span>
                </div>
            </div>
        );
    }
    handleClick = (arg1, arg2) => {
        var checked = document.getElementById('cb' + arg1.id).checked;
        var arr = [];
        if (arg2.target.className == 'icofont-circled-right') {
            arg2.target.className = 'icofont-circled-down';
            arr.push(arg1);
            if (checked) this.props.truckClicked(arr);
        } else {
            arg2.target.className = 'icofont-circled-right';
            this.props.truckClicked(arr);
        }
    }
    handleChecked = (arg) => {
        var remainingEle = this.props.selectedData.filter(function (truck) {
            var checked = document.getElementById('cb' + truck.id).checked;
            return checked !== false;
        });

        this.props.checkList(remainingEle);
    }
    truckClick = (e, truck) => {
        this.props._truckClick(truck);
    }
    searchTrucks = (e) => {
        this.srcData = $(e.target).val().trim();
        if (this.timerSrc) {
            clearTimeout(this.timerSrc);
        }
        this.timerSrc = setTimeout(
            function () {
                clearTimeout(this.timerSrc);
                this.timerSrc = null;
                this.setState({});
            }
                .bind(this),
            500
        );
    }
    filterData = (data) => {
        if (!(data.length)) return [];
        if (this.srcData.length == 0) return data;
        let expr = RegExp(this.srcData, 'i');
        let row = data.filter((item, index) => {
            return expr.test(item.truckNo);
        });
        return row;
    }

    render() {
        var stopped = this.filterData(this.props.stopped);
        var idle = this.filterData(this.props.idle);
        var active = this.filterData(this.props.active);
        const stoppedData = stopped.length ? (
            stopped.map(stoppedList => {
                var titleval = '';
                let location = formatLocation(stoppedList.location);
                if (stoppedList.shipperRefNumber !== null) {
                    titleval = 'Shipper Ref Number : ' + stoppedList.shipperRefNumber;
                }
                else if (stoppedList.loadNumber !== null) {
                    titleval = 'Load Number : ' + stoppedList.loadNumber;
                }
                return (
                    <div className='card' key={stoppedList.id}>
                        <div className='card-header' id={'stoppedHedaing' + stoppedList.id}>
                            <div className='form-check'>
                                <input className='form-check-input' type='checkbox' onChange={this.handleChecked.bind(stoppedList)} value={stoppedList.id} defaultChecked='true' id={'cb' + stoppedList.id} style={{ display: 'none' }} />
                                <label data-placement='top' onClick={(e) => { this.truckClick(e, stoppedList) }} className='form-check-label' htmlFor={'cb1' + stoppedList.id}>
                                    <span>
                                        <Icofont onClick={this.handleClick.bind(this, stoppedList)} icon='circled-right' data-toggle='collapse' data-target={'#stoppedCollapse' + stoppedList.id} aria-expanded='true' aria-controls={'stoppedCollapse' + stoppedList.id} />
                                    </span> <span data-tip={titleval} title={titleval}>
                                        Truck/Asset No :{stoppedList.truckNo} {(stoppedList.loadVehicle == true) ? (<span><img alt='' src={this.props.loadVehicleicon} /></span>) : (<span></span>)}
                                    </span>
                                    <ReactTooltip place='top' /> </label>
                                <span className='float-right' style={{ color: 'red' }} ><Icofont icon='ui-press' /></span>
                            </div>
                        </div>
                        <div id={'stoppedCollapse' + stoppedList.id} className='collapse' aria-labelledby={'stoppedHedaing' + stoppedList.id} data-parent='#stopped_stats'>
                            <div className='card-body'>
                                <ul>
                                    <li>
                                        <span>Date:</span><span>{stoppedList.convertedDate}</span>
                                    </li>
                                    <li>
                                        <span>Location:</span><span>{location}</span>
                                    </li>
                                    <li>
                                        <span>Reason</span><span>-</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                )
            })
        ) : (
            <div className='stay-vertical'>
                <div>
                    <p>No Results Found</p>
                </div>
            </div>
        );

        const idleData = idle.length ? (
            idle.map(idleList => {
                var titlevalidle = '';
                let location = formatLocation(idleList.location);
                if (idleList.shipperRefNumber !== null) {
                    titlevalidle = 'Shipper Ref Number : ' + idleList.shipperRefNumber;
                }
                else if (idleList.loadNumber !== null) {
                    titlevalidle = 'Load Number : ' + idleList.loadNumber;
                }
                return (
                    <div className='card' key={idleList.id}>
                        <div className='card-header' id={'idleHeading' + idleList.id}>
                            <div className='form-check'>
                                <input className='form-check-input' type='checkbox' onChange={this.handleChecked.bind(idleList)} value={idleList.id} defaultChecked='true' id={'cb' + idleList.id} style={{ display: 'none' }} />
                                <label data-placement='top' onClick={(e) => { this.truckClick(e, idleList) }} className='form-check-label' htmlFor={'cb1' + idleList.id}>
                                    <span>
                                        <Icofont onClick={this.handleClick.bind(this, idleList)} icon='circled-right' data-toggle='collapse' data-target={'#idleCollapse' + idleList.id} aria-expanded='true' aria-controls={'idleCollapse' + idleList.id} />
                                    </span> <span data-tip={titlevalidle} title={titlevalidle}>
                                        Truck/Asset No :{idleList.truckNo} {(idleList.loadVehicle == true) ? (<span><img alt='' src={this.props.loadVehicleicon} /></span>) : (<span></span>)}
                                    </span>
                                    <ReactTooltip place='top' /> </label>
                                <span className='float-right' style={{ color: '#fdc800' }}><Icofont icon='ui-press' /></span>
                            </div>
                        </div>
                        <div id={'idleCollapse' + idleList.id} className='collapse' aria-labelledby={'idleHeading' + idleList.id} data-parent='#idle_stats'>
                            <div className='card-body'>
                                <ul>
                                    <li>
                                        <span>Date:</span><span>{idleList.convertedDate}</span>
                                    </li>
                                    <li>
                                        <span>Location:</span><span>{location}</span>
                                    </li>
                                    <li>
                                        <span>Reason</span><span>-</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                )
            })
        ) : (
            <div className='stay-vertical'>
                <div>
                    <p>No Results Found</p>
                </div>
            </div>
        );

        const activeData = active.length ? (
            active.map(activeList => {
                var titlevalactive = '';
                let location = formatLocation(activeList.location);
                if (activeList.shipperRefNumber !== null) {
                    titlevalactive = 'Shipper Ref Number : ' + activeList.shipperRefNumber;
                }
                else if (activeList.loadNumber !== null) {
                    titlevalactive = 'Load Number : ' + activeList.loadNumber;
                }
                return (
                    <div className='card' key={activeList.id}>
                        <div className='card-header' id={'activeHedaing' + activeList.id}>
                            <div className='form-check'>
                                <input className='form-check-input' type='checkbox' onChange={this.handleChecked.bind(activeList)} value={activeList.id} defaultChecked='true' id={'cb' + activeList.id} style={{ display: 'none' }} />
                                <label data-placement='top' onClick={(e) => { this.truckClick(e, activeList) }} className='form-check-label' htmlFor={'cb1' + activeList.id}>
                                    <span>
                                        <Icofont onClick={this.handleClick.bind(this, activeList)} icon='circled-right' data-toggle='collapse' data-target={'#activeCollapse' + activeList.id} aria-expanded='true' aria-controls={'activeCollapse' + activeList.id} />
                                    </span> <span data-tip={titlevalactive} title={titlevalactive}>
                                        Truck/Asset No :{activeList.truckNo} {(activeList.loadVehicle == true) ? (<span><img alt='' src={this.props.loadVehicleicon} /></span>) : (<span></span>)}
                                    </span>
                                    <ReactTooltip place='top' /> </label>

                                <span className='float-right' style={{ color: 'green' }}><Icofont icon='ui-press' /></span>
                            </div>
                        </div>
                        <div id={'activeCollapse' + activeList.id} className='collapse' aria-labelledby={'activeHedaing' + activeList.id} data-parent='#stopped_stats'>
                            <div className='card-body'>
                                <ul>
                                    <li>
                                        <span>Date:</span><span>{activeList.convertedDate}</span>
                                    </li>
                                    <li>
                                        <span>Location:</span><span>{location}</span>
                                    </li>
                                    <li>
                                        <span>Reason</span><span>-</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                )
            })
        ) : (
            <div className='stay-vertical'>
                <div>
                    <p>No Results Found</p>
                </div>
            </div>
        );

        let spinner = this.props.spinner;
        return (
            <div className='truck-status'>
                <h4 className='page-title'><span>{this.props.eldProvider}</span> Trucks Status</h4>
                <ul className='nav nav-tabs flex-column flex-sm-row' id='myTab' role='tablist'>
                    <li className='nav-item flex-sm-fill text-sm-center'>
                        <a className={'nav-link ' + (this.props.aTab == 1 ? 'active' : '')} onClick={() => { this.props.tabClicked(1); }} id='home-tab' data-toggle='tab' href='#home' role='tab' aria-controls='home' aria-selected={(this.props.aTab == 1 ? 'true' : 'false')}>Active [{this.props.active.length}]</a>
                    </li>
                    <li className='nav-item flex-sm-fill text-sm-center'>
                        <a className={'nav-link ' + (this.props.aTab == 2 ? 'active' : '')} onClick={() => { this.props.tabClicked(2); }} id='profile-tab' data-toggle='tab' href='#profile' role='tab' aria-controls='profile' aria-selected={(this.props.aTab == 2 ? 'true' : 'false')}>Idle [{this.props.idle.length}] </a>
                    </li>
                    <li className='nav-item flex-sm-fill text-sm-center'>
                        <a className={'nav-link ' + (this.props.aTab == 3 ? 'active' : '')} onClick={() => { this.props.tabClicked(3); }} id='contact-tab' data-toggle='tab' href='#contact' role='tab' aria-controls='contact' aria-selected={(this.props.aTab == 3 ? 'true' : 'false')}>Stopped [{this.props.stopped.length}] </a>
                    </li>
                </ul>
                <div className='tab-search'>
                    <form onSubmit={e => { e.preventDefault(); }}>
                        <div className='form-group form-icon'>
                            <input type='text' onKeyUp={(e) => { this.searchTrucks(e) }} className='form-control control-custom' id='' placeholder='Search Trucks' />
                            <span><Icofont icon='search' /></span>
                        </div>
                    </form>
                </div>
                <div className='tab-content' id='myTabContent'>
                    <div className={'tab-pane fade ' + (this.props.aTab == 1 ? 'show active' : '')} id='home' role='tabpanel' aria-labelledby='home-tab'>
                        <div className='accordion' id='active_stats'>
                            {spinner ? this.LoadingSpinner() : activeData}
                        </div>
                    </div>
                    <div className={'tab-pane fade ' + (this.props.aTab == 2 ? 'show active' : '')} id='profile' role='tabpanel' aria-labelledby='profile-tab'>
                        <div className='accordion' id='idle_stats'>
                            {spinner ? this.LoadingSpinner() : idleData}
                        </div>
                    </div>
                    <div className={'tab-pane fade ' + (this.props.aTab == 3 ? 'show active' : '')} id='contact' role='tabpanel' aria-labelledby='contact-tab'>
                        <div className='accordion' id='stopped_stats'>
                            {spinner ? this.LoadingSpinner() : stoppedData}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class BreadCrumpTrail extends React.Component {
    render() {
        return (
            <div className={`form-group ${(this.props.touched && this.props.error) ? 'error' : ''}`}>
                <select {...this.props.input} placeholder={this.props.label} id={this.props.id} className={`form-control ${(this.props.controlClass) ? this.props.controlClass : ''}`}>
                    {this.props.condition ? <option value=''>{`${(this.props.label) ? this.props.label : this.props.selectlabel}`}</option> : null}
                    {
                        this.props.condition ?
                            Array.isArray(this.props.data) ? this.props.data.map(option =>
                                <option
                                    value={option.truckNo}
                                    key={option.id}>{option.truckNo}
                                </option>) : ''
                            :

                            Array.isArray(this.props.data) ? this.props.data.map(option => {
                                return (
                                    <option
                                        value={option}
                                        key={option}>{option}
                                    </option>)
                            })
                                : ''
                    }
                </select>
                {this.props.touched && this.props.error && <span className='error'>{this.props.error}</span>}
            </div>
        );
    }
}

class BreadCrumpTrailButton extends React.Component {
    render() {
        return (
            <Fragment>
                {this.props.condition ?
                    <button type='submit' disabled={this.props.state.loading} value={this.props.state.export} onClick={(e) => { this.props.onClick() }} data-name='export' className='btn btn-primary btn-sm'>
                        {this.props.state.loading && this.props.state.exportLoader && (
                            <i
                                className='fa fa-refresh fa-spin'
                                style={{ marginRight: '5px' }}
                            />
                        )}
                        {(!this.props.state.loading || this.props.state.goLoader) && <span>Export</span>}
                        {this.props.state.loading && this.props.state.exportLoader && <span>Fetching...</span>}
                    </button> :
                    <button type='submit' disabled={this.props.state.loading} data-name='go' value={this.props.state.go} id='resizeButton' onClick={(e) => { this.props.onClick() }} data-toggle='modal' data-target='.gopopup' className={this.props.state.loading ? 'btn btn-primary btn-sm float-right loaderButton' : 'btn btn-primary btn-sm float-right'}>
                        {this.props.state.loading && this.props.state.goLoader && (
                            <i
                                className='fa fa-refresh fa-spin'
                                style={{ marginRight: '5px' }}
                            />
                        )}
                        {(!this.props.state.loading || this.props.state.exportLoader) && <span>Go</span>}
                        {this.props.state.loading && this.props.state.goLoader && <span>Fetching...</span>}
                    </button>
                }
            </Fragment>
        )
    }
}

class DashboardGoogleMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markerPopupShow: false,
            currentMarker: null,
            infwCenter: {
                lat: 41.89101,
                lng: -87.62342
            },
            infwData: {},
            activeMovingData: [],
            movingMarkers: []
        }
        this.GOOGLE_MAP_KEY = localStorage.getItem('GOOGLE_MAP_KEY');
        this.map = null;
        this.activeMovingTimer = null;
        this.activeTruckId = null;
        this.mapCenter = props.mapCenter;
        this.mapZoom = props.mapZoom;
    }
    polyLien1 = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: true,
        radius: 30000,
        zIndex: 1
    };
    mapStyle = {
        width: '100%',
        height: '90vh'
    };
    Clusterers1 = {
        imagePath:
            'http://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
    };
    getUrlSegments = (url) => {
        let urlSegs = url.split('/');
        if (Array.isArray(urlSegs)) {
            let len1 = urlSegs.length;
            let lastSeg = urlSegs[len1 - 1];
            lastSeg = lastSeg.split('?');
            urlSegs[len1 - 1] = lastSeg[0];
            return urlSegs;
        } else {
            return [];
        }
    }
    getUrlLastSegments = (url) => {
        let urlSegs = this.getUrlSegments(url);
        let len1 = urlSegs.length;
        if (len1 == 0) {
            return '';
        } else {
            return urlSegs[len1 - 1];
        }
    }
    authHeader() {
        let authToken = localStorage.getItem('authToken');
        if (authToken) {
            return {
                'Content-Type': 'application/json',
                'Authorization': authToken
            };
        }
        else {
            return {
                'Content-Type': 'application/json'
            };
        }

    }
    checkUnauthorized = (status, msg) => {
        if (status == 'UNAUTHORIZED') {
            localStorage.clear();
            localStorage.setItem('logmsg', msg);
            window.location = '/login'
        }
    }
    getActiveTruckTracking = (request) => {
        const BASE_URL = this.props.envData;
        let COMPANY_AUTH_ELD_ENDPOINT = BASE_URL + 'trackingresource/getActiveTruckTracking?assetId=' + request.assetId

        if (request.fromDate) {
            COMPANY_AUTH_ELD_ENDPOINT = COMPANY_AUTH_ELD_ENDPOINT + '&fromDate=' + request.fromDate
        }

        return fetch(COMPANY_AUTH_ELD_ENDPOINT, {
            method: 'GET',
            headers: this.authHeader()
        })
            .then((response) => response.json())
            .then((responseData) => {
                this.checkUnauthorized(responseData.status, responseData.message);
                return responseData;
            })
            .catch(error => {
                return {};
            });
    }
    componentDidUpdate = (prevProps) => {
        if (prevProps.mapCenter != this.props.mapCenter) {
            this.mapCenter = this.props.mapCenter;
        }
        if (this.map) {
            this.map.setZoom(this.mapZoom);
            this.map.setCenter(this.mapCenter);
        }
        if (this.props.mapFlag != prevProps.mapFlag) {
            this.activeTruckId = null;
            if (this.activeMovingTimer != null) {
                clearInterval(this.activeMovingTimer);
                this.activeMovingTimer = null;
            }
            if (this.props.mapViewMode == 'moving') {
                this.mapZoom = 12;
                this.getLastTrackingData(this.props.truckInfo.id);
            } else {
                this.mapZoom = this.props.mapZoom;
            }
            this.setState({
                ...this.state,
                markerPopupShow: false,
                currentMarker: null,
                activeMovingData: []
            });
        }
    }
    getLastTrackingData = (activeTruckId) => {
        let path1 = this.getUrlLastSegments(this.props.location.pathname);
        let path2 = this.getUrlLastSegments(window.location.toString());
        path1 = path1.replace('#', '');
        path2 = path2.replace('#', '');
        if (activeTruckId == this.props.truckInfo.id && this.props.mapViewMode == 'moving' && path1 == path2) {
            this.activeTruckId = activeTruckId;
            let rndval = Date.now();
            this.getActiveTruckTracking({ rndval: rndval, assetId: activeTruckId }).then(response => {

                if (this.activeTruckId == this.props.truckInfo.id && response && response.TrackingData && response.TrackingData.length > 0) {
                    let lat = parseFloat(response.TrackingData[0].latitude);
                    let lng = parseFloat(response.TrackingData[0].longitude);
                    let lastLat = 0;
                    let lastLng = 0;
                    let len1 = this.state.activeMovingData.length;
                    if (len1 > 0) {
                        lastLat = this.state.activeMovingData[len1 - 1].lat;
                        lastLng = this.state.activeMovingData[len1 - 1].lng;
                    }
                    let activeMovingData = [];
                    if (!(isNaN(lat) || isNaN(lng))) {
                        if (lastLat != lat || lastLng != lng) {
                            activeMovingData = [...this.state.activeMovingData, { lat: lat, lng: lng }];
                            let movingMarkers = (len1 == 0 ? [response.TrackingData[0]] : [this.state.movingMarkers[0], response.TrackingData[0]]);

                            let bounds = this.map.getBounds().contains({ lat: lat, lng: lng });
                            if (!bounds) {
                                this.mapZoom--;
                            }
                            this.setState({
                                ...this.state,
                                activeMovingData: activeMovingData,
                                movingMarkers: movingMarkers
                            });
                        }
                    }
                }
                if (this.activeTruckId == this.props.truckInfo.id) {
                    this.activeMovingTimer = setTimeout(() => {
                        this.getLastTrackingData(this.props.truckInfo.id);
                    }, 15000);
                }
            });
        }
    }
    onClickMarker = (e, data, index) => {
        if (index == this.state.currentMarker) return;
        let speed = parseFloat(data.speed);
        if (isNaN(speed)) {
            speed = '';
        }
        else {
            speed = speed.toFixed(2) + ' mph';
        }
        let infwCenter = {
            lat: parseFloat(data.latitude),
            lng: parseFloat(data.longitude)
        }
        let infwData = {
            truckNo: data.truckNo,
            date: data.convertedDate,
            speed: speed,
            location: data.location
        }
        this.setState({
            ...this.state,
            markerPopupShow: true,
            currentMarker: index,
            infwCenter: infwCenter,
            infwData: infwData
        })
    }

    loadMarker = () => {
        let mapData = [];
        if (this.props.mapViewMode == 'moving') {
            mapData = [...this.state.movingMarkers];
            let len1 = mapData.length;
            if (len1 > 0) {
                if (len1 > 1) {
                    let icon1 = 'http://maps.google.com/mapfiles/ms/icons/red.png';
                    let icon2 = 'http://maps.google.com/mapfiles/ms/icons/green.png';
                    let pos1 = {
                        lat: parseFloat(mapData[0].latitude),
                        lng: parseFloat(mapData[0].longitude)
                    };
                    let pos2 = {
                        lat: parseFloat(mapData[1].latitude),
                        lng: parseFloat(mapData[1].longitude)
                    };
                    let data1 = {
                        truckNo: this.props.truckInfo.truckNo,
                        convertedDate: mapData[0].convertedDate,
                        speed: mapData[0].speed,
                        location: mapData[0].location,
                        latitude: mapData[0].latitude,
                        longitude: mapData[0].longitude
                    }
                    let data2 = {
                        truckNo: this.props.truckInfo.truckNo,
                        convertedDate: mapData[1].convertedDate,
                        speed: mapData[1].speed,
                        location: mapData[1].location,
                        latitude: mapData[1].latitude,
                        longitude: mapData[1].longitude
                    }
                    return [<Marker key={0}
                        icon={icon1}
                        position={pos1}
                        onMouseOver={(e) => { this.onClickMarker(e, data1, 0) }}
                    />, <Marker key={1}
                        icon={icon2}
                        position={pos2}
                        onMouseOver={(e) => { this.onClickMarker(e, data2, 1) }}
                    />];
                } else {
                    let icon1 = 'http://maps.google.com/mapfiles/ms/icons/green.png';
                    let pos1 = {
                        lat: parseFloat(mapData[0].latitude),
                        lng: parseFloat(mapData[0].longitude)
                    };
                    let data1 = {
                        truckNo: this.props.truckInfo.truckNo,
                        convertedDate: mapData[0].convertedDate,
                        speed: mapData[0].speed,
                        location: mapData[0].location,
                        latitude: mapData[0].latitude,
                        longitude: mapData[0].longitude
                    }
                    return [<Marker key={0}
                        icon={icon1}
                        position={pos1}
                        onMouseOver={(e) => { this.onClickMarker(e, data1, 0) }}
                    />];
                }

            } else {
                return <Fragment></Fragment>
            }
        } else {
            mapData = [...this.props.mapData];
            let icon = 'http://maps.google.com/mapfiles/ms/icons/green.png';
            if (this.props.truckstate == 'stopped') {
                icon = 'http://maps.google.com/mapfiles/ms/icons/red.png';
            } else if (this.props.truckstate == 'ideal') {
                icon = 'http://maps.google.com/mapfiles/ms/icons/yellow.png';
            }
            return <MarkerClusterer options={this.Clusterers1}>
                {(clusterer) =>
                    mapData.map((row, index) => {
                        let pos = {
                            lat: parseFloat(row.latitude),
                            lng: parseFloat(row.longitude)
                        };
                        return <Marker key={index}
                            icon={icon}
                            position={pos}
                            onMouseOver={(e) => { this.onClickMarker(e, row, index) }}
                            clusterer={clusterer}
                        />
                    })
                }</MarkerClusterer>
        }
    }
    infClose = () => {
        this.setState({
            ...this.state,
            markerPopupShow: false,
            currentMarker: null,
        });
    }
    render = () => {
        return (
            <div>
                <LoadScript
                    googleMapsApiKey={this.GOOGLE_MAP_KEY}>
                    <GoogleMap key={'B1'}
                        mapContainerStyle={this.mapStyle}
                        zoom={this.mapZoom}
                        center={this.mapCenter}
                        onLoad={(map) => { this.map = map }}
                    >
                        {this.loadMarker()}
                        {this.state.markerPopupShow &&
                            <InfoWindow
                                position={this.state.infwCenter}
                                onCloseClick={() => { this.infClose() }}
                            ><span>
                                    <p>Truck/Asset No: {this.state.infwData.truckNo}</p>
                                    <p>Date: {this.state.infwData.date}</p>
                                    <p>Speed: {this.state.infwData.speed}</p>
                                    <p>Location: {this.state.infwData.location}</p>
                                </span>
                            </InfoWindow>
                        }
                        <Polyline
                            path={this.state.activeMovingData}
                            options={this.polyLien1}
                        />
                    </GoogleMap>
                </LoadScript>
            </div>
        );
    }
}

class InviteNewCarrierWithOutExistingData extends React.Component {
    render() {
        return (
            <article className='table-data truck-data shadow bg-white rounded' style={{ display: 'block' }}>
                <form id='new_carrier' >
                    <div className='row'>
                        <div className='col form-group' data-tip data-for={'value' + this.props.state.carrierName}>
                            <label>Carrier Name</label>
                            <input disabled={!this.props.state.carrierName.has_carrier == true ? true : false} type='text' className='form-control requiredfield json-col' placeholder='Carrier Name' name='carrierName' onChange={(e) => { this.props.set_carrierName(e) }} value={this.props.state.carrierName} />
                            <ReactTooltip id={'value' + this.props.state.carrierName}>{this.props.state.carrierName}</ReactTooltip>
                        </div>
                        <div className='col form-group'>
                            <label>MC#</label>
                            <input disabled={!this.props.state.mcNumber.has_mcnumber == true ? true : false} type='text' className='form-control  json-col' placeholder='MC#' name='mcNumber' onChange={(e) => { this.props.set_mcNumber(e) }} value={this.props.state.mcNumber} />
                        </div>
                        <div className='col form-group'>
                            <label>DOT#</label>
                            <input disabled={!this.props.state.dotNumber.has_dotnumber == true ? true : false} type='text' className='form-control requiredfield json-col' placeholder='DOT#' name='dotNumber' onChange={(e) => { this.props.set_dotnumber(e) }} value={this.props.state.dotNumber} />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col form-group'>
                            <label>Carrier Email</label>
                            <input type='email' className='form-control requiredfield json-col' placeholder='Carrier Email' name='email' onChange={(e) => { this.props.set_email(e) }} value={this.props.state.email} />
                            <span className='shadow-input'>{this.props.state.emailError}</span>
                        </div>
                        <div className='col form-group'>
                            <label>First Name</label>
                            <input type='text' className='form-control requiredfield json-col' placeholder='First Name' name='firstName' onChange={(e) => { this.props.set_firstName(e) }} value={this.props.state.firstName} />
                        </div>
                        <div className='col form-group'>
                            <label>Last Name</label>
                            <input type='text' className='form-control requiredfield json-col' placeholder='Last Name' name='lastName' onChange={(e) => { this.props.set_lastName(e) }} value={this.props.state.lastName} />
                        </div>
                    </div>
                </form>
            </article>
        )
    }
}
class InviteNewCarrierWithExistingData extends React.Component {
    render() {
        return (
            <article className='table-data truck-data shadow bg-white rounded' style={{ display: 'block' }}>
                <form id='new_carrier' >
                    <div className='row'>
                        <div className='col form-group' data-tip data-for={'value' + this.props.state.carrierName}  >
                            <label >Carrier Name</label>
                            <input disabled={!this.props.state.carrierName.has_carrier == true ? true : false} type='text' className='form-control requiredfield json-col' placeholder='Carrier Name' name='carrierName' onChange={(e) => { this.props.set_carrierName(e) }} value={this.props.state.carrierName} />
                            <ReactTooltip id={'value' + this.props.state.carrierName}>
                                {this.props.state.carrierName}
                            </ReactTooltip>
                        </div>
                        <div className='col form-group'>
                            <label>MC#</label>
                            <input disabled={!this.props.state.carrierName.has_mcnumber == true ? true : false} type='text' className='form-control  json-col' placeholder='MC#' name='mcNumber' onChange={(e) => { this.props.set_mcNumber(e) }} value={this.props.state.mcNumber} />
                        </div>
                        <div className='col form-group'>
                            <label>DOT#</label>
                            <input disabled={!this.props.state.carrierName.has_dotnumber == true ? true : false} type='text' className='form-control requiredfield json-col' placeholder='DOT#' name='dotNumber' onChange={(e) => { this.props.set_dotnumber(e) }} value={this.props.state.dotNumber} />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col form-group' data-tip data-for={'email' + this.props.state.email}>
                            <label>Carrier Email</label>
                            <input disabled={!this.props.state.email.has_email == true ? true : false} type='email' className='form-control requiredfield json-col' placeholder='Carrier Email' name='email' onChange={(e) => { this.props.set_email(e) }} value={this.props.state.email} />
                            <span className='shadow-input'>{this.props.state.emailError}</span>
                            <ReactTooltip id={'email' + this.props.state.email}>{this.props.state.email}</ReactTooltip>
                        </div>
                        <div className='col form-group' data-tip data-for={'firstname' + this.props.state.firstName}>
                            <label>First Name</label>
                            <input disabled={!this.props.state.email.has_firstname == true ? true : false} type='text' className='form-control requiredfield json-col' placeholder='First Name' name='firstName' onChange={(e) => { this.props.set_firstName(e) }} value={this.props.state.firstName} />
                            {this.props.state.firstName !== null ? <ReactTooltip id={'firstname' + this.props.state.firstName}>{this.props.state.firstName}</ReactTooltip> : ''}
                        </div>
                        <div className='col form-group' data-tip data-for={'lastname' + this.props.state.lastName}>
                            <label>Last Name</label>
                            <input disabled={!this.props.state.email.has_lastname == true ? true : false} type='text' className='form-control requiredfield json-col' placeholder='Last Name' name='lastName' onChange={(e) => { this.props.set_lastName(e) }} value={this.props.state.lastName} />
                            {this.props.state.lastName !== null ? <ReactTooltip id={'lastname' + this.props.state.lastName}>{this.props.state.lastName}</ReactTooltip> : ''}
                        </div>
                    </div>
                </form>
            </article>
        )
    }
}
class NewUserComponent extends React.Component {
    render() {
        return (
            <form id='new_user'>
                <div className='row'>
                    <div className='col form-group'>
                        <label>First Name</label>
                        <input type='text' className='form-control requiredfield json-col' placeholder='First Name' name='firstName' onChange={(e) => { this.props.set_firstName(e) }} value={this.props.state.firstName} />
                    </div>
                    <div className='col form-group'>
                        <label>Last Name</label>
                        <input type='text' className='form-control json-col' placeholder='Last Name' name='lastName' onChange={(e) => { this.props.set_lastName(e) }} value={this.props.state.lastName} />
                    </div>
                    <div className='col form-group'>
                        <label>Address</label>
                        <input type='text' className='form-control json-col' placeholder='Address' name='currentAddress1' onChange={(e) => { this.props.set_currentAddress1(e) }} value={this.props.state.currentAddress1} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col form-group'>
                        <label>Email</label>
                        <input type='text' className='form-control requiredfield json-col' placeholder='Email' name='emailId' onChange={(e) => { this.props.set_emailId(e) }} value={this.props.state.emailId} />
                    </div>
                    <div className='col form-group'>
                        <label>Role</label>
                        {this.props.renderRoles}
                    </div>
                    <div className='col form-group'>
                        <label>Mobile Number</label>
                        <input type='text' className='form-control json-col' placeholder='Mobile Number' name='phoneNumber' onChange={(e) => { this.props.set_phoneNumber(e) }} value={this.props.state.phoneNumber} />
                    </div>

                </div>
                <div className='row'>
                    <div className='col-4 form-group'>
                        <label>State</label>
                        {this.props.renderState}
                    </div>
                    <div className='col-8' id='roleDesc'>

                    </div>
                </div>
                <div className='row'>
                    <div className='col-4 form-group'>
                        <label>City</label>
                        <input type='text' className='form-control json-col' placeholder='City' name='City' onChange={(e) => { this.props.set_City(e) }} value={this.props.state.City} />
                    </div>
                    <div className='col-4 form-group'>
                        <label>Zip</label>
                        <input type='text' className='form-control json-col' placeholder='Zip' name='Zip' onChange={(e) => { this.props.set_Zip(e) }} value={this.props.state.Zip} />
                    </div>
                    {this.props.getEmailTempaleField}
                </div>
                <input type='hidden' className='json-col' name='userTypeId' value={this.props.state.userTypeId} />
                <input type='hidden' className='json-col' name='companyId' value={this.props.state.companyId} />
                <input type='hidden' className='json-col' name='userId' value={this.props.state.userId} />
            </form>
        )
    }
}
class NewEmailTemplate extends React.Component {
    render() {
        return (
            <form id='new_template'>
                <div className='row'>
                    <div className='col form-group'>
                        <label>Template Name</label>
                        <input type='text' className='form-control requiredfield json-col' placeholder='Template Name' name='emailTemplateName' onChange={(e) => { this.props.set_templateName(e) }} value={this.props.state.templateName} />
                    </div>
                    <div className='col form-group'>
                        <label>Template Type</label>
                        {this.props.renderTemplateTypes}
                    </div>
                    <div className='col form-group'>
                        <label>Email Accounts</label>
                        {this.props.renderAccountList}
                    </div>
                </div>
                <div className='row'>
                    <div className='col form-group'>
                        <Editor apiKey='t21f9uu8xkoyas4d800dojz0q68xajd2gcs62n8ocskunmxe'
                            init={{
                                selector: 'textarea#myTextArea',
                                paste_data_images: true,
                                automatic_uploads: false,
                                file_picker_types: 'image',
                                relative_urls: false,
                                remove_script_host: false,
                                menubar: false,
                                statusbar: false,
                                height: 300,
                                toolbar: 'undo redo preview | formatselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | insertsitelink insertsitelink1',
                                setup: editor => {
                                    editor.ui.registry.addMenuButton('insertsitelink', {
                                        text: 'Web Links',
                                        icon: 'link',
                                        fetch: function (callback) {
                                            var items = [];
                                            var myListItems = weblinkdata;
                                            function iterate(row, index) {
                                                items.push({
                                                    type: 'menuitem',
                                                    text: row['title'],
                                                    content: row['description'],
                                                    onAction: function () {
                                                        editor.insertContent(row['description']);
                                                    }
                                                })
                                            }
                                            myListItems.forEach(iterate);
                                            callback(items);
                                        },
                                        onAction: function (_) {
                                            editor.insertContent('&nbsp;<strong>Cloning...</strong>&nbsp;');
                                        }
                                    });
                                    editor.ui.registry.addMenuButton('insertsitelink1', {
                                        text: 'Template Tags',
                                        icon: 'link',
                                        fetch: function (callback) {
                                            var items = [];
                                            var myListItems = alltags;
                                            function iterate(row, index) {
                                                items.push({
                                                    type: 'menuitem',
                                                    text: row['title'],
                                                    onAction: function () {
                                                        editor.insertContent(row['description']);
                                                    }
                                                })
                                            }
                                            myListItems.forEach(iterate);
                                            callback(items);
                                        },
                                        onAction: function (_) {
                                            editor.insertContent('&nbsp;<strong>Cloning...</strong>&nbsp;');
                                        }
                                    });
                                }
                            }}
                            textareaName='templateContent'
                            value={this.props.state.templateContent}
                            onEditorChange={(e) => { this.props.handleEditorChange(e); }}
                        />
                        <input type='hidden' name='templateContentX' value={this.props.state.templateContent} />
                    </div>
                </div>
                <input type='hidden' className='json-col' name='templateId' value={this.props.state.templateId} />
            </form>
        )
    }
}
class NewEmailAccountComponent extends React.Component {
    render() {
        return (
            <form id='new_account'>
                <div className='row'>
                    <div className='col form-group'>
                        <label>Sender Name</label>
                        <input type='text' className='form-control requiredfield json-col' placeholder='First Name' name='senderName' onChange={(e) => { this.props.set_senderName(e) }} value={this.props.state.senderName} />
                    </div>
                    <div className='col form-group'>
                        <label>Sender Email</label>
                        <input type='text' className='form-control requiredfield json-col' placeholder='Sender Email' name='senderEmail' onChange={(e) => { this.props.set_senderEmail(e) }} value={this.props.state.senderEmail} />
                    </div>
                </div>
                <input type='hidden' className='json-col' name='id' value={this.props.state.accountId} />
            </form>
        )
    }
}
class BrandingComponent extends React.Component {
    render() {
        return (
            <div className='row'>
                <div className='col'>
                    <div className='row'>
                        <div className='col logo-container'>
                            <img src={`${this.props.state.profile.logo}?${new Date().getTime()}`} className='profileLogo' />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col form-group'>
                            <label>Change Logo</label>
                            <input type='file' className='form-control' id='profileLogo' accept='image/*' onChange={(e) => { this.props.saveLogo(e) }} />
                            <div class='card prod-card mt-4'>
                                <div class='card-body'>
                                    <p class='small'><strong>Logo Image Size</strong></p>
                                    <p class='small'><b>Height :</b> 35px</p>
                                    <p class='small'><b>Width :</b> 230px</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col'>
                    <div className='row'>
                        <div className='col logo-container'>
                            <img src={`${this.props.state.profile.banner}?${new Date().getTime()}`} className='profileLogo' />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col form-group'>
                            <label>Change Banner</label>
                            <input type='file' className='form-control' id='profileBanner' accept='image/*' onChange={(e) => { this.props.saveBanner(e) }} />
                            <div class='card prod-card mt-4'>
                                <div class='card-body'>
                                    <p class='small'><strong>Banner Image Size</strong></p>
                                    <p class='small'><b>Height :</b> 400px</p>
                                    <p class='small'><b>Width :</b> 400px</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col'>
                    <div className='row'>
                        <div className='col logo-container'>
                            <img src={`${this.props.state.profile.favicon}?${new Date().getTime()}`} className='profileFavicon' />
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col form-group'>
                            <label>Change Favicon</label>
                            <input type='file' className='form-control' id='profileFavicon' accept='image/*' onChange={(e) => { this.props.saveFavicon(e) }} />
                            <div class='card prod-card mt-4'>
                                <div class='card-body'>
                                    <p class='small'><strong>Favicon Image Size</strong></p>
                                    <p class='small'><b>Height :</b> 16px</p>
                                    <p class='small'><b>Width :</b> 16px</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col'>
                    <div className='row'>
                        <div className='col form-group'>
                            <label>Theme Color</label>
                            <input type='text' className='form-control json-col' placeholder='Theme Color' name='colourCode' onChange={(e) => { this.props.set_colourCode(e) }} value={this.props.state.profile.colourCode} onClick={(e) => { this.props.openColor(e) }} />
                            <SketchPicker className={this.props.state.colorClass}
                                color={this.props.state.colourCode}
                                onChangeComplete={this.props.handleChangeComplete}
                            />
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
class CompanyConfigComponent extends React.Component {
    render() {
        return (
            <article className='table-data truck-data shadow bg-white rounded'>
                <div className='row'>
                    <div className='col col-md-6'>
                        <div className='row'>
                            <div className='col-md-12'>
                                <h3>Company Details</h3>
                            </div>
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='companyName' component={renderField} className='form-control' label='Company Name' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='companyId' component={renderField} className='form-control' label='Company ID' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='companyType' component={renderField} className='form-control' label='Company Type' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='mc' component={renderField} className='form-control' label='MC' />
                        </div>
                        <div className='form-group'>
                            <Field type='number' name='dot' component={renderField} className='form-control' label='DOT' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='scac' component={renderField} className='form-control' label='SCAC' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='email' component={renderField} className='form-control' label='Support Email' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='enableSubscription' component={renderCheckbox1} className='form-control' label='Enable Subscription' />
                        </div>
                    </div>
                    <div className='col col-md-6'>
                        <div className='row'>
                            <div className='col-md-12'>
                                <h3>Company Contacts</h3>
                            </div>
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='currentAddress1' component={renderField} className='form-control' label='Address' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='city' component={renderField} label='City' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='state' controlClass='requiredfield' component={renderField} label='State' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='zipcode' component={renderField} className='form-control' label='Zip' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='companyEmail' component={renderField} controlClass='requiredfield' label='Email' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='websiteAddress' component={renderField} label='Website' />
                        </div>
                        <div className='form-group'>
                            <Field type='text' name='phoneNumber' component={renderField} className='form-control' label='Phone Number' />
                        </div>
                    </div>
                    <div className='row'>
                    </div>
                </div>
                <div className='col text-center'>
                    <button type='button' className='btn btn-primary' disabled>Save</button>
                </div>
            </article>
        )
    }
}
class Carrierprofiledetailsview extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Fragment>
                <form className='tg-forms'>
                    <article className='table-data truck-data shadow bg-white rounded'>
                        <div className='row'>
                            <div className='col col-md-3'>
                                <div className='form-group'>
                                    <Field type='text' name='companyName' component={renderField} label='Carrier Name' />
                                </div>
                                <div className='form-group'>
                                    <Field type='text' name='specialityTypeName' component={renderField} label='Speciality' />
                                </div>
                                <div className='form-group'>
                                    <Field type='text' name='currentAddress1' component={renderField} label='Current Address' />
                                </div>
                            </div>
                            <div className='col col-md-3'>
                                <div className='form-group'>
                                    <Field type='text' name='companyEmail' component={renderField} label='Email' />
                                </div>
                                <div className='form-group'>
                                    <Field type='text' name='phoneNumber' component={renderField} label='Primary Phone' />
                                </div>
                                <div className='form-group'>
                                    <Field type='text' name='altPhoneNumber' component={renderField} label='Secondary Phone' />
                                </div>
                            </div>
                            <div className='col col-md-3'>
                                <div className='form-group'>
                                    <Field type='text' name='currentCityName' component={renderField} label='City' />
                                </div>
                                <div className='form-group'>
                                    <Field type='text' name='faxNumber' component={renderField} label='Fax' />
                                </div>
                                <div className='form-group'>
                                    <Field type='text' name='websiteAddress' component={renderField} label='Website' />
                                </div>
                            </div>
                            <div className='col col-md-3'>
                                <div className='form-group'>
                                    <Field type='text' name='currentStateName' component={renderField} label='State' />
                                </div>
                                <div className='form-group'>
                                    <Field type='text' name='currentZipValue' component={renderField} label='Zip Code' />
                                </div>
                                <div className='form-group'>
                                    <Field type='text' name='currentCountryName' component={renderField} label='Country' />
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='col-md-12'>
                                <Field name='instructions' label='Description' component={renderTextArea} />
                            </div>
                        </div>
                    </article>
                </form>
            </Fragment>
        )
    }
}
const PageOneForm = reduxForm({
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true,
})(Carrierprofiledetailsview)
const mapStateToProps = (state) => {
    return {
        data: state.userReducer.data,
        CityVal: state.loadReducer.CityVal,
        ZipVal: state.loadReducer.ZipVal,
        form: 'profiledetailsview',
        validate
    }
}
const PageOneFormContainerComponent = connect(mapStateToProps)(PageOneForm)

class renderField extends Component {
    render() {
        const { input, label, controlClass, id, labelnotavailabe, data, disable, type, meta: { touched, error } } = this.props
        return (
            <div className={`form-group ${(touched && error) ? 'error' : ''}`}>
                {(labelnotavailabe) ? '' : <label>{label}</label>}
                <div>
                    <input {...input} text={`${(data) ? data : ''}`} id={`${(id) ? id : ''}`} placeholder={label} type={type} disabled={`${(disable) ? disable : ''}`} className={`form-control ${(controlClass) ? controlClass : ''}`} />
                    {touched && (error && <span className='error'>{error}</span>)}
                </div>
            </div>
        )
    }
}
class renderCheckbox1 extends Component {
    render() {
        const { input, label, name, id, value, type, controlClass, meta: { touched, error } } = this.props
        return (
            <div className='parentDivCss'>
                <input type='checkbox' id={id} name={name} value={value} className='checkBoxCss' />&ensp;
                <label for='vehicle3' className='labelCss'> {label}</label>
                {touched && (error && <span className='error'>{error}</span>)}
            </div>
        )
    }
}
class ViewDriverPOPUPComponent extends Component {
    render() {
        return (
            <form id='view_driver'>
                <div className='row'>
                    <div className='col form-group'>
                        <label>First Name</label>
                        <input type='text' className='viewTruckCss  json-col' placeholder='First Name' name='firstName' value={this.props.state.firstName} />
                    </div>
                    <div className='col form-group'>
                        <label>Last Name</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Last Name' name='lastName' value={this.props.state.lastName} />
                    </div>
                    <div className='col form-group'>
                        <label>License Exp. Date</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='License Exp. Date' name='drivingLicenseExpDate' value={this.props.state.drivingLicenseExpDate} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col form-group'>
                        <label>License State</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='License State' name='drivingLicenseStateCode' value={this.props.state.drivingLicenseStateCode} />
                    </div>
                    <div className='col form-group'>
                        <label>License Number</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='License Number' name='drivingLicenseNo' value={this.props.state.drivingLicenseNo} />
                    </div>
                    <div className='col  form-group'>
                        <label>Email</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Email' name='emailId' value={this.props.state.emailId} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col  form-group'>
                        <label>Mobile Number</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Mobile Number' name='phoneNumber' value={this.props.state.phoneNumber} />
                    </div>
                    <div className='col form-group'>
                        <label>State</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='State' name='state' value={this.props.state.state} />
                    </div>
                    <div className='col form-group'>
                        <label>City</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='City' name='city' value={this.props.state.city} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col form-group'>
                        <label>Address</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Address' name='address' value={this.props.state.address} />
                    </div>
                    <div className='col form-group'>
                        <label>ELD Provider</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='ELD Provider' name='eldProvider' value={this.props.state.eldProvider} />
                    </div>
                </div>
            </form>
        )
    }
}
class LoadDropdown extends Component {
    getRow(data, value) {
        switch (value) {
            case 'cid': return (
                data ? (data.map(function (row, index) {
                    return (
                        <option key={index} data-lanedetailsindex={index} value={row.companyId}>{row.companyName}</option>
                    )
                })) : (<option></option>)
            )
            case 'id': return (
                data ? (data.map(function (row, index) {
                    return (
                        <option key={index} data-lanedetailsindex={index} value={row.id}>{row.name}</option>
                    )
                })) : (<option></option>)
            )
            case 'c_id': return (
                data ? (data.map(function (row, index) {
                    return (
                        <option key={index} data-lanedetailsindex={index} value={row.Company_Id}>{row.Company_Name}</option>
                    )
                })) : (<option></option>)
            )
            case 'lane': return (
                data ? (data.map(function (row, index) {
                    return (
                        <option key={index} value={row.laneId}>{row.selectLane}</option>
                    )
                })) : (<option></option>)
            )
            case 'cState': return (
                data ? (data.map(function (row, index) {
                    return (
                        <option key={index} value={row.id}>{row.name}</option>
                    )
                })) : (<option value=''>Loading...</option>)
            )
            case 'cCity': return (
                data.length > 0 ? (data.map(function (row, index) {
                    return (
                        <option key={index} value={row.cityId}>{row.cityName}</option>
                    )
                })) : (<option value=''>Loading...</option>)
            )
            case 'cZip': return (
                data.length > 0 ? (data.map(function (row, index) {
                    return (
                        <option key={index} value={row.zipId}>{row.zipValue}</option>
                    )
                })) : (<option value=''>Loading...</option>)
            )
        }
    }
    render() {
        let value = this.props.value
        return (
            <div className='col form-group'>
                <label>{this.props.label}</label>
                <select onChange={this.props.onChange} className='form-control json-col' id={this.props.id} name={this.props.name}>
                    <option value='' hidden>{this.props.placeHolder}</option>
                    {
                        this.getRow(this.props.data, value)
                    }
                </select>
            </div>
        )
    }
}
class LoadInput extends Component {
    render() {
        return (
            <div className='col form-group'>
                <label>{this.props.label}</label>
                <input name={this.props.name} type={this.props.type} className={this.props.class} placeholder={this.props.placeHolder} />
            </div>
        )
    }
}
class ViewTruckPOPUPComponent extends Component {
    render() {
        return (
            <form id='view_truck'>
                <div className='row'>
                    <div className='col form-group'>
                        <label>Truck/Asset No</label>
                        <input type='text' className='viewTruckCss  json-col' placeholder='Truck/Asset No' name='assetNumber' value={this.props.state.assetNumber} />
                    </div>
                    <div className='col form-group'>
                        <label>License Plate No</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='License Plate No' name='licensePlateNo' value={this.props.state.licensePlateNo} />
                    </div>
                    <div className='col  form-group'>
                        <label>VIN</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='VIN' name='identificationNo' value={this.props.state.identificationNo} />
                    </div>
                    <div className='col  form-group'>
                        <label>Device ID</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Device ID' name='assetSerialNo' value={this.props.state.assetSerialNo} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col form-group'>
                        <label>Asset Name</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Asset Name' name='assetName' value={this.props.state.assetName} />
                    </div>
                    <div className='col form-group'>
                        <label>Asset Type</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Asset Type' name='assetType' value={this.props.state.assetType} />
                    </div>
                    <div className='col form-group'>
                        <label>License State</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='License State' name='licenseState' value={this.props.state.licenseState} />
                    </div>
                    <div className='col form-group'>
                        <label>ELD Provider</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='ELD Provider' name='eldProviderDisplayName' value={this.props.state.eldProviderDisplayName} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col form-group'>
                        <label>Make</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Make' name='make' value={this.props.state.make} />
                    </div>
                    <div className='col form-group'>
                        <label>Model</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Model' name='model' value={this.props.state.model} />
                    </div>
                    <div className='col form-group'>
                        <label>Year</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Year' name='modelYear' value={this.props.state.modelYear} />
                    </div>
                    <div className='col form-group'>
                        <label>Color</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Color' name='color' value={this.props.state.color} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col  form-group'>
                        <label>Manufacturer</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Manufacturer' name='manufacturer' value={this.props.state.manufacturer} />
                    </div>
                    <div className='col form-group'>
                        <label>Axles</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Axles' name='axles' value={this.props.state.axles} />
                    </div>
                    <div className='col form-group'>
                        <label>Size</label>
                        <input type='text' className='viewTruckCss json-col' placeholder='Size' name='size' value={this.props.state.size} />
                    </div>
                </div>
            </form>
        )
    }
}

module.exports = {
    SearchComponent,
    CarrierInfo,
    EldSelection,
    AuthorizeEldRequired,
    AuthorizeEldAdditional,
    UseExistingCredentials,
    NewCredentials,
    TabsBar,
    TableDataComponent,
    SearchCarrierComponent,
    HeaderBar,
    DashboardTrucksComponent,
    GetCarrier,
    GetEld,
    DashboardTrucksTrmComponent,
    BreadCrumpTrail,
    BreadCrumpTrailButton,
    DashboardGoogleMap,
    InviteNewCarrierWithOutExistingData,
    InviteNewCarrierWithExistingData,
    NewUserComponent,
    NewEmailAccountComponent,
    NewEmailTemplate,
    Carrierprofiledetailsview,
    BrandingComponent,
    CompanyConfigComponent,
    renderField,
    renderCheckbox1,
    ViewDriverPOPUPComponent,
    ViewTruckPOPUPComponent,
    LoadDropdown,
    LoadInput,
    PageOneFormContainerComponent
}
