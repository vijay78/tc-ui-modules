import React from 'react';
class GetCarrier extends React.Component {
    handleChange = (e) => {
        this.props.onChange(e)
    }
    render() {
        console.log("usertype",this.props.userType)
        console.log("dataasa", this.props.state.carrierNames);
        let userType = this.props.userType
        return (
            <div className="carrierDropdown">
                <div className="col col-md-12">
                    <label htmlFor="carrierdropdownLabel" className="carrierdropdownLabel" ><b>Select Carrier</b></label>
                    <select disabled={this.props.state.disabled} className="form-control" id="findCarrierId" onChange={(e) => { this.handleChange(e) }}>
                        <option value="0">Select Carrier</option>
                        {
                            this.props.state.carrierNames.length > 0 ? (this.props.state.carrierNames.map(function (company, index) {
                                return (
                                    <option key={index} value={userType === 'broker'?company.Invited_Company_Id : company.Company_Id}>{company.Company_Name}</option>
                                )
                            })) : (<option>Loading.....</option>)
                        }
                    </select>
                </div>
            </div>
        );
    }
}

module.exports = {
    GetCarrier
}