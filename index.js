"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var GetCarrier = /*#__PURE__*/function (_React$Component) {
  _inherits(GetCarrier, _React$Component);

  var _super = _createSuper(GetCarrier);

  function GetCarrier() {
    var _this;

    _classCallCheck(this, GetCarrier);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "handleChange", function (e) {
      _this.props.onChange(e);
    });

    return _this;
  }

  _createClass(GetCarrier, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      console.log("usertype", this.props.userType);
      console.log("dataasa", this.props.state.carrierNames);
      var userType = this.props.userType;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "carrierDropdown"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-12"
      }, /*#__PURE__*/_react["default"].createElement("label", {
        htmlFor: "carrierdropdownLabel",
        className: "carrierdropdownLabel"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Select Carrier")), /*#__PURE__*/_react["default"].createElement("select", {
        disabled: this.props.state.disabled,
        className: "form-control",
        id: "findCarrierId",
        onChange: function onChange(e) {
          _this2.handleChange(e);
        }
      }, /*#__PURE__*/_react["default"].createElement("option", {
        value: "0"
      }, "Select Carrier"), this.props.state.carrierNames.length > 0 ? this.props.state.carrierNames.map(function (company, index) {
        return /*#__PURE__*/_react["default"].createElement("option", {
          key: index,
          value: userType === 'broker' ? company.Invited_Company_Id : company.Company_Id
        }, company.Company_Name);
      }) : /*#__PURE__*/_react["default"].createElement("option", null, "Loading....."))));
    }
  }]);

  return GetCarrier;
}(_react["default"].Component);

module.exports = {
  GetCarrier: GetCarrier
};
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

var _react = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _reduxForm = require("redux-form");

var _reactIcofont = _interopRequireDefault(require("react-icofont"));

var _reactTooltip = _interopRequireDefault(require("react-tooltip"));

var _reactBootstrapTable2Toolkit = _interopRequireWildcard(require("react-bootstrap-table2-toolkit"));

var _reactBootstrapTableNext = _interopRequireDefault(require("react-bootstrap-table-next"));

require("react-bootstrap-table/dist/react-bootstrap-table-all.min.css");

var _reactBootstrapTable2Paginator = _interopRequireDefault(require("react-bootstrap-table2-paginator"));

var _api = require("@react-google-maps/api");

var _tinymceReact = require("@tinymce/tinymce-react");

var _reactColor = require("react-color");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var scriptFn = function scriptFn() {};

var SearchBar = _reactBootstrapTable2Toolkit.Search.SearchBar;

var SearchComponent = /*#__PURE__*/function (_React$Component) {
  _inherits(SearchComponent, _React$Component);

  var _super = _createSuper(SearchComponent);

  function SearchComponent() {
    _classCallCheck(this, SearchComponent);

    return _super.apply(this, arguments);
  }

  _createClass(SearchComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      scriptFn();
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      return /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("form", {
        onSubmit: function onSubmit(e) {
          e.preventDefault();
        }
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group form-icon"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        onKeyUp: function onKeyUp(e) {
          _this.props.onKeyUp(e);
        },
        className: "eldSearch",
        placeholder: "Search "
      }), /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "search",
        style: {
          marginLeft: '-2rem'
        }
      }), " ", /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "icofont-info-circle",
        style: {
          marginLeft: '2rem',
          color: 'var(--main-color)',
          fontSize: 'medium'
        }
      }), /*#__PURE__*/_react["default"].createElement("i", {
        className: "eldText"
      }, "Select your ELD providers from the list provided."))));
    }
  }]);

  return SearchComponent;
}(_react["default"].Component);

var CarrierInfo = /*#__PURE__*/function (_React$Component2) {
  _inherits(CarrierInfo, _React$Component2);

  var _super2 = _createSuper(CarrierInfo);

  function CarrierInfo(props) {
    _classCallCheck(this, CarrierInfo);

    return _super2.call(this, props);
  }

  _createClass(CarrierInfo, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      scriptFn();
    }
  }, {
    key: "carrierCodes",
    value: function carrierCodes() {
      var carrierCode = [];
      carrierCode.push(this.props.passingData.invitationData.carrierInfo.carrierCodes.map(function (val, index) {
        return /*#__PURE__*/_react["default"].createElement("div", {
          className: "col",
          key: index
        }, /*#__PURE__*/_react["default"].createElement("h5", null, "# ", val.codeType, "-", val.codeValue));
      }));
      return carrierCode;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$passingDa, _this$props$passingDa2, _this$props$passingDa3, _this$props$passingDa4, _this$props$passingDa5, _this$props$passingDa6, _this$props$passingDa7, _this$props$passingDa8;

      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("h5", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "icofont-user-alt-3",
        id: "iconCOB",
        style: {
          fontSize: '17px',
          fontWeight: '400'
        }
      }), " \xA0", (_this$props$passingDa = this.props.passingData.invitationData) === null || _this$props$passingDa === void 0 ? void 0 : (_this$props$passingDa2 = _this$props$passingDa.contactInfo) === null || _this$props$passingDa2 === void 0 ? void 0 : _this$props$passingDa2.firstName, "\xA0", (_this$props$passingDa3 = this.props.passingData.invitationData) === null || _this$props$passingDa3 === void 0 ? void 0 : (_this$props$passingDa4 = _this$props$passingDa3.contactInfo) === null || _this$props$passingDa4 === void 0 ? void 0 : _this$props$passingDa4.lastName)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("h5", null, " ", /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "icofont-truck-alt",
        id: "iconCOB",
        style: {
          fontSize: '17px',
          fontWeight: '400'
        }
      }), " \xA0", (_this$props$passingDa5 = this.props.passingData.invitationData) === null || _this$props$passingDa5 === void 0 ? void 0 : (_this$props$passingDa6 = _this$props$passingDa5.carrierInfo) === null || _this$props$passingDa6 === void 0 ? void 0 : _this$props$passingDa6.carrierName)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("h5", null, " ", /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "icofont-envelope",
        id: "iconCOB",
        style: {
          fontSize: '17px',
          fontWeight: '400'
        }
      }), " \xA0", (_this$props$passingDa7 = this.props.passingData.invitationData) === null || _this$props$passingDa7 === void 0 ? void 0 : (_this$props$passingDa8 = _this$props$passingDa7.contactInfo) === null || _this$props$passingDa8 === void 0 ? void 0 : _this$props$passingDa8.email)), this.carrierCodes());
    }
  }]);

  return CarrierInfo;
}(_react["default"].Component);

var EldSelection = /*#__PURE__*/function (_React$Component3) {
  _inherits(EldSelection, _React$Component3);

  var _super3 = _createSuper(EldSelection);

  function EldSelection(props) {
    var _this2;

    _classCallCheck(this, EldSelection);

    _this2 = _super3.call(this, props);

    _defineProperty(_assertThisInitialized(_this2), "handleChange", function (e, data, index) {
      _this2.props.onChange(e, data, index);
    });

    return _this2;
  }

  _createClass(EldSelection, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      scriptFn();
    }
  }, {
    key: "showEld",
    value: function showEld() {
      var _this3 = this;

      var Eld = [];
      var compLength = this.props.eldData.invitationData.eldInfo.length;
      var noOfRows = Math.ceil(compLength / 4);
      var arr = Array(4).fill(0);

      var _loop = function _loop(j) {
        Eld.push( /*#__PURE__*/_react["default"].createElement("div", {
          className: "row",
          style: {
            width: '100%'
          },
          key: j + 1
        }, arr.map(function (val, index) {
          return /*#__PURE__*/_react["default"].createElement("div", {
            className: "col-md-3",
            key: index + 2
          }, _this3.getData(j, index));
        })));
      };

      for (var j = 0; j < noOfRows; j++) {
        _loop(j);
      }

      return Eld;
    }
  }, {
    key: "getData",
    value: function getData(j, i) {
      var _this4 = this;

      var index = j * 4 + i;
      var arr = [];
      var path = this.props.envData + 'elds/logo/';

      if (this.props.eldData.invitationData.eldInfo[index] !== undefined || null) {
        var _this$props$eldData$i, _this$props$eldData$i2, _this$props$eldData$i3, _this$props$eldData$i4, _this$props$eldData$i5, _this$props$eldData$i6, _this$props$eldData$i7, _this$props$eldData$i8, _this$props$eldData$i9, _this$props$eldData$i10, _this$props$eldData$i11;

        arr.push( /*#__PURE__*/_react["default"].createElement("div", _defineProperty({
          className: "row",
          style: {
            opacity: ((_this$props$eldData$i = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i === void 0 ? void 0 : _this$props$eldData$i.overallStatus) === 'VERIFIED' || !this.diableCheck((_this$props$eldData$i2 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i2 === void 0 ? void 0 : _this$props$eldData$i2.eldProviderId) && this.props.eldData.checkedData.length > 4 ? '.5' : '1'
          },
          key: (_this$props$eldData$i3 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i3 === void 0 ? void 0 : _this$props$eldData$i3.eldProviderId
        }, "key", (_this$props$eldData$i4 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i4 === void 0 ? void 0 : _this$props$eldData$i4.eldProviderId), /*#__PURE__*/_react["default"].createElement("div", {
          className: "col-md-1"
        }, /*#__PURE__*/_react["default"].createElement("input", {
          type: "checkbox",
          name: (_this$props$eldData$i5 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i5 === void 0 ? void 0 : _this$props$eldData$i5.eldProviderId,
          defaultChecked: this.props.eldData.invitationData.eldInfo[index].overallStatus === 'VERIFIED',
          readOnly: ((_this$props$eldData$i6 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i6 === void 0 ? void 0 : _this$props$eldData$i6.overallStatus) === 'VERIFIED' || !this.diableCheck((_this$props$eldData$i7 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i7 === void 0 ? void 0 : _this$props$eldData$i7.eldProviderId) && this.props.eldData.checkedData.length > 4,
          className: "checkBox",
          onChange: function onChange(e) {
            return _this4.handleChange(e, _this4.props.eldData.invitationData.eldInfo[index], index);
          }
        })), /*#__PURE__*/_react["default"].createElement("div", {
          className: "col-md-9",
          style: {
            bottom: '28px'
          }
        }, /*#__PURE__*/_react["default"].createElement("div", {
          style: {
            width: '60%',
            marginBottom: '5px'
          },
          "data-tip": true,
          "data-for": 'image' + ((_this$props$eldData$i8 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i8 === void 0 ? void 0 : _this$props$eldData$i8.eldVendor)
        }, /*#__PURE__*/_react["default"].createElement("img", {
          src: path + ((_this$props$eldData$i9 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i9 === void 0 ? void 0 : _this$props$eldData$i9.eldVendor),
          className: "image"
        }), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
          id: 'image' + ((_this$props$eldData$i10 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i10 === void 0 ? void 0 : _this$props$eldData$i10.eldVendor)
        }, (_this$props$eldData$i11 = this.props.eldData.invitationData.eldInfo[index]) === null || _this$props$eldData$i11 === void 0 ? void 0 : _this$props$eldData$i11.eldVendor)))));
        return arr;
      }
    }
  }, {
    key: "diableCheck",
    value: function diableCheck(data) {
      for (var i = 0; i < this.props.eldData.checkedData.length; i++) {
        if (this.props.eldData.checkedData[i].eldProviderId === data) {
          return true;
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement("div", null, this.showEld());
    }
  }]);

  return EldSelection;
}(_react["default"].Component);

var AuthorizeEldRequired = /*#__PURE__*/function (_React$Component4) {
  _inherits(AuthorizeEldRequired, _React$Component4);

  var _super4 = _createSuper(AuthorizeEldRequired);

  function AuthorizeEldRequired(props) {
    var _this5;

    _classCallCheck(this, AuthorizeEldRequired);

    _this5 = _super4.call(this, props);

    _defineProperty(_assertThisInitialized(_this5), "handleChecked", function (e, item, i) {
      _this5.props.onChange(e, item, i);
    });

    return _this5;
  }

  _createClass(AuthorizeEldRequired, [{
    key: "preventCheck",
    value: function preventCheck(e, item) {
      if (e.target.checked === true) {
        e.target.checked = false;
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        return false;
      }

      e.target.checked = true;
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
      return false;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$stateData,
          _this6 = this,
          _this$props$stateData2,
          _this$props$stateData3;

      return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("h5", {
        className: "subTitleCss"
      }, "Required Data Permissions \xA0", /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "icofont-info-circle",
        style: {
          color: 'var(--main-color)',
          fontSize: 'medium'
        }
      })), /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "form-check-input",
        type: "checkbox",
        name: this.props.item.eldDataSharing.key,
        id: "authenticationData",
        readOnly: !this.props.item.eldDataSharing.authenticationDataEnabled || ((_this$props$stateData = this.props.stateData.fieldDisable[this.props.item]) === null || _this$props$stateData === void 0 ? void 0 : _this$props$stateData.diableField),
        defaultChecked: this.props.item.eldDataSharing.authenticationData,
        onChange: function onChange(e) {
          var _this6$props$stateDat;

          !_this6.props.item.eldDataSharing.authenticationDataEnabled || (_this6$props$stateDat = _this6.props.stateData.fieldDisable[_this6.props.item]) !== null && _this6$props$stateDat !== void 0 && _this6$props$stateDat.diableField ? _this6.preventCheck(e, _this6.props.i, _this6.props.item) : _this6.handleChecked(e, _this6.props.item, _this6.props.i);
        }
      }), /*#__PURE__*/_react["default"].createElement("label", {
        className: "form-check-label"
      }, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "icofont-lock iconName",
        id: "iconCOB"
      }), " \xA0Authentication")), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "form-check-input",
        type: "checkbox",
        id: "locationData",
        readOnly: !this.props.item.eldDataSharing.locationDataEnabled || ((_this$props$stateData2 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData2 === void 0 ? void 0 : _this$props$stateData2.diableField),
        defaultChecked: this.props.item.eldDataSharing.locationData,
        onChange: function onChange(e) {
          var _this6$props$stateDat2;

          !_this6.props.item.eldDataSharing.locationDataEnabled || (_this6$props$stateDat2 = _this6.props.stateData.fieldDisable[_this6.props.i]) !== null && _this6$props$stateDat2 !== void 0 && _this6$props$stateDat2.diableField ? _this6.preventCheck(e, _this6.props.i, _this6.props.item) : _this6.handleChecked(e, _this6.props.item, _this6.props.i);
        }
      }), /*#__PURE__*/_react["default"].createElement("label", {
        className: "form-check-label "
      }, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "icofont-location-pin iconName ",
        id: "iconCOB"
      }), " \xA0Location Data")), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "form-check-input",
        type: "checkbox",
        id: "vehicleData",
        readOnly: !this.props.item.eldDataSharing.vehicleDataEnabled || ((_this$props$stateData3 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData3 === void 0 ? void 0 : _this$props$stateData3.diableField),
        defaultChecked: this.props.item.eldDataSharing.vehicleData,
        onChange: function onChange(e) {
          var _this6$props$stateDat3;

          !_this6.props.item.eldDataSharing.vehicleDataEnabled || (_this6$props$stateDat3 = _this6.props.stateData.fieldDisable[_this6.props.i]) !== null && _this6$props$stateDat3 !== void 0 && _this6$props$stateDat3.diableField ? _this6.preventCheck(e, _this6.props.i, _this6.props.item) : _this6.handleChecked(e, _this6.props.item, _this6.props.i);
        }
      }), /*#__PURE__*/_react["default"].createElement("label", {
        className: "form-check-label "
      }, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "icofont-truck-alt iconName ",
        id: "iconCOB"
      }), " \xA0Vehicle Data")), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("label", {
        className: "form-check-label"
      })))));
    }
  }]);

  return AuthorizeEldRequired;
}(_react["default"].Component);

var AuthorizeEldAdditional = /*#__PURE__*/function (_React$Component5) {
  _inherits(AuthorizeEldAdditional, _React$Component5);

  var _super5 = _createSuper(AuthorizeEldAdditional);

  function AuthorizeEldAdditional(props) {
    var _this7;

    _classCallCheck(this, AuthorizeEldAdditional);

    _this7 = _super5.call(this, props);

    _defineProperty(_assertThisInitialized(_this7), "handleChecked", function (e, item, i) {
      _this7.props.onChange(e, item, i);
    });

    return _this7;
  }

  _createClass(AuthorizeEldAdditional, [{
    key: "preventCheck",
    value: function preventCheck(e, item) {
      if (e.target.checked === true) {
        e.target.checked = false;
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        return false;
      }

      e.target.checked = true;
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
      return false;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$stateData4,
          _this8 = this,
          _this$props$stateData5,
          _this$props$stateData6,
          _this$props$stateData7;

      return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("h5", {
        className: "subTitleCss"
      }, "Additional Data Permissions"), /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "form-check-input",
        type: "checkbox",
        id: "driverData",
        readOnly: !this.props.item.eldDataSharing.driverDataEnabled || ((_this$props$stateData4 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData4 === void 0 ? void 0 : _this$props$stateData4.diableField),
        defaultChecked: this.props.item.eldDataSharing.driverData,
        onChange: function onChange(e) {
          var _this8$props$stateDat;

          !_this8.props.item.eldDataSharing.driverDataEnabled || (_this8$props$stateDat = _this8.props.stateData.fieldDisable[_this8.props.i]) !== null && _this8$props$stateDat !== void 0 && _this8$props$stateDat.diableField || _this8.props.stateData.isSkipData ? _this8.preventCheck(e, _this8.props.i, _this8.props.item) : _this8.handleChecked(e, _this8.props.item, _this8.props.i);
        }
      }), /*#__PURE__*/_react["default"].createElement("label", {
        className: "form-check-label"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "icofont-user-alt-7 iconName ",
        id: "iconCOB"
      }), " \xA0Driver Data")), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "form-check-input",
        type: "checkbox",
        id: "hoursOfServiceData",
        readOnly: !this.props.item.eldDataSharing.hoursOfServiceDataEnabled || ((_this$props$stateData5 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData5 === void 0 ? void 0 : _this$props$stateData5.diableField),
        defaultChecked: this.props.item.eldDataSharing.hoursOfServiceData,
        onChange: function onChange(e) {
          var _this8$props$stateDat2;

          !_this8.props.item.eldDataSharing.hoursOfServiceDataEnabled || (_this8$props$stateDat2 = _this8.props.stateData.fieldDisable[_this8.props.i]) !== null && _this8$props$stateDat2 !== void 0 && _this8$props$stateDat2.diableField || _this8.props.stateData.isSkipData ? _this8.preventCheck(e, _this8.props.i, _this8.props.item) : _this8.handleChecked(e, _this8.props.item, _this8.props.i);
        }
      }), /*#__PURE__*/_react["default"].createElement("label", {
        className: "form-check-label"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "icofont-clock-time iconName",
        id: "iconCOB"
      }), " \xA0Hours of Service")), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "form-check-input",
        type: "checkbox",
        id: "engineData",
        readOnly: !this.props.item.eldDataSharing.engineDataEnabled || ((_this$props$stateData6 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData6 === void 0 ? void 0 : _this$props$stateData6.diableField),
        onChange: function onChange(e) {
          var _this8$props$stateDat3;

          !_this8.props.item.eldDataSharing.engineDataEnabled || (_this8$props$stateDat3 = _this8.props.stateData.fieldDisable[_this8.props.i]) !== null && _this8$props$stateDat3 !== void 0 && _this8$props$stateDat3.diableField || _this8.props.stateData.isSkipData ? _this8.preventCheck(e, _this8.props.i, _this8.props.item) : _this8.handleChecked(e, _this8.props.item, _this8.props.i);
        },
        defaultChecked: this.props.item.eldDataSharing.engineData
      }), /*#__PURE__*/_react["default"].createElement("label", {
        className: "form-check-label"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "icofont-spanner iconName",
        id: "iconCOB"
      }), " \xA0Engine Data")), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "form-check-input",
        type: "checkbox",
        id: "dvirData",
        readOnly: !this.props.item.eldDataSharing.dvirDataEnabled || ((_this$props$stateData7 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData7 === void 0 ? void 0 : _this$props$stateData7.diableField),
        onChange: function onChange(e) {
          var _this8$props$stateDat4;

          !_this8.props.item.eldDataSharing.dvirDataEnabled || (_this8$props$stateDat4 = _this8.props.stateData.fieldDisable[_this8.props.i]) !== null && _this8$props$stateDat4 !== void 0 && _this8$props$stateDat4.diableField || _this8.props.stateData.isSkipData ? _this8.preventCheck(e, _this8.props.i, _this8.props.item) : _this8.handleChecked(e, _this8.props.item, _this8.props.i);
        },
        defaultChecked: this.props.item.eldDataSharing.dvirData
      }), /*#__PURE__*/_react["default"].createElement("label", {
        className: "form-check-label"
      }, /*#__PURE__*/_react["default"].createElement("i", {
        className: "icofont-ebook iconName",
        id: "iconCOB"
      }), " \xA0DVIR")))));
    }
  }]);

  return AuthorizeEldAdditional;
}(_react["default"].Component);

var UseExistingCredentials = /*#__PURE__*/function (_React$Component6) {
  _inherits(UseExistingCredentials, _React$Component6);

  var _super6 = _createSuper(UseExistingCredentials);

  function UseExistingCredentials(props) {
    var _this9;

    _classCallCheck(this, UseExistingCredentials);

    _this9 = _super6.call(this, props);

    _defineProperty(_assertThisInitialized(_this9), "handleExitingEld", function (item, i) {
      _this9.props.onChange(item, i);
    });

    return _this9;
  }

  _createClass(UseExistingCredentials, [{
    key: "preventCheck",
    value: function preventCheck(e, item) {
      if (e.target.checked === true) {
        e.target.checked = false;
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        return false;
      }

      e.target.checked = true;
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
      return false;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$stateData8,
          _this$props$stateData9,
          _this10 = this;

      return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "form-check-input",
        type: "checkbox",
        id: "exiteldChcekbox",
        readOnly: ((_this$props$stateData8 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData8 === void 0 ? void 0 : _this$props$stateData8.diableField) || this.props.stateData.isSkipData,
        defaultChecked: (_this$props$stateData9 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData9 === void 0 ? void 0 : _this$props$stateData9.diableField,
        onChange: function onChange(e) {
          var _this10$props$stateDa;

          (_this10$props$stateDa = _this10.props.stateData.fieldDisable[_this10.props.i]) !== null && _this10$props$stateDa !== void 0 && _this10$props$stateDa.diableField || _this10.props.stateData.isSkipData ? _this10.preventCheck(e, _this10.props.i, _this10.props.item) : _this10.handleExitingEld(_this10.props.item, _this10.props.i);
        }
      }), /*#__PURE__*/_react["default"].createElement("label", {
        className: "div-content"
      }, "\xA0 Utilize my existing ELD provider credentials")));
    }
  }]);

  return UseExistingCredentials;
}(_react["default"].Component);

var NewCredentials = /*#__PURE__*/function (_React$Component7) {
  _inherits(NewCredentials, _React$Component7);

  var _super7 = _createSuper(NewCredentials);

  function NewCredentials(props) {
    var _this11;

    _classCallCheck(this, NewCredentials);

    _this11 = _super7.call(this, props);

    _defineProperty(_assertThisInitialized(_this11), "handleInput", function (e, item, i) {
      _this11.props.onChange(e, item, i);
    });

    return _this11;
  }

  _createClass(NewCredentials, [{
    key: "render",
    value: function render() {
      var _this$props$stateData10,
          _this12 = this,
          _this$props$innsideIt,
          _this$props$innsideIt2,
          _this$props$stateData11,
          _this$props$innsideIt3;

      return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, this.props.item.eldVendor === 'Samsara' ? /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, "  ", /*#__PURE__*/_react["default"].createElement("span", {
        className: "samsaraPrefix"
      }, "samsara_api_"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control samasarlabelCssForVerifying  requiredfield json-col",
        placeholder: this.props.innsideItem.paramIsRequired ? this.props.innsideItem.paramName + ' *' : this.props.innsideItem.paramName,
        id: this.props.innsideItem.paramName,
        disabled: !this.props.innsideItem.paramIsRequired,
        readOnly: (_this$props$stateData10 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData10 === void 0 ? void 0 : _this$props$stateData10.diableField,
        onChange: function onChange(e) {
          _this12.handleInput(e, _this12.props.item, _this12.props.j);
        },
        value: (_this$props$innsideIt = this.props.innsideItem) === null || _this$props$innsideIt === void 0 ? void 0 : _this$props$innsideIt.paramDefaultValue
      })) : /*#__PURE__*/_react["default"].createElement("input", {
        type: (_this$props$innsideIt2 = this.props.innsideItem) !== null && _this$props$innsideIt2 !== void 0 && _this$props$innsideIt2.paramName.includes('password') ? 'password' : 'text',
        className: "form-control labelCssForVerifying  requiredfield json-col",
        placeholder: this.props.innsideItem.paramIsRequired ? this.props.innsideItem.paramName + ' *' : this.props.innsideItem.paramName,
        id: this.props.innsideItem.paramName,
        disabled: !this.props.innsideItem.paramIsRequired,
        readOnly: (_this$props$stateData11 = this.props.stateData.fieldDisable[this.props.i]) === null || _this$props$stateData11 === void 0 ? void 0 : _this$props$stateData11.diableField,
        onChange: function onChange(e) {
          _this12.handleInput(e, _this12.props.item, _this12.props.j);
        },
        value: (_this$props$innsideIt3 = this.props.innsideItem) === null || _this$props$innsideIt3 === void 0 ? void 0 : _this$props$innsideIt3.paramDefaultValue
      }), this.props.innsideItem.paramIsRequired && (this.props.innsideItem.paramValue === undefined || this.props.innsideItem.paramValue === null || this.props.innsideItem.paramValue === 'undefined' || this.props.innsideItem.paramValue === '') && this.props.stateData.enableError && /*#__PURE__*/_react["default"].createElement("span", {
        className: "shadow-input"
      }, "Please Fill Required data"));
    }
  }]);

  return NewCredentials;
}(_react["default"].Component); //Created for Carriers Tab


var TabsBar = /*#__PURE__*/function (_React$Component8) {
  _inherits(TabsBar, _React$Component8);

  var _super8 = _createSuper(TabsBar);

  function TabsBar() {
    _classCallCheck(this, TabsBar);

    return _super8.apply(this, arguments);
  }

  _createClass(TabsBar, [{
    key: "setIndex",
    value: function () {
      var _setIndex = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(methodName, selectedId) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.props.getIndex(methodName, selectedId);

              case 2:
                methodName();

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function setIndex(_x, _x2) {
        return _setIndex.apply(this, arguments);
      }

      return setIndex;
    }()
  }, {
    key: "render",
    value: function render() {
      var _this13 = this;

      return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("ul", {
        className: "nav nav-tabs",
        id: "myTabs",
        role: "tablist"
      }, this.props.tabsData.map(function (tab, index) {
        return /*#__PURE__*/_react["default"].createElement("li", {
          className: "nav-item"
        }, /*#__PURE__*/_react["default"].createElement("a", {
          className: tab.refData == _this13.props.activeTab ? 'nav-link active' : 'nav-link',
          id: tab.tabID,
          "data-toggle": "tab",
          href: tab.refData,
          role: "tab",
          "aria-controls": tab.refData,
          "aria-selected": index == 0 ? 'true' : 'false',
          onClick: function onClick() {
            _this13.setIndex(tab.methodName, tab.refData);
          }
        }, tab.component));
      })));
    }
  }]);

  return TabsBar;
}(_react["default"].Component);

var TableDataComponent = /*#__PURE__*/function (_React$Component9) {
  _inherits(TableDataComponent, _React$Component9);

  var _super9 = _createSuper(TableDataComponent);

  function TableDataComponent() {
    _classCallCheck(this, TableDataComponent);

    return _super9.apply(this, arguments);
  }

  _createClass(TableDataComponent, [{
    key: "render",
    value: function render() {
      var _this14 = this;

      var customTotal = function customTotal(from, to, size) {
        return /*#__PURE__*/_react["default"].createElement("span", {
          className: "react-bootstrap-table-pagination-total"
        }, "Showing ", from, " to ", to, " of ", size, " Results");
      };

      var options = {
        paginationSize: 4,
        pageStartIndex: 1,
        firstPageText: 'First',
        prePageText: 'Back',
        nextPageText: 'Next',
        lastPageText: 'Last',
        nextPageTitle: 'First page',
        prePageTitle: 'Pre page',
        firstPageTitle: 'Next page',
        lastPageTitle: 'Last page',
        showTotal: true,
        paginationTotalRenderer: customTotal
      };
      return /*#__PURE__*/_react["default"].createElement(_reactBootstrapTable2Toolkit["default"], {
        keyField: "id",
        data: this.props.tableData,
        columns: this.props.columnData,
        search: true
      }, function (props) {
        return /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement(SearchBar, props.searchProps), /*#__PURE__*/_react["default"].createElement(_reactBootstrapTableNext["default"], _extends({
          bootstrap4: true
        }, props.baseProps, {
          pagination: (0, _reactBootstrapTable2Paginator["default"])(options),
          noDataIndication: _this14.props.noDataIndication,
          hover: true,
          bordered: false,
          expandRow: _this14.props.expandRow ? _this14.props.expandRow : ''
        })));
      });
    }
  }]);

  return TableDataComponent;
}(_react["default"].Component);

var SearchCarrierComponent = /*#__PURE__*/function (_React$Component10) {
  _inherits(SearchCarrierComponent, _React$Component10);

  var _super10 = _createSuper(SearchCarrierComponent);

  function SearchCarrierComponent() {
    _classCallCheck(this, SearchCarrierComponent);

    return _super10.apply(this, arguments);
  }

  _createClass(SearchCarrierComponent, [{
    key: "render",
    value: function render() {
      var _this15 = this;

      return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("span", {
        className: "icofont-info-circle"
      }), "\u2002MC , DOT and Name searches both Trucker Cloud and FMCSA for information. SCAC searches only Trucker Cloud.", /*#__PURE__*/_react["default"].createElement("article", {
        className: "table-data truck-data shadow bg-white rounded",
        style: {
          display: 'block'
        }
      }, /*#__PURE__*/_react["default"].createElement("form", null, /*#__PURE__*/_react["default"].createElement("div", {
        onChange: function onChange(e) {
          _this15.props.onChange(e);
        },
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-lg-3"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "radio",
        value: this.props.passedData.carriername || '',
        id: "carriernameid",
        checked: this.props.passedData.radio1,
        name: "searchGroup"
      }), /*#__PURE__*/_react["default"].createElement("input", {
        placeholder: "Carrier Name ",
        id: "carriernameid",
        autocomplete: "off",
        type: "text",
        "class": "labelCssForSearch",
        disabled: !this.props.passedData.radio1,
        value: this.props.passedData.carriername || ''
      }), /*#__PURE__*/_react["default"].createElement("span", {
        className: "shadow-input cname-input"
      }, this.props.passedData.carrierNameError)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-lg-3"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "radio",
        id: "mcnumberid",
        value: this.props.passedData.mcNumber || '',
        name: "searchGroup",
        checked: this.props.passedData.radio2
      }), /*#__PURE__*/_react["default"].createElement("input", {
        id: "mcnumberid",
        "class": "labelCssForSearch",
        autocomplete: "off",
        type: "text",
        placeholder: "MC # ",
        disabled: !this.props.passedData.radio2,
        value: this.props.passedData.mcNumber || ''
      }), /*#__PURE__*/_react["default"].createElement("span", {
        className: "shadow-input"
      }, this.props.passedData.mcNumberError)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-lg-3"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "radio",
        value: this.props.passedData.dotNumber || '',
        id: "dotnumberid",
        name: "searchGroup",
        checked: this.props.passedData.radio3
      }), /*#__PURE__*/_react["default"].createElement("input", {
        id: "dotnumberid",
        type: "text",
        placeholder: "DOT #",
        autocomplete: "off",
        "class": "labelCssForSearch",
        disabled: !this.props.passedData.radio3,
        value: this.props.passedData.dotNumber || ''
      }), /*#__PURE__*/_react["default"].createElement("span", {
        className: "shadow-input"
      }, this.props.passedData.dotNumberError)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-lg-2"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "radio",
        id: "scacnumberid",
        name: "searchGroup",
        disabled: true
      }), /*#__PURE__*/_react["default"].createElement("input", {
        id: "scacnumberid",
        type: "text",
        placeholder: "SCAC #",
        "class": "labelCssForSearch",
        disabled: true
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-lg-1"
      }, /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        disabled: this.props.passedData.searchValue,
        value: "searchbtn",
        className: "btn btn-primary",
        onClick: function onClick(e) {
          _this15.props.searchClick(e);
        }
      }, "Search"))))));
    }
  }]);

  return SearchCarrierComponent;
}(_react["default"].Component);

var HeaderBar = /*#__PURE__*/function (_React$Component11) {
  _inherits(HeaderBar, _React$Component11);

  var _super11 = _createSuper(HeaderBar);

  function HeaderBar() {
    _classCallCheck(this, HeaderBar);

    return _super11.apply(this, arguments);
  }

  _createClass(HeaderBar, [{
    key: "render",
    value: function render() {
      var _this16 = this;

      return /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("a", {
        className: "navbar-brand",
        href: "#",
        onClick: function onClick(e) {
          _this16.props.onclick(e);
        }
      }, /*#__PURE__*/_react["default"].createElement("img", {
        id: "trade-logo",
        src: this.props.passingData
      })), /*#__PURE__*/_react["default"].createElement("button", {
        className: "navbar-toggler",
        type: "button",
        "data-toggle": "collapse",
        "data-target": "#navbarSupportedContent",
        "aria-controls": "navbarSupportedContent",
        "aria-expanded": "false",
        "aria-label": "Toggle navigation"
      }, /*#__PURE__*/_react["default"].createElement("span", {
        className: "navbar-toggler-icon"
      })));
    }
  }]);

  return HeaderBar;
}(_react["default"].Component);

var DashboardTrucksComponent = /*#__PURE__*/function (_React$Component12) {
  _inherits(DashboardTrucksComponent, _React$Component12);

  var _super12 = _createSuper(DashboardTrucksComponent);

  function DashboardTrucksComponent(props) {
    var _this17;

    _classCallCheck(this, DashboardTrucksComponent);

    _this17 = _super12.call(this, props);

    _defineProperty(_assertThisInitialized(_this17), "formatLocation", function (location) {
      try {
        var locationArray = location.split('|');
        locationArray = locationArray.reverse();
        return locationArray.filter(function (x) {
          return x.trim().length > 0;
        }).join(', ');
      } catch (err) {
        return location;
      }
    });

    _defineProperty(_assertThisInitialized(_this17), "handleClick", function (arg1, arg2) {
      var checked = document.getElementById('cb' + arg1.id).checked;
      var arr = [];

      if (arg2.target.className == 'icofont-circled-right') {
        arg2.target.className = 'icofont-circled-down';
        arr.push(arg1);
        if (checked) _this17.props.truckClicked(arr);
      } else {
        arg2.target.className = 'icofont-circled-right';

        _this17.props.truckClicked(arr);
      }
    });

    _defineProperty(_assertThisInitialized(_this17), "handleChecked", function (arg) {
      var remainingEle = _this17.props.selectedData.filter(function (truck) {
        var checked = document.getElementById('cb' + truck.id).checked;
        return checked !== false;
      });

      _this17.props.checkList(remainingEle);
    });

    _defineProperty(_assertThisInitialized(_this17), "truckClick", function (e, truck) {
      var arg2 = e.target.closest('label').firstElementChild.firstElementChild;

      if (arg2.className == 'icofont-circled-right') {
        arg2.className = 'icofont-circled-down';
      } else {
        arg2.className = 'icofont-circled-right';
      }

      _this17.props._truckClick(truck);
    });

    _defineProperty(_assertThisInitialized(_this17), "searchTrucks", function (e) {
      _this17.srcData = $(e.target).val().trim();

      if (_this17.timerSrc) {
        clearTimeout(_this17.timerSrc);
      }

      _this17.timerSrc = setTimeout(function () {
        clearTimeout(this.timerSrc);
        this.timerSrc = null;
        this.setState({});
      }.bind(_assertThisInitialized(_this17)), 500);
    });

    _defineProperty(_assertThisInitialized(_this17), "filterData", function (data) {
      if (!data.length) return [];
      if (_this17.srcData.length == 0) return data;
      var userType = localStorage.getItem('userType').toLowerCase();
      var expr = RegExp(_this17.srcData, 'i');
      var row = data.filter(function (item, index) {
        return expr.test(userType == 'shipper' ? item.shipperRefNumber : item.truckNo);
      });
      return row;
    });

    _this17.timerSrc = null;
    _this17.srcData = '';
    var val = _this17.props.spinner ? true : false;
    return _this17;
  }

  _createClass(DashboardTrucksComponent, [{
    key: "render",
    value: function render() {
      var _this18 = this;

      var userType = localStorage.getItem('userType').toLowerCase();
      var label = userType == 'shipper' ? 'Load' : 'Truck';
      var stopped = this.filterData(this.props.stopped);
      var idle = this.filterData(this.props.idle);
      var active = this.filterData(this.props.active);
      var disabled = this.props.disabled;
      var stoppedData = !this.props.loading && stopped.length ? stopped.map(function (stoppedList, index) {
        var titleval = '';

        var location = _this18.formatLocation(stoppedList.location);

        if (userType == 'broker') {
          if (stoppedList.shipperRefNumber !== null) //&&userType!=='carrier'
            {
              titleval = 'Customer Load # : ' + stoppedList.shipperRefNumber;
            }
        }

        var truckOrPoNumber = '';

        if (userType !== 'shipper') {
          truckOrPoNumber = 'Truck/Asset No : ' + stoppedList.truckNo;
        } else {
          truckOrPoNumber = 'Load No : ' + stoppedList.shipperRefNumber;
        }

        return /*#__PURE__*/_react["default"].createElement("article", {
          key: index
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card"
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-header",
          id: 'stoppedHedaing' + stoppedList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "form-check"
        }, /*#__PURE__*/_react["default"].createElement("input", {
          className: "form-check-input",
          type: "checkbox",
          onChange: _this18.handleChecked.bind(stoppedList),
          value: stoppedList.id,
          defaultChecked: "true",
          id: 'cb' + stoppedList.id,
          style: {
            display: 'none'
          }
        }), /*#__PURE__*/_react["default"].createElement("label", {
          "data-placement": "top",
          onClick: function onClick(e) {
            _this18.truckClick(e, stoppedList);
          },
          className: "form-check-label",
          htmlFor: 'cb1' + stoppedList.id,
          "data-toggle": "collapse",
          "data-target": '#stoppedCollapse' + stoppedList.id,
          "aria-expanded": "true",
          "aria-controls": 'stoppedCollapse' + stoppedList.id
        }, /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          onClick: _this18.handleClick.bind(_this18, stoppedList),
          icon: "circled-right",
          "data-toggle": "collapse",
          "data-target": '#stoppedCollapse' + stoppedList.id,
          "aria-expanded": "true",
          "aria-controls": 'stoppedCollapse' + stoppedList.id
        })), " ", /*#__PURE__*/_react["default"].createElement("span", {
          "data-tip": titleval,
          title: titleval
        }, truckOrPoNumber, " ", stoppedList.loadVehicle == true ? /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement("img", {
          alt: "",
          src: _this18.props.loadVehicleicon
        })) : /*#__PURE__*/_react["default"].createElement("span", null)), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
          place: "top"
        }), " "), /*#__PURE__*/_react["default"].createElement("span", {
          className: "float-right",
          style: {
            color: 'red'
          }
        }, " Stopped ", /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          icon: "ui-press"
        })))), /*#__PURE__*/_react["default"].createElement("div", {
          id: 'stoppedCollapse' + stoppedList.id,
          className: "collapse",
          "aria-labelledby": 'stoppedHedaing' + stoppedList.id,
          "data-parent": "#stopped_stats"
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-body"
        }, /*#__PURE__*/_react["default"].createElement("ul", null, /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Date:"), /*#__PURE__*/_react["default"].createElement("span", null, stoppedList.convertedDate)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Location:"), /*#__PURE__*/_react["default"].createElement("span", null, location)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Reason"), /*#__PURE__*/_react["default"].createElement("span", null, "-")))))));
      }) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "stay-vertical"
      }, !this.props.loading ? /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("p", null, "No Results Found")) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "text-center"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "spinner-border",
        role: "status"
      }, /*#__PURE__*/_react["default"].createElement("span", {
        className: "sr-only"
      }, "Loading..."))));
      var idleData = !this.props.loading && idle.length ? idle.map(function (idleList, index) {
        var titlevalidle = '';

        var location = _this18.formatLocation(idleList.location);

        if (userType == 'broker') {
          if (idleList.shipperRefNumber !== null) {
            titlevalidle = 'Customer Load # : ' + idleList.shipperRefNumber;
          }
        }

        var truckOrPoNumber = '';

        if (userType !== 'shipper') {
          truckOrPoNumber = 'Truck/Asset No : ' + idleList.truckNo;
        } else {
          truckOrPoNumber = 'Load No : ' + idleList.shipperRefNumber;
        }

        return /*#__PURE__*/_react["default"].createElement("div", {
          className: "card",
          key: index
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-header",
          id: 'idleHeading' + idleList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "form-check"
        }, /*#__PURE__*/_react["default"].createElement("input", {
          className: "form-check-input",
          type: "checkbox",
          onChange: _this18.handleChecked.bind(idleList),
          value: idleList.id,
          defaultChecked: "true",
          id: 'cb' + idleList.id,
          style: {
            display: 'none'
          }
        }), /*#__PURE__*/_react["default"].createElement("label", {
          "data-placement": "top",
          onClick: function onClick(e) {
            _this18.truckClick(e, idleList);
          },
          className: "form-check-label",
          htmlFor: 'cb1' + idleList.id,
          "data-toggle": "collapse",
          "data-target": '#idleCollapse' + idleList.id,
          "aria-expanded": "true",
          "aria-controls": 'idleCollapse' + idleList.id
        }, /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          onClick: _this18.handleClick.bind(_this18, idleList),
          icon: "circled-right",
          "data-toggle": "collapse",
          "data-target": '#idleCollapse' + idleList.id,
          "aria-expanded": "true",
          "aria-controls": 'idleCollapse' + idleList.id
        })), " ", /*#__PURE__*/_react["default"].createElement("span", {
          "data-tip": titlevalidle,
          title: titlevalidle
        }, truckOrPoNumber, " ", idleList.loadVehicle == true ? /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement("img", {
          alt: "",
          src: _this18.props.loadVehicleicon
        })) : /*#__PURE__*/_react["default"].createElement("span", null)), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
          place: "top"
        }), " "), /*#__PURE__*/_react["default"].createElement("span", {
          className: "float-right",
          style: {
            color: '#fdc800'
          }
        }, " ", /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          icon: "ui-press"
        })))), /*#__PURE__*/_react["default"].createElement("div", {
          id: 'idleCollapse' + idleList.id,
          className: "collapse",
          "aria-labelledby": 'idleHeading' + idleList.id,
          "data-parent": "#idle_stats"
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-body"
        }, /*#__PURE__*/_react["default"].createElement("ul", null, /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Date:"), /*#__PURE__*/_react["default"].createElement("span", null, idleList.convertedDate)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Location:"), /*#__PURE__*/_react["default"].createElement("span", null, location)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Reason"), /*#__PURE__*/_react["default"].createElement("span", null, "-"))))));
      }) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "stay-vertical"
      }, !this.props.loading ? /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("p", null, "No Results Found")) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "text-center"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "spinner-border",
        role: "status"
      }, /*#__PURE__*/_react["default"].createElement("span", {
        className: "sr-only"
      }, "Loading..."))));
      var activeData = !this.props.loading && active.length ? active.map(function (activeList, index) {
        var titlevalactive = '';

        var location = _this18.formatLocation(activeList.location);

        if (userType == 'broker') {
          if (activeList.shipperRefNumber !== null) {
            titlevalactive = 'Customer Load # : ' + activeList.shipperRefNumber;
          }
        }

        var truckOrPoNumber = '';

        if (userType !== 'shipper') {
          truckOrPoNumber = 'Truck/Asset No : ' + activeList.truckNo;
        } else {
          truckOrPoNumber = 'Load No : ' + activeList.shipperRefNumber;
        }

        return /*#__PURE__*/_react["default"].createElement("div", {
          className: "card",
          key: index
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-header",
          id: 'activeHedaing' + activeList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "form-check"
        }, /*#__PURE__*/_react["default"].createElement("input", {
          className: "form-check-input",
          type: "checkbox",
          onChange: _this18.handleChecked.bind(activeList),
          value: activeList.id,
          defaultChecked: "true",
          id: 'cb' + activeList.id,
          style: {
            display: 'none'
          }
        }), /*#__PURE__*/_react["default"].createElement("label", {
          "data-placement": "top",
          onClick: function onClick(e) {
            _this18.truckClick(e, activeList);
          },
          className: "form-check-label",
          htmlFor: 'cb1' + activeList.id,
          "data-toggle": "collapse",
          "data-target": '#activeCollapse' + activeList.id,
          "aria-expanded": "true",
          "aria-controls": 'activeCollapse' + activeList.id
        }, /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          onClick: _this18.handleClick.bind(_this18, activeList),
          icon: "circled-right",
          "data-toggle": "collapse",
          "data-target": '#activeCollapse' + activeList.id,
          "aria-expanded": "true",
          "aria-controls": 'activeCollapse' + activeList.id
        })), " ", /*#__PURE__*/_react["default"].createElement("span", {
          "data-tip": titlevalactive,
          title: titlevalactive
        }, truckOrPoNumber, " ", activeList.loadVehicle == true ? /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement("img", {
          alt: "",
          src: _this18.props.loadVehicleicon
        })) : /*#__PURE__*/_react["default"].createElement("span", null)), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
          place: "top"
        }), " "), /*#__PURE__*/_react["default"].createElement("span", {
          className: "float-right",
          style: {
            color: 'green'
          }
        }, " ", /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          icon: "ui-press"
        })))), /*#__PURE__*/_react["default"].createElement("div", {
          id: 'activeCollapse' + activeList.id,
          className: "collapse",
          "aria-labelledby": 'activeHedaing' + activeList.id,
          "data-parent": "#active_stats"
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-body"
        }, /*#__PURE__*/_react["default"].createElement("ul", null, /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Date:"), /*#__PURE__*/_react["default"].createElement("span", null, activeList.convertedDate)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Location:"), /*#__PURE__*/_react["default"].createElement("span", null, location)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Reason"), /*#__PURE__*/_react["default"].createElement("span", null, "-"))))));
      }) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "stay-vertical"
      }, !this.props.loading ? /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("p", null, "No Results Found")) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "text-center"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "spinner-border",
        role: "status"
      }, /*#__PURE__*/_react["default"].createElement("span", {
        className: "sr-only"
      }, "Loading..."))));
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "truck-status"
      }, /*#__PURE__*/_react["default"].createElement("h4", {
        className: "page-title"
      }, label, " Status"), /*#__PURE__*/_react["default"].createElement("ul", {
        className: "nav nav-tabs flex-column flex-sm-row",
        id: "myTab",
        role: "tablist"
      }, /*#__PURE__*/_react["default"].createElement("li", {
        className: "nav-item flex-sm-fill text-sm-center"
      }, /*#__PURE__*/_react["default"].createElement("a", {
        className: "nav-link active",
        style: disabled ? {
          pointerEvents: 'none',
          opacity: '0.4'
        } : {},
        onClick: function onClick() {
          _this18.props.tabClicked(1);
        },
        id: "home-tab",
        "data-toggle": "tab",
        href: "#home",
        role: "tab",
        "aria-controls": "home",
        "aria-selected": "true"
      }, "Active [", this.props.active.length, "]")), /*#__PURE__*/_react["default"].createElement("li", {
        className: "nav-item flex-sm-fill text-sm-center"
      }, /*#__PURE__*/_react["default"].createElement("a", {
        className: "nav-link",
        style: disabled ? {
          pointerEvents: 'none',
          opacity: '0.4'
        } : {},
        onClick: function onClick() {
          _this18.props.tabClicked(2);
        },
        id: "profile-tab",
        "data-toggle": "tab",
        href: "#profile",
        role: "tab",
        "aria-controls": "profile",
        "aria-selected": "false"
      }, "Idle [", this.props.idle.length, "] ")), /*#__PURE__*/_react["default"].createElement("li", {
        className: "nav-item flex-sm-fill text-sm-center"
      }, /*#__PURE__*/_react["default"].createElement("a", {
        className: "nav-link",
        style: disabled ? {
          pointerEvents: 'none',
          opacity: '0.4'
        } : {},
        onClick: function onClick() {
          _this18.props.tabClicked(3);
        },
        id: "contact-tab",
        "data-toggle": "tab",
        href: "#contact",
        role: "tab",
        "aria-controls": "contact",
        "aria-selected": "false"
      }, "Stopped [", this.props.stopped.length, "] "))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "tab-search"
      }, /*#__PURE__*/_react["default"].createElement("form", {
        onSubmit: function onSubmit(e) {
          e.preventDefault();
        }
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group form-icon",
        id: "searchicondashboard"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        onKeyUp: function onKeyUp(e) {
          _this18.searchTrucks(e);
        },
        className: "form-control control-custom",
        id: "truckSearch",
        placeholder: 'Search ' + label
      }), /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "search"
      }))))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "tab-content",
        id: "myTabContent"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "tab-pane fade show active",
        id: "home",
        role: "tabpanel",
        "aria-labelledby": "home-tab"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "accordion",
        id: "active_stats"
      }, activeData)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "tab-pane fade",
        id: "profile",
        role: "tabpanel",
        "aria-labelledby": "profile-tab"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "accordion",
        id: "idle_stats"
      }, idleData)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "tab-pane fade",
        id: "contact",
        role: "tabpanel",
        "aria-labelledby": "contact-tab"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "accordion",
        id: "stopped_stats"
      }, stoppedData))));
    }
  }]);

  return DashboardTrucksComponent;
}(_react["default"].Component);

var GetCarrier = /*#__PURE__*/function (_React$Component13) {
  _inherits(GetCarrier, _React$Component13);

  var _super13 = _createSuper(GetCarrier);

  function GetCarrier() {
    var _this19;

    _classCallCheck(this, GetCarrier);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this19 = _super13.call.apply(_super13, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this19), "handleChange", function (e) {
      _this19.props.onChange(e);
    });

    return _this19;
  }

  _createClass(GetCarrier, [{
    key: "render",
    value: function render() {
      var _this20 = this,
          _this$props$state$car;

      var userType = this.props.userType;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "carrierDropdown"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-12"
      }, /*#__PURE__*/_react["default"].createElement("label", {
        htmlFor: "carrierdropdownLabel",
        className: "carrierdropdownLabel"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Select Carrier")), /*#__PURE__*/_react["default"].createElement("select", {
        disabled: this.props.state.disabled,
        className: "form-control",
        id: "findCarrierId",
        onChange: function onChange(e) {
          _this20.handleChange(e);
        }
      }, /*#__PURE__*/_react["default"].createElement("option", {
        value: "0"
      }, "Select Carrier"), ((_this$props$state$car = this.props.state.carrierNames) === null || _this$props$state$car === void 0 ? void 0 : _this$props$state$car.length) > 0 ? this.props.state.carrierNames.map(function (company, index) {
        return /*#__PURE__*/_react["default"].createElement("option", {
          key: index,
          value: userType === 'broker' ? company.Invited_Company_Id : company.Company_Id
        }, company.Company_Name);
      }) : /*#__PURE__*/_react["default"].createElement("option", null, "Loading....."))));
    }
  }]);

  return GetCarrier;
}(_react["default"].Component);

var GetEld = /*#__PURE__*/function (_React$Component14) {
  _inherits(GetEld, _React$Component14);

  var _super14 = _createSuper(GetEld);

  function GetEld() {
    var _this21;

    _classCallCheck(this, GetEld);

    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    _this21 = _super14.call.apply(_super14, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this21), "selectELD", function (e) {
      _this21.props.onChange(e);
    });

    return _this21;
  }

  _createClass(GetEld, [{
    key: "render",
    value: function render() {
      var _this22 = this,
          _this$props$selectELD,
          _this$props$selectELD2,
          _this$props$selectELD3;

      return /*#__PURE__*/_react["default"].createElement("div", null, this.props.dashboard ? this.props.eldList && this.props.eldList.length > 0 ? /*#__PURE__*/_react["default"].createElement("div", {
        className: "carrierDropdown"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-12"
      }, /*#__PURE__*/_react["default"].createElement("label", {
        htmlFor: "carrierdropdownLabel",
        className: "carrierdropdownLabel"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Select ELD Providers")), /*#__PURE__*/_react["default"].createElement("select", {
        className: "form-control",
        id: "findCarrierId",
        onChange: function onChange(e) {
          _this22.selectELD(e);
        }
      }, this.props.dashboard && this.props.eldList.length > 0 ? this.props.eldList.map(function (row, index) {
        return /*#__PURE__*/_react["default"].createElement("option", {
          key: index,
          value: row.ELD_Provider_Id
        }, row.ELD_Provider_Disp_Name);
      }) : null))) : null : this.props.selectELD ? /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-3"
      }, /*#__PURE__*/_react["default"].createElement("label", null, /*#__PURE__*/_react["default"].createElement("b", null, "Select ELD Provider")), /*#__PURE__*/_react["default"].createElement("select", {
        className: "form-control",
        id: "findELDId",
        onChange: function onChange(e) {
          _this22.selectELD(e);
        }
      }, /*#__PURE__*/_react["default"].createElement("option", {
        value: "0"
      }, "Select ELD Provider"), (_this$props$selectELD = this.props.selectELD) !== null && _this$props$selectELD !== void 0 && _this$props$selectELD.eldProvidersList ? this.props.selectELD.eldProvidersList.map(function (row) {
        return /*#__PURE__*/_react["default"].createElement("option", {
          key: row.ELD_Provider_Id,
          value: row.ELD_Provider_Id
        }, row.ELD_Provider_Disp_Name);
      }) : (_this$props$selectELD2 = this.props.selectELD) === null || _this$props$selectELD2 === void 0 ? void 0 : (_this$props$selectELD3 = _this$props$selectELD2.eldList) === null || _this$props$selectELD3 === void 0 ? void 0 : _this$props$selectELD3.map(function (row) {
        return row.name !== 'root Eld' ? /*#__PURE__*/_react["default"].createElement("option", {
          key: row.id,
          value: row.id
        }, row.eldProviderDispName) : null;
      })))) : null);
    }
  }]);

  return GetEld;
}(_react["default"].Component);

var DashboardTrucksTrmComponent = /*#__PURE__*/function (_React$Component15) {
  _inherits(DashboardTrucksTrmComponent, _React$Component15);

  var _super15 = _createSuper(DashboardTrucksTrmComponent);

  function DashboardTrucksTrmComponent(props) {
    var _this23;

    _classCallCheck(this, DashboardTrucksTrmComponent);

    _this23 = _super15.call(this, props);

    _defineProperty(_assertThisInitialized(_this23), "formatLocation", function (location) {
      try {
        var locationArray = location.split('|');
        locationArray = locationArray.reverse();
        return locationArray.filter(function (x) {
          return x.trim().length > 0;
        }).join(', ');
      } catch (err) {
        return location;
      }
    });

    _defineProperty(_assertThisInitialized(_this23), "handleClick", function (arg1, arg2) {
      var checked = document.getElementById('cb' + arg1.id).checked;
      var arr = [];

      if (arg2.target.className == 'icofont-circled-right') {
        arg2.target.className = 'icofont-circled-down';
        arr.push(arg1);
        if (checked) _this23.props.truckClicked(arr);
      } else {
        arg2.target.className = 'icofont-circled-right';

        _this23.props.truckClicked(arr);
      }
    });

    _defineProperty(_assertThisInitialized(_this23), "handleChecked", function (arg) {
      var remainingEle = _this23.props.selectedData.filter(function (truck) {
        var checked = document.getElementById('cb' + truck.id).checked;
        return checked !== false;
      });

      _this23.props.checkList(remainingEle);
    });

    _defineProperty(_assertThisInitialized(_this23), "truckClick", function (e, truck) {
      _this23.props._truckClick(truck);
    });

    _defineProperty(_assertThisInitialized(_this23), "searchTrucks", function (e) {
      _this23.srcData = $(e.target).val().trim();

      if (_this23.timerSrc) {
        clearTimeout(_this23.timerSrc);
      }

      _this23.timerSrc = setTimeout(function () {
        clearTimeout(this.timerSrc);
        this.timerSrc = null;
        this.setState({});
      }.bind(_assertThisInitialized(_this23)), 500);
    });

    _defineProperty(_assertThisInitialized(_this23), "filterData", function (data) {
      if (!data.length) return [];
      if (_this23.srcData.length == 0) return data;
      var expr = RegExp(_this23.srcData, 'i');
      var row = data.filter(function (item, index) {
        return expr.test(item.truckNo);
      });
      return row;
    });

    _this23.timerSrc = null;
    _this23.srcData = '';
    return _this23;
  }

  _createClass(DashboardTrucksTrmComponent, [{
    key: "LoadingSpinner",
    value: function LoadingSpinner() {
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "d-flex justify-content-center"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "spinner-border",
        role: "status"
      }, /*#__PURE__*/_react["default"].createElement("span", {
        className: "sr-only"
      }, "Loading...")));
    }
  }, {
    key: "render",
    value: function render() {
      var _this24 = this;

      var stopped = this.filterData(this.props.stopped);
      var idle = this.filterData(this.props.idle);
      var active = this.filterData(this.props.active);
      var stoppedData = stopped.length ? stopped.map(function (stoppedList) {
        var titleval = '';
        var location = formatLocation(stoppedList.location);

        if (stoppedList.shipperRefNumber !== null) {
          titleval = 'Shipper Ref Number : ' + stoppedList.shipperRefNumber;
        } else if (stoppedList.loadNumber !== null) {
          titleval = 'Load Number : ' + stoppedList.loadNumber;
        }

        return /*#__PURE__*/_react["default"].createElement("div", {
          className: "card",
          key: stoppedList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-header",
          id: 'stoppedHedaing' + stoppedList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "form-check"
        }, /*#__PURE__*/_react["default"].createElement("input", {
          className: "form-check-input",
          type: "checkbox",
          onChange: _this24.handleChecked.bind(stoppedList),
          value: stoppedList.id,
          defaultChecked: "true",
          id: 'cb' + stoppedList.id,
          style: {
            display: 'none'
          }
        }), /*#__PURE__*/_react["default"].createElement("label", {
          "data-placement": "top",
          onClick: function onClick(e) {
            _this24.truckClick(e, stoppedList);
          },
          className: "form-check-label",
          htmlFor: 'cb1' + stoppedList.id
        }, /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          onClick: _this24.handleClick.bind(_this24, stoppedList),
          icon: "circled-right",
          "data-toggle": "collapse",
          "data-target": '#stoppedCollapse' + stoppedList.id,
          "aria-expanded": "true",
          "aria-controls": 'stoppedCollapse' + stoppedList.id
        })), " ", /*#__PURE__*/_react["default"].createElement("span", {
          "data-tip": titleval,
          title: titleval
        }, "Truck/Asset No :", stoppedList.truckNo, " ", stoppedList.loadVehicle == true ? /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement("img", {
          alt: "",
          src: _this24.props.loadVehicleicon
        })) : /*#__PURE__*/_react["default"].createElement("span", null)), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
          place: "top"
        }), " "), /*#__PURE__*/_react["default"].createElement("span", {
          className: "float-right",
          style: {
            color: 'red'
          }
        }, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          icon: "ui-press"
        })))), /*#__PURE__*/_react["default"].createElement("div", {
          id: 'stoppedCollapse' + stoppedList.id,
          className: "collapse",
          "aria-labelledby": 'stoppedHedaing' + stoppedList.id,
          "data-parent": "#stopped_stats"
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-body"
        }, /*#__PURE__*/_react["default"].createElement("ul", null, /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Date:"), /*#__PURE__*/_react["default"].createElement("span", null, stoppedList.convertedDate)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Location:"), /*#__PURE__*/_react["default"].createElement("span", null, location)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Reason"), /*#__PURE__*/_react["default"].createElement("span", null, "-"))))));
      }) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "stay-vertical"
      }, /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("p", null, "No Results Found")));
      var idleData = idle.length ? idle.map(function (idleList) {
        var titlevalidle = '';
        var location = formatLocation(idleList.location);

        if (idleList.shipperRefNumber !== null) {
          titlevalidle = 'Shipper Ref Number : ' + idleList.shipperRefNumber;
        } else if (idleList.loadNumber !== null) {
          titlevalidle = 'Load Number : ' + idleList.loadNumber;
        }

        return /*#__PURE__*/_react["default"].createElement("div", {
          className: "card",
          key: idleList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-header",
          id: 'idleHeading' + idleList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "form-check"
        }, /*#__PURE__*/_react["default"].createElement("input", {
          className: "form-check-input",
          type: "checkbox",
          onChange: _this24.handleChecked.bind(idleList),
          value: idleList.id,
          defaultChecked: "true",
          id: 'cb' + idleList.id,
          style: {
            display: 'none'
          }
        }), /*#__PURE__*/_react["default"].createElement("label", {
          "data-placement": "top",
          onClick: function onClick(e) {
            _this24.truckClick(e, idleList);
          },
          className: "form-check-label",
          htmlFor: 'cb1' + idleList.id
        }, /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          onClick: _this24.handleClick.bind(_this24, idleList),
          icon: "circled-right",
          "data-toggle": "collapse",
          "data-target": '#idleCollapse' + idleList.id,
          "aria-expanded": "true",
          "aria-controls": 'idleCollapse' + idleList.id
        })), " ", /*#__PURE__*/_react["default"].createElement("span", {
          "data-tip": titlevalidle,
          title: titlevalidle
        }, "Truck/Asset No :", idleList.truckNo, " ", idleList.loadVehicle == true ? /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement("img", {
          alt: "",
          src: _this24.props.loadVehicleicon
        })) : /*#__PURE__*/_react["default"].createElement("span", null)), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
          place: "top"
        }), " "), /*#__PURE__*/_react["default"].createElement("span", {
          className: "float-right",
          style: {
            color: '#fdc800'
          }
        }, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          icon: "ui-press"
        })))), /*#__PURE__*/_react["default"].createElement("div", {
          id: 'idleCollapse' + idleList.id,
          className: "collapse",
          "aria-labelledby": 'idleHeading' + idleList.id,
          "data-parent": "#idle_stats"
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-body"
        }, /*#__PURE__*/_react["default"].createElement("ul", null, /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Date:"), /*#__PURE__*/_react["default"].createElement("span", null, idleList.convertedDate)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Location:"), /*#__PURE__*/_react["default"].createElement("span", null, location)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Reason"), /*#__PURE__*/_react["default"].createElement("span", null, "-"))))));
      }) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "stay-vertical"
      }, /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("p", null, "No Results Found")));
      var activeData = active.length ? active.map(function (activeList) {
        var titlevalactive = '';
        var location = formatLocation(activeList.location);

        if (activeList.shipperRefNumber !== null) {
          titlevalactive = 'Shipper Ref Number : ' + activeList.shipperRefNumber;
        } else if (activeList.loadNumber !== null) {
          titlevalactive = 'Load Number : ' + activeList.loadNumber;
        }

        return /*#__PURE__*/_react["default"].createElement("div", {
          className: "card",
          key: activeList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-header",
          id: 'activeHedaing' + activeList.id
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "form-check"
        }, /*#__PURE__*/_react["default"].createElement("input", {
          className: "form-check-input",
          type: "checkbox",
          onChange: _this24.handleChecked.bind(activeList),
          value: activeList.id,
          defaultChecked: "true",
          id: 'cb' + activeList.id,
          style: {
            display: 'none'
          }
        }), /*#__PURE__*/_react["default"].createElement("label", {
          "data-placement": "top",
          onClick: function onClick(e) {
            _this24.truckClick(e, activeList);
          },
          className: "form-check-label",
          htmlFor: 'cb1' + activeList.id
        }, /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          onClick: _this24.handleClick.bind(_this24, activeList),
          icon: "circled-right",
          "data-toggle": "collapse",
          "data-target": '#activeCollapse' + activeList.id,
          "aria-expanded": "true",
          "aria-controls": 'activeCollapse' + activeList.id
        })), " ", /*#__PURE__*/_react["default"].createElement("span", {
          "data-tip": titlevalactive,
          title: titlevalactive
        }, "Truck/Asset No :", activeList.truckNo, " ", activeList.loadVehicle == true ? /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement("img", {
          alt: "",
          src: _this24.props.loadVehicleicon
        })) : /*#__PURE__*/_react["default"].createElement("span", null)), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
          place: "top"
        }), " "), /*#__PURE__*/_react["default"].createElement("span", {
          className: "float-right",
          style: {
            color: 'green'
          }
        }, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
          icon: "ui-press"
        })))), /*#__PURE__*/_react["default"].createElement("div", {
          id: 'activeCollapse' + activeList.id,
          className: "collapse",
          "aria-labelledby": 'activeHedaing' + activeList.id,
          "data-parent": "#stopped_stats"
        }, /*#__PURE__*/_react["default"].createElement("div", {
          className: "card-body"
        }, /*#__PURE__*/_react["default"].createElement("ul", null, /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Date:"), /*#__PURE__*/_react["default"].createElement("span", null, activeList.convertedDate)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Location:"), /*#__PURE__*/_react["default"].createElement("span", null, location)), /*#__PURE__*/_react["default"].createElement("li", null, /*#__PURE__*/_react["default"].createElement("span", null, "Reason"), /*#__PURE__*/_react["default"].createElement("span", null, "-"))))));
      }) : /*#__PURE__*/_react["default"].createElement("div", {
        className: "stay-vertical"
      }, /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("p", null, "No Results Found")));
      var spinner = this.props.spinner;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "truck-status"
      }, /*#__PURE__*/_react["default"].createElement("h4", {
        className: "page-title"
      }, /*#__PURE__*/_react["default"].createElement("span", null, this.props.eldProvider), " Trucks Status"), /*#__PURE__*/_react["default"].createElement("ul", {
        className: "nav nav-tabs flex-column flex-sm-row",
        id: "myTab",
        role: "tablist"
      }, /*#__PURE__*/_react["default"].createElement("li", {
        className: "nav-item flex-sm-fill text-sm-center"
      }, /*#__PURE__*/_react["default"].createElement("a", {
        className: 'nav-link ' + (this.props.aTab == 1 ? 'active' : ''),
        onClick: function onClick() {
          _this24.props.tabClicked(1);
        },
        id: "home-tab",
        "data-toggle": "tab",
        href: "#home",
        role: "tab",
        "aria-controls": "home",
        "aria-selected": this.props.aTab == 1 ? 'true' : 'false'
      }, "Active [", this.props.active.length, "]")), /*#__PURE__*/_react["default"].createElement("li", {
        className: "nav-item flex-sm-fill text-sm-center"
      }, /*#__PURE__*/_react["default"].createElement("a", {
        className: 'nav-link ' + (this.props.aTab == 2 ? 'active' : ''),
        onClick: function onClick() {
          _this24.props.tabClicked(2);
        },
        id: "profile-tab",
        "data-toggle": "tab",
        href: "#profile",
        role: "tab",
        "aria-controls": "profile",
        "aria-selected": this.props.aTab == 2 ? 'true' : 'false'
      }, "Idle [", this.props.idle.length, "] ")), /*#__PURE__*/_react["default"].createElement("li", {
        className: "nav-item flex-sm-fill text-sm-center"
      }, /*#__PURE__*/_react["default"].createElement("a", {
        className: 'nav-link ' + (this.props.aTab == 3 ? 'active' : ''),
        onClick: function onClick() {
          _this24.props.tabClicked(3);
        },
        id: "contact-tab",
        "data-toggle": "tab",
        href: "#contact",
        role: "tab",
        "aria-controls": "contact",
        "aria-selected": this.props.aTab == 3 ? 'true' : 'false'
      }, "Stopped [", this.props.stopped.length, "] "))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "tab-search"
      }, /*#__PURE__*/_react["default"].createElement("form", {
        onSubmit: function onSubmit(e) {
          e.preventDefault();
        }
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group form-icon"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        onKeyUp: function onKeyUp(e) {
          _this24.searchTrucks(e);
        },
        className: "form-control control-custom",
        id: "",
        placeholder: "Search Trucks"
      }), /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement(_reactIcofont["default"], {
        icon: "search"
      }))))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "tab-content",
        id: "myTabContent"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: 'tab-pane fade ' + (this.props.aTab == 1 ? 'show active' : ''),
        id: "home",
        role: "tabpanel",
        "aria-labelledby": "home-tab"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "accordion",
        id: "active_stats"
      }, spinner ? this.LoadingSpinner() : activeData)), /*#__PURE__*/_react["default"].createElement("div", {
        className: 'tab-pane fade ' + (this.props.aTab == 2 ? 'show active' : ''),
        id: "profile",
        role: "tabpanel",
        "aria-labelledby": "profile-tab"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "accordion",
        id: "idle_stats"
      }, spinner ? this.LoadingSpinner() : idleData)), /*#__PURE__*/_react["default"].createElement("div", {
        className: 'tab-pane fade ' + (this.props.aTab == 3 ? 'show active' : ''),
        id: "contact",
        role: "tabpanel",
        "aria-labelledby": "contact-tab"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "accordion",
        id: "stopped_stats"
      }, spinner ? this.LoadingSpinner() : stoppedData))));
    }
  }]);

  return DashboardTrucksTrmComponent;
}(_react["default"].Component);

var BreadCrumpTrail = /*#__PURE__*/function (_React$Component16) {
  _inherits(BreadCrumpTrail, _React$Component16);

  var _super16 = _createSuper(BreadCrumpTrail);

  function BreadCrumpTrail() {
    _classCallCheck(this, BreadCrumpTrail);

    return _super16.apply(this, arguments);
  }

  _createClass(BreadCrumpTrail, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group ".concat(this.props.touched && this.props.error ? 'error' : '')
      }, /*#__PURE__*/_react["default"].createElement("select", _extends({}, this.props.input, {
        placeholder: this.props.label,
        id: this.props.id,
        className: "form-control ".concat(this.props.controlClass ? this.props.controlClass : '')
      }), this.props.condition ? /*#__PURE__*/_react["default"].createElement("option", {
        value: ""
      }, "".concat(this.props.label ? this.props.label : this.props.selectlabel)) : null, this.props.condition ? Array.isArray(this.props.data) ? this.props.data.map(function (option) {
        return /*#__PURE__*/_react["default"].createElement("option", {
          value: option.truckNo,
          key: option.id
        }, option.truckNo);
      }) : '' : Array.isArray(this.props.data) ? this.props.data.map(function (option) {
        return /*#__PURE__*/_react["default"].createElement("option", {
          value: option,
          key: option
        }, option);
      }) : ''), this.props.touched && this.props.error && /*#__PURE__*/_react["default"].createElement("span", {
        className: "error"
      }, this.props.error));
    }
  }]);

  return BreadCrumpTrail;
}(_react["default"].Component);

var BreadCrumpTrailButton = /*#__PURE__*/function (_React$Component17) {
  _inherits(BreadCrumpTrailButton, _React$Component17);

  var _super17 = _createSuper(BreadCrumpTrailButton);

  function BreadCrumpTrailButton() {
    _classCallCheck(this, BreadCrumpTrailButton);

    return _super17.apply(this, arguments);
  }

  _createClass(BreadCrumpTrailButton, [{
    key: "render",
    value: function render() {
      var _this25 = this;

      return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, this.props.condition ? /*#__PURE__*/_react["default"].createElement("button", {
        type: "submit",
        disabled: this.props.state.loading,
        value: this.props.state["export"],
        onClick: function onClick(e) {
          _this25.props.onClick();
        },
        "data-name": "export",
        className: "btn btn-primary btn-sm"
      }, this.props.state.loading && this.props.state.exportLoader && /*#__PURE__*/_react["default"].createElement("i", {
        className: "fa fa-refresh fa-spin",
        style: {
          marginRight: '5px'
        }
      }), (!this.props.state.loading || this.props.state.goLoader) && /*#__PURE__*/_react["default"].createElement("span", null, "Export"), this.props.state.loading && this.props.state.exportLoader && /*#__PURE__*/_react["default"].createElement("span", null, "Fetching...")) : /*#__PURE__*/_react["default"].createElement("button", {
        type: "submit",
        disabled: this.props.state.loading,
        "data-name": "go",
        value: this.props.state.go,
        id: "resizeButton",
        onClick: function onClick(e) {
          _this25.props.onClick();
        },
        "data-toggle": "modal",
        "data-target": ".gopopup",
        className: this.props.state.loading ? 'btn btn-primary btn-sm float-right loaderButton' : 'btn btn-primary btn-sm float-right'
      }, this.props.state.loading && this.props.state.goLoader && /*#__PURE__*/_react["default"].createElement("i", {
        className: "fa fa-refresh fa-spin",
        style: {
          marginRight: '5px'
        }
      }), (!this.props.state.loading || this.props.state.exportLoader) && /*#__PURE__*/_react["default"].createElement("span", null, "Go"), this.props.state.loading && this.props.state.goLoader && /*#__PURE__*/_react["default"].createElement("span", null, "Fetching...")));
    }
  }]);

  return BreadCrumpTrailButton;
}(_react["default"].Component);

var DashboardGoogleMap = /*#__PURE__*/function (_Component) {
  _inherits(DashboardGoogleMap, _Component);

  var _super18 = _createSuper(DashboardGoogleMap);

  function DashboardGoogleMap(props) {
    var _this26;

    _classCallCheck(this, DashboardGoogleMap);

    _this26 = _super18.call(this, props);

    _defineProperty(_assertThisInitialized(_this26), "polyLien1", {
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      clickable: false,
      draggable: false,
      editable: false,
      visible: true,
      radius: 30000,
      zIndex: 1
    });

    _defineProperty(_assertThisInitialized(_this26), "mapStyle", {
      width: '100%',
      height: '90vh'
    });

    _defineProperty(_assertThisInitialized(_this26), "Clusterers1", {
      imagePath: 'http://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
    });

    _defineProperty(_assertThisInitialized(_this26), "getUrlSegments", function (url) {
      var urlSegs = url.split('/');

      if (Array.isArray(urlSegs)) {
        var len1 = urlSegs.length;
        var lastSeg = urlSegs[len1 - 1];
        lastSeg = lastSeg.split('?');
        urlSegs[len1 - 1] = lastSeg[0];
        return urlSegs;
      } else {
        return [];
      }
    });

    _defineProperty(_assertThisInitialized(_this26), "getUrlLastSegments", function (url) {
      var urlSegs = _this26.getUrlSegments(url);

      var len1 = urlSegs.length;

      if (len1 == 0) {
        return '';
      } else {
        return urlSegs[len1 - 1];
      }
    });

    _defineProperty(_assertThisInitialized(_this26), "checkUnauthorized", function (status, msg) {
      if (status == 'UNAUTHORIZED') {
        localStorage.clear();
        localStorage.setItem('logmsg', msg);
        window.location = '/login';
      }
    });

    _defineProperty(_assertThisInitialized(_this26), "getActiveTruckTracking", function (request) {
      var BASE_URL = _this26.props.envData;
      var COMPANY_AUTH_ELD_ENDPOINT = BASE_URL + 'trackingresource/getActiveTruckTracking?assetId=' + request.assetId;

      if (request.fromDate) {
        COMPANY_AUTH_ELD_ENDPOINT = COMPANY_AUTH_ELD_ENDPOINT + '&fromDate=' + request.fromDate;
      }

      return fetch(COMPANY_AUTH_ELD_ENDPOINT, {
        method: 'GET',
        headers: _this26.authHeader()
      }).then(function (response) {
        return response.json();
      }).then(function (responseData) {
        _this26.checkUnauthorized(responseData.status, responseData.message);

        return responseData;
      })["catch"](function (error) {
        return {};
      });
    });

    _defineProperty(_assertThisInitialized(_this26), "componentDidUpdate", function (prevProps) {
      if (prevProps.mapCenter != _this26.props.mapCenter) {
        _this26.mapCenter = _this26.props.mapCenter;
      }

      if (_this26.map) {
        _this26.map.setZoom(_this26.mapZoom);

        _this26.map.setCenter(_this26.mapCenter);
      }

      if (_this26.props.mapFlag != prevProps.mapFlag) {
        _this26.activeTruckId = null;

        if (_this26.activeMovingTimer != null) {
          clearInterval(_this26.activeMovingTimer);
          _this26.activeMovingTimer = null;
        }

        if (_this26.props.mapViewMode == 'moving') {
          _this26.mapZoom = 12;

          _this26.getLastTrackingData(_this26.props.truckInfo.id);
        } else {
          _this26.mapZoom = _this26.props.mapZoom;
        }

        _this26.setState(_objectSpread(_objectSpread({}, _this26.state), {}, {
          markerPopupShow: false,
          currentMarker: null,
          activeMovingData: []
        }));
      }
    });

    _defineProperty(_assertThisInitialized(_this26), "getLastTrackingData", function (activeTruckId) {
      var path1 = _this26.getUrlLastSegments(_this26.props.location.pathname);

      var path2 = _this26.getUrlLastSegments(window.location.toString());

      path1 = path1.replace('#', '');
      path2 = path2.replace('#', '');

      if (activeTruckId == _this26.props.truckInfo.id && _this26.props.mapViewMode == 'moving' && path1 == path2) {
        _this26.activeTruckId = activeTruckId;
        var rndval = Date.now();

        _this26.getActiveTruckTracking({
          rndval: rndval,
          assetId: activeTruckId
        }).then(function (response) {
          if (_this26.activeTruckId == _this26.props.truckInfo.id && response && response.TrackingData && response.TrackingData.length > 0) {
            var lat = parseFloat(response.TrackingData[0].latitude);
            var lng = parseFloat(response.TrackingData[0].longitude);
            var lastLat = 0;
            var lastLng = 0;
            var len1 = _this26.state.activeMovingData.length;

            if (len1 > 0) {
              lastLat = _this26.state.activeMovingData[len1 - 1].lat;
              lastLng = _this26.state.activeMovingData[len1 - 1].lng;
            }

            var activeMovingData = [];

            if (!(isNaN(lat) || isNaN(lng))) {
              if (lastLat != lat || lastLng != lng) {
                activeMovingData = [].concat(_toConsumableArray(_this26.state.activeMovingData), [{
                  lat: lat,
                  lng: lng
                }]);
                var movingMarkers = len1 == 0 ? [response.TrackingData[0]] : [_this26.state.movingMarkers[0], response.TrackingData[0]];

                var bounds = _this26.map.getBounds().contains({
                  lat: lat,
                  lng: lng
                });

                if (!bounds) {
                  _this26.mapZoom--;
                }

                _this26.setState(_objectSpread(_objectSpread({}, _this26.state), {}, {
                  activeMovingData: activeMovingData,
                  movingMarkers: movingMarkers
                }));
              }
            }
          }

          if (_this26.activeTruckId == _this26.props.truckInfo.id) {
            _this26.activeMovingTimer = setTimeout(function () {
              _this26.getLastTrackingData(_this26.props.truckInfo.id);
            }, 15000);
          }
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this26), "onClickMarker", function (e, data, index) {
      if (index == _this26.state.currentMarker) return;
      var speed = parseFloat(data.speed);

      if (isNaN(speed)) {
        speed = '';
      } else {
        speed = speed.toFixed(2) + ' mph';
      }

      var infwCenter = {
        lat: parseFloat(data.latitude),
        lng: parseFloat(data.longitude)
      };
      var infwData = {
        truckNo: data.truckNo,
        date: data.convertedDate,
        speed: speed,
        location: data.location
      };

      _this26.setState(_objectSpread(_objectSpread({}, _this26.state), {}, {
        markerPopupShow: true,
        currentMarker: index,
        infwCenter: infwCenter,
        infwData: infwData
      }));
    });

    _defineProperty(_assertThisInitialized(_this26), "loadMarker", function () {
      var mapData = [];

      if (_this26.props.mapViewMode == 'moving') {
        mapData = _toConsumableArray(_this26.state.movingMarkers);
        var len1 = mapData.length;

        if (len1 > 0) {
          if (len1 > 1) {
            var icon1 = 'http://maps.google.com/mapfiles/ms/icons/red.png';
            var icon2 = 'http://maps.google.com/mapfiles/ms/icons/green.png';
            var pos1 = {
              lat: parseFloat(mapData[0].latitude),
              lng: parseFloat(mapData[0].longitude)
            };
            var pos2 = {
              lat: parseFloat(mapData[1].latitude),
              lng: parseFloat(mapData[1].longitude)
            };
            var data1 = {
              truckNo: _this26.props.truckInfo.truckNo,
              convertedDate: mapData[0].convertedDate,
              speed: mapData[0].speed,
              location: mapData[0].location,
              latitude: mapData[0].latitude,
              longitude: mapData[0].longitude
            };
            var data2 = {
              truckNo: _this26.props.truckInfo.truckNo,
              convertedDate: mapData[1].convertedDate,
              speed: mapData[1].speed,
              location: mapData[1].location,
              latitude: mapData[1].latitude,
              longitude: mapData[1].longitude
            };
            return [/*#__PURE__*/_react["default"].createElement(_api.Marker, {
              key: 0,
              icon: icon1,
              position: pos1,
              onMouseOver: function onMouseOver(e) {
                _this26.onClickMarker(e, data1, 0);
              }
            }), /*#__PURE__*/_react["default"].createElement(_api.Marker, {
              key: 1,
              icon: icon2,
              position: pos2,
              onMouseOver: function onMouseOver(e) {
                _this26.onClickMarker(e, data2, 1);
              }
            })];
          } else {
            var _icon = 'http://maps.google.com/mapfiles/ms/icons/green.png';
            var _pos = {
              lat: parseFloat(mapData[0].latitude),
              lng: parseFloat(mapData[0].longitude)
            };
            var _data = {
              truckNo: _this26.props.truckInfo.truckNo,
              convertedDate: mapData[0].convertedDate,
              speed: mapData[0].speed,
              location: mapData[0].location,
              latitude: mapData[0].latitude,
              longitude: mapData[0].longitude
            };
            return [/*#__PURE__*/_react["default"].createElement(_api.Marker, {
              key: 0,
              icon: _icon,
              position: _pos,
              onMouseOver: function onMouseOver(e) {
                _this26.onClickMarker(e, _data, 0);
              }
            })];
          }
        } else {
          return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null);
        }
      } else {
        mapData = _toConsumableArray(_this26.props.mapData);
        var icon = 'http://maps.google.com/mapfiles/ms/icons/green.png';

        if (_this26.props.truckstate == 'stopped') {
          icon = 'http://maps.google.com/mapfiles/ms/icons/red.png';
        } else if (_this26.props.truckstate == 'ideal') {
          icon = 'http://maps.google.com/mapfiles/ms/icons/yellow.png';
        }

        return /*#__PURE__*/_react["default"].createElement(_api.MarkerClusterer, {
          options: _this26.Clusterers1
        }, function (clusterer) {
          return mapData.map(function (row, index) {
            var pos = {
              lat: parseFloat(row.latitude),
              lng: parseFloat(row.longitude)
            };
            return /*#__PURE__*/_react["default"].createElement(_api.Marker, {
              key: index,
              icon: icon,
              position: pos,
              onMouseOver: function onMouseOver(e) {
                _this26.onClickMarker(e, row, index);
              },
              clusterer: clusterer
            });
          });
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this26), "infClose", function () {
      _this26.setState(_objectSpread(_objectSpread({}, _this26.state), {}, {
        markerPopupShow: false,
        currentMarker: null
      }));
    });

    _defineProperty(_assertThisInitialized(_this26), "render", function () {
      return /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement(_api.LoadScript, {
        googleMapsApiKey: _this26.GOOGLE_MAP_KEY
      }, /*#__PURE__*/_react["default"].createElement(_api.GoogleMap, {
        key: 'B1',
        mapContainerStyle: _this26.mapStyle,
        zoom: _this26.mapZoom,
        center: _this26.mapCenter,
        onLoad: function onLoad(map) {
          _this26.map = map;
        }
      }, _this26.loadMarker(), _this26.state.markerPopupShow && /*#__PURE__*/_react["default"].createElement(_api.InfoWindow, {
        position: _this26.state.infwCenter,
        onCloseClick: function onCloseClick() {
          _this26.infClose();
        }
      }, /*#__PURE__*/_react["default"].createElement("span", null, /*#__PURE__*/_react["default"].createElement("p", null, "Truck/Asset No: ", _this26.state.infwData.truckNo), /*#__PURE__*/_react["default"].createElement("p", null, "Date: ", _this26.state.infwData.date), /*#__PURE__*/_react["default"].createElement("p", null, "Speed: ", _this26.state.infwData.speed), /*#__PURE__*/_react["default"].createElement("p", null, "Location: ", _this26.state.infwData.location))), /*#__PURE__*/_react["default"].createElement(_api.Polyline, {
        path: _this26.state.activeMovingData,
        options: _this26.polyLien1
      }))));
    });

    _this26.state = {
      markerPopupShow: false,
      currentMarker: null,
      infwCenter: {
        lat: 41.89101,
        lng: -87.62342
      },
      infwData: {},
      activeMovingData: [],
      movingMarkers: []
    };
    _this26.GOOGLE_MAP_KEY = localStorage.getItem('GOOGLE_MAP_KEY');
    _this26.map = null;
    _this26.activeMovingTimer = null;
    _this26.activeTruckId = null;
    _this26.mapCenter = props.mapCenter;
    _this26.mapZoom = props.mapZoom;
    return _this26;
  }

  _createClass(DashboardGoogleMap, [{
    key: "authHeader",
    value: function authHeader() {
      var authToken = localStorage.getItem('authToken');

      if (authToken) {
        return {
          'Content-Type': 'application/json',
          'Authorization': authToken
        };
      } else {
        return {
          'Content-Type': 'application/json'
        };
      }
    }
  }]);

  return DashboardGoogleMap;
}(_react.Component);

var InviteNewCarrierWithOutExistingData = /*#__PURE__*/function (_React$Component18) {
  _inherits(InviteNewCarrierWithOutExistingData, _React$Component18);

  var _super19 = _createSuper(InviteNewCarrierWithOutExistingData);

  function InviteNewCarrierWithOutExistingData() {
    _classCallCheck(this, InviteNewCarrierWithOutExistingData);

    return _super19.apply(this, arguments);
  }

  _createClass(InviteNewCarrierWithOutExistingData, [{
    key: "render",
    value: function render() {
      var _this27 = this;

      return /*#__PURE__*/_react["default"].createElement("article", {
        className: "table-data truck-data shadow bg-white rounded",
        style: {
          display: 'block'
        }
      }, /*#__PURE__*/_react["default"].createElement("form", {
        id: "new_carrier"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group",
        "data-tip": true,
        "data-for": 'value' + this.props.state.carrierName
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Carrier Name"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.carrierName.has_carrier == true ? true : false,
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "Carrier Name",
        name: "carrierName",
        onChange: function onChange(e) {
          _this27.props.set_carrierName(e);
        },
        value: this.props.state.carrierName
      }), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
        id: 'value' + this.props.state.carrierName
      }, this.props.state.carrierName)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "MC#"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.mcNumber.has_mcnumber == true ? true : false,
        type: "text",
        className: "form-control  json-col",
        placeholder: "MC#",
        name: "mcNumber",
        onChange: function onChange(e) {
          _this27.props.set_mcNumber(e);
        },
        value: this.props.state.mcNumber
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "DOT#"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.dotNumber.has_dotnumber == true ? true : false,
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "DOT#",
        name: "dotNumber",
        onChange: function onChange(e) {
          _this27.props.set_dotnumber(e);
        },
        value: this.props.state.dotNumber
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Carrier Email"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "email",
        className: "form-control requiredfield json-col",
        placeholder: "Carrier Email",
        name: "email",
        onChange: function onChange(e) {
          _this27.props.set_email(e);
        },
        value: this.props.state.email
      }), /*#__PURE__*/_react["default"].createElement("span", {
        className: "shadow-input"
      }, this.props.state.emailError)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "First Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "First Name",
        name: "firstName",
        onChange: function onChange(e) {
          _this27.props.set_firstName(e);
        },
        value: this.props.state.firstName
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Last Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "Last Name",
        name: "lastName",
        onChange: function onChange(e) {
          _this27.props.set_lastName(e);
        },
        value: this.props.state.lastName
      })))));
    }
  }]);

  return InviteNewCarrierWithOutExistingData;
}(_react["default"].Component);

var InviteNewCarrierWithExistingData = /*#__PURE__*/function (_React$Component19) {
  _inherits(InviteNewCarrierWithExistingData, _React$Component19);

  var _super20 = _createSuper(InviteNewCarrierWithExistingData);

  function InviteNewCarrierWithExistingData() {
    _classCallCheck(this, InviteNewCarrierWithExistingData);

    return _super20.apply(this, arguments);
  }

  _createClass(InviteNewCarrierWithExistingData, [{
    key: "render",
    value: function render() {
      var _this28 = this;

      return /*#__PURE__*/_react["default"].createElement("article", {
        className: "table-data truck-data shadow bg-white rounded",
        style: {
          display: 'block'
        }
      }, /*#__PURE__*/_react["default"].createElement("form", {
        id: "new_carrier"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group",
        "data-tip": true,
        "data-for": 'value' + this.props.state.carrierName
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Carrier Name"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.carrierName.has_carrier == true ? true : false,
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "Carrier Name",
        name: "carrierName",
        onChange: function onChange(e) {
          _this28.props.set_carrierName(e);
        },
        value: this.props.state.carrierName
      }), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
        id: 'value' + this.props.state.carrierName
      }, this.props.state.carrierName)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "MC#"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.carrierName.has_mcnumber == true ? true : false,
        type: "text",
        className: "form-control  json-col",
        placeholder: "MC#",
        name: "mcNumber",
        onChange: function onChange(e) {
          _this28.props.set_mcNumber(e);
        },
        value: this.props.state.mcNumber
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "DOT#"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.carrierName.has_dotnumber == true ? true : false,
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "DOT#",
        name: "dotNumber",
        onChange: function onChange(e) {
          _this28.props.set_dotnumber(e);
        },
        value: this.props.state.dotNumber
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group",
        "data-tip": true,
        "data-for": 'email' + this.props.state.email
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Carrier Email"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.email.has_email == true ? true : false,
        type: "email",
        className: "form-control requiredfield json-col",
        placeholder: "Carrier Email",
        name: "email",
        onChange: function onChange(e) {
          _this28.props.set_email(e);
        },
        value: this.props.state.email
      }), /*#__PURE__*/_react["default"].createElement("span", {
        className: "shadow-input"
      }, this.props.state.emailError), /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
        id: 'email' + this.props.state.email
      }, this.props.state.email)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group",
        "data-tip": true,
        "data-for": 'firstname' + this.props.state.firstName
      }, /*#__PURE__*/_react["default"].createElement("label", null, "First Name"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.email.has_firstname == true ? true : false,
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "First Name",
        name: "firstName",
        onChange: function onChange(e) {
          _this28.props.set_firstName(e);
        },
        value: this.props.state.firstName
      }), this.props.state.firstName !== null ? /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
        id: 'firstname' + this.props.state.firstName
      }, this.props.state.firstName) : ''), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group",
        "data-tip": true,
        "data-for": 'lastname' + this.props.state.lastName
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Last Name"), /*#__PURE__*/_react["default"].createElement("input", {
        disabled: !this.props.state.email.has_lastname == true ? true : false,
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "Last Name",
        name: "lastName",
        onChange: function onChange(e) {
          _this28.props.set_lastName(e);
        },
        value: this.props.state.lastName
      }), this.props.state.lastName !== null ? /*#__PURE__*/_react["default"].createElement(_reactTooltip["default"], {
        id: 'lastname' + this.props.state.lastName
      }, this.props.state.lastName) : ''))));
    }
  }]);

  return InviteNewCarrierWithExistingData;
}(_react["default"].Component);

var NewUserComponent = /*#__PURE__*/function (_React$Component20) {
  _inherits(NewUserComponent, _React$Component20);

  var _super21 = _createSuper(NewUserComponent);

  function NewUserComponent() {
    _classCallCheck(this, NewUserComponent);

    return _super21.apply(this, arguments);
  }

  _createClass(NewUserComponent, [{
    key: "render",
    value: function render() {
      var _this29 = this;

      return /*#__PURE__*/_react["default"].createElement("form", {
        id: "new_user"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "First Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "First Name",
        name: "firstName",
        onChange: function onChange(e) {
          _this29.props.set_firstName(e);
        },
        value: this.props.state.firstName
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Last Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control json-col",
        placeholder: "Last Name",
        name: "lastName",
        onChange: function onChange(e) {
          _this29.props.set_lastName(e);
        },
        value: this.props.state.lastName
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Address"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control json-col",
        placeholder: "Address",
        name: "currentAddress1",
        onChange: function onChange(e) {
          _this29.props.set_currentAddress1(e);
        },
        value: this.props.state.currentAddress1
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Email"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "Email",
        name: "emailId",
        onChange: function onChange(e) {
          _this29.props.set_emailId(e);
        },
        value: this.props.state.emailId
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Role"), this.props.renderRoles), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Mobile Number"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control json-col",
        placeholder: "Mobile Number",
        name: "phoneNumber",
        onChange: function onChange(e) {
          _this29.props.set_phoneNumber(e);
        },
        value: this.props.state.phoneNumber
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-4 form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "State"), this.props.renderState), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-8",
        id: "roleDesc"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-4 form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "City"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control json-col",
        placeholder: "City",
        name: "City",
        onChange: function onChange(e) {
          _this29.props.set_City(e);
        },
        value: this.props.state.City
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-4 form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Zip"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control json-col",
        placeholder: "Zip",
        name: "Zip",
        onChange: function onChange(e) {
          _this29.props.set_Zip(e);
        },
        value: this.props.state.Zip
      })), this.props.getEmailTempaleField), /*#__PURE__*/_react["default"].createElement("input", {
        type: "hidden",
        className: "json-col",
        name: "userTypeId",
        value: this.props.state.userTypeId
      }), /*#__PURE__*/_react["default"].createElement("input", {
        type: "hidden",
        className: "json-col",
        name: "companyId",
        value: this.props.state.companyId
      }), /*#__PURE__*/_react["default"].createElement("input", {
        type: "hidden",
        className: "json-col",
        name: "userId",
        value: this.props.state.userId
      }));
    }
  }]);

  return NewUserComponent;
}(_react["default"].Component);

var NewEmailTemplate = /*#__PURE__*/function (_React$Component21) {
  _inherits(NewEmailTemplate, _React$Component21);

  var _super22 = _createSuper(NewEmailTemplate);

  function NewEmailTemplate() {
    _classCallCheck(this, NewEmailTemplate);

    return _super22.apply(this, arguments);
  }

  _createClass(NewEmailTemplate, [{
    key: "render",
    value: function render() {
      var _this30 = this;

      return /*#__PURE__*/_react["default"].createElement("form", {
        id: "new_template"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Template Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "Template Name",
        name: "emailTemplateName",
        onChange: function onChange(e) {
          _this30.props.set_templateName(e);
        },
        value: this.props.state.templateName
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Template Type"), this.props.renderTemplateTypes), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Email Accounts"), this.props.renderAccountList)), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement(_tinymceReact.Editor, {
        apiKey: "t21f9uu8xkoyas4d800dojz0q68xajd2gcs62n8ocskunmxe",
        init: {
          selector: 'textarea#myTextArea',
          paste_data_images: true,
          automatic_uploads: false,
          file_picker_types: 'image',
          relative_urls: false,
          remove_script_host: false,
          menubar: false,
          statusbar: false,
          height: 300,
          toolbar: 'undo redo preview | formatselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | insertsitelink insertsitelink1',
          setup: function setup(editor) {
            editor.ui.registry.addMenuButton('insertsitelink', {
              text: 'Web Links',
              icon: 'link',
              fetch: function fetch(callback) {
                var items = [];
                var myListItems = weblinkdata;

                function iterate(row, index) {
                  items.push({
                    type: 'menuitem',
                    text: row['title'],
                    content: row['description'],
                    onAction: function onAction() {
                      editor.insertContent(row['description']);
                    }
                  });
                }

                myListItems.forEach(iterate);
                callback(items);
              },
              onAction: function onAction(_) {
                editor.insertContent('&nbsp;<strong>Cloning...</strong>&nbsp;');
              }
            });
            editor.ui.registry.addMenuButton('insertsitelink1', {
              text: 'Template Tags',
              icon: 'link',
              fetch: function fetch(callback) {
                var items = [];
                var myListItems = alltags;

                function iterate(row, index) {
                  items.push({
                    type: 'menuitem',
                    text: row['title'],
                    onAction: function onAction() {
                      editor.insertContent(row['description']);
                    }
                  });
                }

                myListItems.forEach(iterate);
                callback(items);
              },
              onAction: function onAction(_) {
                editor.insertContent('&nbsp;<strong>Cloning...</strong>&nbsp;');
              }
            });
          }
        },
        textareaName: "templateContent",
        value: this.props.state.templateContent,
        onEditorChange: function onEditorChange(e) {
          _this30.props.handleEditorChange(e);
        }
      }), /*#__PURE__*/_react["default"].createElement("input", {
        type: "hidden",
        name: "templateContentX",
        value: this.props.state.templateContent
      }))), /*#__PURE__*/_react["default"].createElement("input", {
        type: "hidden",
        className: "json-col",
        name: "templateId",
        value: this.props.state.templateId
      }));
    }
  }]);

  return NewEmailTemplate;
}(_react["default"].Component);

var NewEmailAccountComponent = /*#__PURE__*/function (_React$Component22) {
  _inherits(NewEmailAccountComponent, _React$Component22);

  var _super23 = _createSuper(NewEmailAccountComponent);

  function NewEmailAccountComponent() {
    _classCallCheck(this, NewEmailAccountComponent);

    return _super23.apply(this, arguments);
  }

  _createClass(NewEmailAccountComponent, [{
    key: "render",
    value: function render() {
      var _this31 = this;

      return /*#__PURE__*/_react["default"].createElement("form", {
        id: "new_account"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Sender Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "First Name",
        name: "senderName",
        onChange: function onChange(e) {
          _this31.props.set_senderName(e);
        },
        value: this.props.state.senderName
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Sender Email"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control requiredfield json-col",
        placeholder: "Sender Email",
        name: "senderEmail",
        onChange: function onChange(e) {
          _this31.props.set_senderEmail(e);
        },
        value: this.props.state.senderEmail
      }))), /*#__PURE__*/_react["default"].createElement("input", {
        type: "hidden",
        className: "json-col",
        name: "id",
        value: this.props.state.accountId
      }));
    }
  }]);

  return NewEmailAccountComponent;
}(_react["default"].Component);

var BrandingComponent = /*#__PURE__*/function (_React$Component23) {
  _inherits(BrandingComponent, _React$Component23);

  var _super24 = _createSuper(BrandingComponent);

  function BrandingComponent() {
    _classCallCheck(this, BrandingComponent);

    return _super24.apply(this, arguments);
  }

  _createClass(BrandingComponent, [{
    key: "render",
    value: function render() {
      var _this32 = this;

      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col logo-container"
      }, /*#__PURE__*/_react["default"].createElement("img", {
        src: "".concat(this.props.state.profile.logo, "?").concat(new Date().getTime()),
        className: "profileLogo"
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Change Logo"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "file",
        className: "form-control",
        id: "profileLogo",
        accept: "image/*",
        onChange: function onChange(e) {
          _this32.props.saveLogo(e);
        }
      }), /*#__PURE__*/_react["default"].createElement("div", {
        "class": "card prod-card mt-4"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        "class": "card-body"
      }, /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("strong", null, "Logo Image Size")), /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Height :"), " 35px"), /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Width :"), " 230px")))))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col logo-container"
      }, /*#__PURE__*/_react["default"].createElement("img", {
        src: "".concat(this.props.state.profile.banner, "?").concat(new Date().getTime()),
        className: "profileLogo"
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Change Banner"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "file",
        className: "form-control",
        id: "profileBanner",
        accept: "image/*",
        onChange: function onChange(e) {
          _this32.props.saveBanner(e);
        }
      }), /*#__PURE__*/_react["default"].createElement("div", {
        "class": "card prod-card mt-4"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        "class": "card-body"
      }, /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("strong", null, "Banner Image Size")), /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Height :"), " 400px"), /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Width :"), " 400px")))))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col logo-container"
      }, /*#__PURE__*/_react["default"].createElement("img", {
        src: "".concat(this.props.state.profile.favicon, "?").concat(new Date().getTime()),
        className: "profileFavicon"
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Change Favicon"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "file",
        className: "form-control",
        id: "profileFavicon",
        accept: "image/*",
        onChange: function onChange(e) {
          _this32.props.saveFavicon(e);
        }
      }), /*#__PURE__*/_react["default"].createElement("div", {
        "class": "card prod-card mt-4"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        "class": "card-body"
      }, /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("strong", null, "Favicon Image Size")), /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Height :"), " 16px"), /*#__PURE__*/_react["default"].createElement("p", {
        "class": "small"
      }, /*#__PURE__*/_react["default"].createElement("b", null, "Width :"), " 16px")))))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Theme Color"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "form-control json-col",
        placeholder: "Theme Color",
        name: "colourCode",
        onChange: function onChange(e) {
          _this32.props.set_colourCode(e);
        },
        value: this.props.state.profile.colourCode,
        onClick: function onClick(e) {
          _this32.props.openColor(e);
        }
      }), /*#__PURE__*/_react["default"].createElement(_reactColor.SketchPicker, {
        className: this.props.state.colorClass,
        color: this.props.state.colourCode,
        onChangeComplete: this.props.handleChangeComplete
      })))));
    }
  }]);

  return BrandingComponent;
}(_react["default"].Component);

var CompanyConfigComponent = /*#__PURE__*/function (_React$Component24) {
  _inherits(CompanyConfigComponent, _React$Component24);

  var _super25 = _createSuper(CompanyConfigComponent);

  function CompanyConfigComponent() {
    _classCallCheck(this, CompanyConfigComponent);

    return _super25.apply(this, arguments);
  }

  _createClass(CompanyConfigComponent, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement("article", {
        className: "table-data truck-data shadow bg-white rounded"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-6"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-md-12"
      }, /*#__PURE__*/_react["default"].createElement("h3", null, "Company Details"))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "companyName",
        component: renderField,
        className: "form-control",
        label: "Company Name"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "companyId",
        component: renderField,
        className: "form-control",
        label: "Company ID"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "companyType",
        component: renderField,
        className: "form-control",
        label: "Company Type"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "mc",
        component: renderField,
        className: "form-control",
        label: "MC"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "number",
        name: "dot",
        component: renderField,
        className: "form-control",
        label: "DOT"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "scac",
        component: renderField,
        className: "form-control",
        label: "SCAC"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "email",
        component: renderField,
        className: "form-control",
        label: "Support Email"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "enableSubscription",
        component: renderCheckbox1,
        className: "form-control",
        label: "Enable Subscription"
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-6"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-md-12"
      }, /*#__PURE__*/_react["default"].createElement("h3", null, "Company Contacts"))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "currentAddress1",
        component: renderField,
        className: "form-control",
        label: "Address"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "city",
        component: renderField,
        label: "City"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "state",
        controlClass: "requiredfield",
        component: renderField,
        label: "State"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "zipcode",
        component: renderField,
        className: "form-control",
        label: "Zip"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "companyEmail",
        component: renderField,
        controlClass: "requiredfield",
        label: "Email"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "websiteAddress",
        component: renderField,
        label: "Website"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "phoneNumber",
        component: renderField,
        className: "form-control",
        label: "Phone Number"
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col text-center"
      }, /*#__PURE__*/_react["default"].createElement("button", {
        type: "button",
        className: "btn btn-primary",
        disabled: true
      }, "Save")));
    }
  }]);

  return CompanyConfigComponent;
}(_react["default"].Component);

var Carrierprofiledetailsview = /*#__PURE__*/function (_Component2) {
  _inherits(Carrierprofiledetailsview, _Component2);

  var _super26 = _createSuper(Carrierprofiledetailsview);

  function Carrierprofiledetailsview(props) {
    _classCallCheck(this, Carrierprofiledetailsview);

    return _super26.call(this, props);
  }

  _createClass(Carrierprofiledetailsview, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement("form", {
        className: "tg-forms"
      }, /*#__PURE__*/_react["default"].createElement("article", {
        className: "table-data truck-data shadow bg-white rounded"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-3"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "companyName",
        component: renderField,
        label: "Carrier Name"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "specialityTypeName",
        component: renderField,
        label: "Speciality"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "currentAddress1",
        component: renderField,
        label: "Current Address"
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-3"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "companyEmail",
        component: renderField,
        label: "Email"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "phoneNumber",
        component: renderField,
        label: "Primary Phone"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "altPhoneNumber",
        component: renderField,
        label: "Secondary Phone"
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-3"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "currentCityName",
        component: renderField,
        label: "City"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "faxNumber",
        component: renderField,
        label: "Fax"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "websiteAddress",
        component: renderField,
        label: "Website"
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col col-md-3"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "currentStateName",
        component: renderField,
        label: "State"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "currentZipValue",
        component: renderField,
        label: "Zip Code"
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        type: "text",
        name: "currentCountryName",
        component: renderField,
        label: "Country"
      })))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-md-12"
      }, /*#__PURE__*/_react["default"].createElement(_reduxForm.Field, {
        name: "instructions",
        label: "Description",
        component: renderTextArea
      }))))));
    }
  }]);

  return Carrierprofiledetailsview;
}(_react.Component);

var PageOneForm = (0, _reduxForm.reduxForm)({
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true
})(Carrierprofiledetailsview);

var mapStateToProps = function mapStateToProps(state) {
  return {
    data: state.userReducer.data,
    CityVal: state.loadReducer.CityVal,
    ZipVal: state.loadReducer.ZipVal,
    form: 'profiledetailsview',
    validate: validate
  };
};

var PageOneFormContainerComponent = (0, _reactRedux.connect)(mapStateToProps)(PageOneForm);

var renderField = /*#__PURE__*/function (_Component3) {
  _inherits(renderField, _Component3);

  var _super27 = _createSuper(renderField);

  function renderField() {
    _classCallCheck(this, renderField);

    return _super27.apply(this, arguments);
  }

  _createClass(renderField, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          input = _this$props.input,
          label = _this$props.label,
          controlClass = _this$props.controlClass,
          id = _this$props.id,
          labelnotavailabe = _this$props.labelnotavailabe,
          data = _this$props.data,
          disable = _this$props.disable,
          type = _this$props.type,
          _this$props$meta = _this$props.meta,
          touched = _this$props$meta.touched,
          error = _this$props$meta.error;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "form-group ".concat(touched && error ? 'error' : '')
      }, labelnotavailabe ? '' : /*#__PURE__*/_react["default"].createElement("label", null, label), /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("input", _extends({}, input, {
        text: "".concat(data ? data : ''),
        id: "".concat(id ? id : ''),
        placeholder: label,
        type: type,
        disabled: "".concat(disable ? disable : ''),
        className: "form-control ".concat(controlClass ? controlClass : '')
      })), touched && error && /*#__PURE__*/_react["default"].createElement("span", {
        className: "error"
      }, error)));
    }
  }]);

  return renderField;
}(_react.Component);

var renderCheckbox1 = /*#__PURE__*/function (_Component4) {
  _inherits(renderCheckbox1, _Component4);

  var _super28 = _createSuper(renderCheckbox1);

  function renderCheckbox1() {
    _classCallCheck(this, renderCheckbox1);

    return _super28.apply(this, arguments);
  }

  _createClass(renderCheckbox1, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          input = _this$props2.input,
          label = _this$props2.label,
          name = _this$props2.name,
          id = _this$props2.id,
          value = _this$props2.value,
          type = _this$props2.type,
          controlClass = _this$props2.controlClass,
          _this$props2$meta = _this$props2.meta,
          touched = _this$props2$meta.touched,
          error = _this$props2$meta.error;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "parentDivCss"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "checkbox",
        id: id,
        name: name,
        value: value,
        className: "checkBoxCss"
      }), "\u2002", /*#__PURE__*/_react["default"].createElement("label", {
        "for": "vehicle3",
        className: "labelCss"
      }, " ", label), touched && error && /*#__PURE__*/_react["default"].createElement("span", {
        className: "error"
      }, error));
    }
  }]);

  return renderCheckbox1;
}(_react.Component);

var ViewDriverPOPUPComponent = /*#__PURE__*/function (_Component5) {
  _inherits(ViewDriverPOPUPComponent, _Component5);

  var _super29 = _createSuper(ViewDriverPOPUPComponent);

  function ViewDriverPOPUPComponent() {
    _classCallCheck(this, ViewDriverPOPUPComponent);

    return _super29.apply(this, arguments);
  }

  _createClass(ViewDriverPOPUPComponent, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement("form", {
        id: "view_driver"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "First Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss  json-col",
        placeholder: "First Name",
        name: "firstName",
        value: this.props.state.firstName
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Last Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Last Name",
        name: "lastName",
        value: this.props.state.lastName
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "License Exp. Date"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "License Exp. Date",
        name: "drivingLicenseExpDate",
        value: this.props.state.drivingLicenseExpDate
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "License State"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "License State",
        name: "drivingLicenseStateCode",
        value: this.props.state.drivingLicenseStateCode
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "License Number"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "License Number",
        name: "drivingLicenseNo",
        value: this.props.state.drivingLicenseNo
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col  form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Email"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Email",
        name: "emailId",
        value: this.props.state.emailId
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col  form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Mobile Number"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Mobile Number",
        name: "phoneNumber",
        value: this.props.state.phoneNumber
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "State"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "State",
        name: "state",
        value: this.props.state.state
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "City"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "City",
        name: "city",
        value: this.props.state.city
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Address"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Address",
        name: "address",
        value: this.props.state.address
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "ELD Provider"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "ELD Provider",
        name: "eldProvider",
        value: this.props.state.eldProvider
      }))));
    }
  }]);

  return ViewDriverPOPUPComponent;
}(_react.Component);

var LoadDropdown = /*#__PURE__*/function (_Component6) {
  _inherits(LoadDropdown, _Component6);

  var _super30 = _createSuper(LoadDropdown);

  function LoadDropdown() {
    _classCallCheck(this, LoadDropdown);

    return _super30.apply(this, arguments);
  }

  _createClass(LoadDropdown, [{
    key: "getRow",
    value: function getRow(data, value) {
      switch (value) {
        case 'cid':
          return data ? data.map(function (row, index) {
            return /*#__PURE__*/_react["default"].createElement("option", {
              key: index,
              "data-lanedetailsindex": index,
              value: row.companyId
            }, row.companyName);
          }) : /*#__PURE__*/_react["default"].createElement("option", null);

        case 'id':
          return data ? data.map(function (row, index) {
            return /*#__PURE__*/_react["default"].createElement("option", {
              key: index,
              "data-lanedetailsindex": index,
              value: row.id
            }, row.name);
          }) : /*#__PURE__*/_react["default"].createElement("option", null);

        case 'c_id':
          return data ? data.map(function (row, index) {
            return /*#__PURE__*/_react["default"].createElement("option", {
              key: index,
              "data-lanedetailsindex": index,
              value: row.Company_Id
            }, row.Company_Name);
          }) : /*#__PURE__*/_react["default"].createElement("option", null);

        case 'lane':
          return data ? data.map(function (row, index) {
            return /*#__PURE__*/_react["default"].createElement("option", {
              key: index,
              value: row.laneId
            }, row.selectLane);
          }) : /*#__PURE__*/_react["default"].createElement("option", null);

        case 'cState':
          return data ? data.map(function (row, index) {
            return /*#__PURE__*/_react["default"].createElement("option", {
              key: index,
              value: row.id
            }, row.name);
          }) : /*#__PURE__*/_react["default"].createElement("option", {
            value: ""
          }, "Loading...");

        case 'cCity':
          return data.length > 0 ? data.map(function (row, index) {
            return /*#__PURE__*/_react["default"].createElement("option", {
              key: index,
              value: row.cityId
            }, row.cityName);
          }) : /*#__PURE__*/_react["default"].createElement("option", {
            value: ""
          }, "Loading...");

        case 'cZip':
          return data.length > 0 ? data.map(function (row, index) {
            return /*#__PURE__*/_react["default"].createElement("option", {
              key: index,
              value: row.zipId
            }, row.zipValue);
          }) : /*#__PURE__*/_react["default"].createElement("option", {
            value: ""
          }, "Loading...");
      }
    }
  }, {
    key: "render",
    value: function render() {
      var value = this.props.value;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, this.props.label), /*#__PURE__*/_react["default"].createElement("select", {
        onChange: this.props.onChange,
        className: "form-control json-col",
        id: this.props.id,
        name: this.props.name
      }, /*#__PURE__*/_react["default"].createElement("option", {
        value: "",
        hidden: true
      }, this.props.placeHolder), this.getRow(this.props.data, value)));
    }
  }]);

  return LoadDropdown;
}(_react.Component);

var LoadInput = /*#__PURE__*/function (_Component7) {
  _inherits(LoadInput, _Component7);

  var _super31 = _createSuper(LoadInput);

  function LoadInput() {
    _classCallCheck(this, LoadInput);

    return _super31.apply(this, arguments);
  }

  _createClass(LoadInput, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, this.props.label), /*#__PURE__*/_react["default"].createElement("input", {
        name: this.props.name,
        type: this.props.type,
        className: this.props["class"],
        placeholder: this.props.placeHolder
      }));
    }
  }]);

  return LoadInput;
}(_react.Component);

var ViewTruckPOPUPComponent = /*#__PURE__*/function (_Component8) {
  _inherits(ViewTruckPOPUPComponent, _Component8);

  var _super32 = _createSuper(ViewTruckPOPUPComponent);

  function ViewTruckPOPUPComponent() {
    _classCallCheck(this, ViewTruckPOPUPComponent);

    return _super32.apply(this, arguments);
  }

  _createClass(ViewTruckPOPUPComponent, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement("form", {
        id: "view_truck"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Truck/Asset No"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss  json-col",
        placeholder: "Truck/Asset No",
        name: "assetNumber",
        value: this.props.state.assetNumber
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "License Plate No"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "License Plate No",
        name: "licensePlateNo",
        value: this.props.state.licensePlateNo
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col  form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "VIN"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "VIN",
        name: "identificationNo",
        value: this.props.state.identificationNo
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col  form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Device ID"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Device ID",
        name: "assetSerialNo",
        value: this.props.state.assetSerialNo
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Asset Name"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Asset Name",
        name: "assetName",
        value: this.props.state.assetName
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Asset Type"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Asset Type",
        name: "assetType",
        value: this.props.state.assetType
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "License State"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "License State",
        name: "licenseState",
        value: this.props.state.licenseState
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "ELD Provider"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "ELD Provider",
        name: "eldProviderDisplayName",
        value: this.props.state.eldProviderDisplayName
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Make"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Make",
        name: "make",
        value: this.props.state.make
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Model"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Model",
        name: "model",
        value: this.props.state.model
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Year"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Year",
        name: "modelYear",
        value: this.props.state.modelYear
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Color"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Color",
        name: "color",
        value: this.props.state.color
      }))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col  form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Manufacturer"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Manufacturer",
        name: "manufacturer",
        value: this.props.state.manufacturer
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Axles"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Axles",
        name: "axles",
        value: this.props.state.axles
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col form-group"
      }, /*#__PURE__*/_react["default"].createElement("label", null, "Size"), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        className: "viewTruckCss json-col",
        placeholder: "Size",
        name: "size",
        value: this.props.state.size
      }))));
    }
  }]);

  return ViewTruckPOPUPComponent;
}(_react.Component);

module.exports = {
  SearchComponent: SearchComponent,
  CarrierInfo: CarrierInfo,
  EldSelection: EldSelection,
  AuthorizeEldRequired: AuthorizeEldRequired,
  AuthorizeEldAdditional: AuthorizeEldAdditional,
  UseExistingCredentials: UseExistingCredentials,
  NewCredentials: NewCredentials,
  TabsBar: TabsBar,
  TableDataComponent: TableDataComponent,
  SearchCarrierComponent: SearchCarrierComponent,
  HeaderBar: HeaderBar,
  DashboardTrucksComponent: DashboardTrucksComponent,
  GetCarrier: GetCarrier,
  GetEld: GetEld,
  DashboardTrucksTrmComponent: DashboardTrucksTrmComponent,
  BreadCrumpTrail: BreadCrumpTrail,
  BreadCrumpTrailButton: BreadCrumpTrailButton,
  DashboardGoogleMap: DashboardGoogleMap,
  InviteNewCarrierWithOutExistingData: InviteNewCarrierWithOutExistingData,
  InviteNewCarrierWithExistingData: InviteNewCarrierWithExistingData,
  NewUserComponent: NewUserComponent,
  NewEmailAccountComponent: NewEmailAccountComponent,
  NewEmailTemplate: NewEmailTemplate,
  Carrierprofiledetailsview: Carrierprofiledetailsview,
  BrandingComponent: BrandingComponent,
  CompanyConfigComponent: CompanyConfigComponent,
  renderField: renderField,
  renderCheckbox1: renderCheckbox1,
  ViewDriverPOPUPComponent: ViewDriverPOPUPComponent,
  ViewTruckPOPUPComponent: ViewTruckPOPUPComponent,
  LoadDropdown: LoadDropdown,
  LoadInput: LoadInput,
  PageOneFormContainerComponent: PageOneFormContainerComponent
};
// import React from 'react';
// import ReactTooltip from 'react-tooltip';
// import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
// class NewUserComponent extends React.Component {
//     render() {
//         return (
//             <article className="table-data truck-data shadow bg-white rounded" style={{ display: 'block' }}>
//                 <form id="new_carrier" >
//                     <div className="row">
//                         <div className="col form-group" data-tip data-for={"value" + this.props.state.carrierName}  >
//                             <label >Carrier Name</label>
//                             <input disabled={!this.props.state.carrierName.has_carrier == true ? true : false} type="text" className="form-control requiredfield json-col" placeholder="Carrier Name" name="carrierName" onChange={(e) => { this.props.set_carrierName(e) }} value={this.props.state.carrierName} />
//                             <ReactTooltip id={"value" + this.props.state.carrierName}>
//                                 {this.props.state.carrierName}
//                             </ReactTooltip>
//                         </div>
//                         <div className="col form-group">
//                             <label>MC#</label>
//                             <input disabled={!this.props.state.carrierName.has_mcnumber == true ? true : false} type="text" className="form-control  json-col" placeholder="MC#" name="mcNumber" onChange={(e) => { this.props.set_mcNumber(e) }} value={this.props.state.mcNumber} />
//                         </div>
//                         <div className="col form-group">
//                             <label>DOT#</label>
//                             <input disabled={!this.props.state.carrierName.has_dotnumber == true ? true : false} type="text" className="form-control requiredfield json-col" placeholder="DOT#" name="dotNumber" onChange={(e) => { this.props.set_dotnumber(e) }} value={this.props.state.dotNumber} />
//                         </div>
//                     </div>
//                     <div className="row">
//                         <div className="col form-group" data-tip data-for={"email" + this.props.state.email}>
//                             <label>Carrier Email</label>
//                             <input disabled={!this.props.state.email.has_email == true ? true : false} type="email" className="form-control requiredfield json-col" placeholder="Carrier Email" name="email" onChange={(e) => { this.props.set_email(e) }} value={this.props.state.email} />
//                             <span className="shadow-input">{this.props.state.emailError}</span>
//                             <ReactTooltip id={"email" + this.props.state.email}>{this.props.state.email}</ReactTooltip>
//                         </div>
//                         <div className="col form-group" data-tip data-for={"firstname" + this.props.state.firstName}>
//                             <label>First Name</label>
//                             <input disabled={!this.props.state.email.has_firstname == true ? true : false} type="text" className="form-control requiredfield json-col" placeholder="First Name" name="firstName" onChange={(e) => { this.props.set_firstName(e) }} value={this.props.state.firstName} />
//                             {this.props.state.firstName !== null ? <ReactTooltip id={"firstname" + this.props.state.firstName}>{this.props.state.firstName}</ReactTooltip> : ''}
//                         </div>
//                         <div className="col form-group" data-tip data-for={"lastname" + this.props.state.lastName}>
//                             <label>Last Name</label>
//                             <input disabled={!this.props.state.email.has_lastname == true ? true : false} type="text" className="form-control requiredfield json-col" placeholder="Last Name" name="lastName" onChange={(e) => { this.props.set_lastName(e) }} value={this.props.state.lastName} />
//                             {this.props.state.lastName !== null ? <ReactTooltip id={"lastname" + this.props.state.lastName}>{this.props.state.lastName}</ReactTooltip> : ''}
//                         </div>
//                     </div>
//                 </form>
//             </article>
//         )
//     }
// }
// module.exports = { NewUserComponent}
"use strict";
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Table = /*#__PURE__*/function (_Component) {
  _inherits(Table, _Component);

  var _super = _createSuper(Table);

  function Table() {
    _classCallCheck(this, Table);

    return _super.apply(this, arguments);
  }

  _createClass(Table, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement("div", null);
    }
  }]);

  return Table;
}(_react.Component);

exports["default"] = Table;
